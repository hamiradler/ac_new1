// approve.js

{jsvar name=approval var=$approval}

{literal}

function approval_approve() {
	if ( !confirm(jsAreYouSure) ) return;
	ac_ui_api_call(jsApproving);
	ac_ajax_call_cb("api.php", "approval.approval_approve", approval_approve_cb, approval.id, approval.hash);
}

function approval_approve_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded == 1) {
		ac_result_show(ary.message);
		ac_dom_toggle_class('infobox', 'ac_block', 'ac_hidden');
		ac_dom_toggle_class('approvedbox', 'ac_block', 'ac_hidden');
		//window.setTimeout(function() { window.close(); }, 1000);
	} else {
		ac_error_show(ary.message);
	}
}

function approval_decline_toggle() {
	ac_dom_toggle_class('declinebox', 'ac_block', 'ac_hidden');
	ac_dom_toggle_class('infobox', 'ac_block', 'ac_hidden');
}

function approval_decline() {
	if ( !confirm(jsAreYouSure) ) return;
	var post = ac_form_post($('declinebox'));
	post.id = approval.id;
	post.hash = approval.hash;

	ac_ui_api_call(jsLoading);
	ac_ajax_post_cb("api.php", "approval.approval_decline", approval_decline_cb, post);
}

function approval_decline_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded == 1) {
		ac_result_show(ary.message);
		ac_dom_toggle_class('declinebox', 'ac_block', 'ac_hidden');
		ac_dom_toggle_class('declinedbox', 'ac_block', 'ac_hidden');
		//window.setTimeout(function() { window.close(); }, 1000);
	} else {
		ac_error_show(ary.message);
	}
}

{/literal}
