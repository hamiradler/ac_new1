<?php
header("HTTP/1.1 301 Moved Permanently");

if (isset($_GET["c"]) && isset($_GET["m"])) {
	$hash = sprintf("%s.%d", md5($_GET["c"]), $_GET["m"]);
	header("Location: index.php?action=social&c=$hash");
} else {
	header("Location: index.php");
}

?>
