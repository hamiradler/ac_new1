{literal}
function group_search_defaults() {
	$("search_content").value = '';

	var boxes = $("search").getElementsByTagName("input");

	for (var i = 0; i < boxes.length; i++) {
		if (boxes[i].type == "checkbox")
			boxes[i].checked = true;
	}
}

function group_search_check() {
	ac_dom_display_block("search");
}

function group_search() {
	var post = ac_form_post($("search"));

	$("list_search").value = post.content;

	ac_ajax_post_cb("api.php", "group!ac_group_filter_post", group_search_cb, post);
}

function group_search_cb(xml) {
	ac_dom_toggle_display("search", "block");
	group_list_search_cb(xml);
}
{/literal}
