{literal}
function cron_search_defaults() {
	$("search_content").value = '';

	var boxes = $("search").getElementsByTagName("input");

	for (var i = 0; i < boxes.length; i++) {
		if (boxes[i].type == "checkbox")
			boxes[i].checked = true;
	}
}

function cron_search_check() {
	ac_dom_display_block("search");
}

function cron_search() {
	var post = ac_form_post($("search"));

	$("list_search").value = post.content;

	ac_ajax_post_cb("api.php", "cron!ac_cron_filter_post", cron_search_cb, post);
}

function cron_search_cb(xml) {
	ac_dom_toggle_display("search", "block");
	cron_list_search_cb(xml);
}
{/literal}
