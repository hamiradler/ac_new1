function auth_check() {
	ac_ui_api_call(jsLoading);

	var post = { "password": $("authPassword").value };
	ac_ajax_post_cb(apipath, 'instup!auth_check', auth_check_cb, post);
}

function auth_check_cb(xml) {
	var ary = ac_dom_read_node(xml, ac_b64_decode);
	ac_ui_api_callback();
	if ( ary.succeeded && ary.succeeded == 1 ) {
		updater_step();
		ac_result_show(ary.message);
		$('dl_s').value = ary.dl_s;
		$('dl_s').focus();
	} else {
		ac_error_show(ary.message);
	}
}


function updater_step() {
	step++;
	if ( $('auth') ) $('auth').className = ( step == 1 ? 'ac_block' : 'ac_hidden' );
	if ( $('backend') ) $('backend').className = ( step == 2 ? 'ac_block' : 'ac_hidden' );
	if ( $('checks') ) $('checks').className = ( step == 4 ? 'ac_block' : 'ac_hidden' );
	if ( $('settings') ) $('settings').className = ( step == 5 ? 'ac_block' : 'ac_hidden' );
	if ( $('updater') ) $('updater').className = ( step == 6 ? 'ac_block' : 'ac_hidden' );
	if ( $('backupwarning') ) $('backupwarning').className = ( step != 6 ? 'backupwarning' : 'ac_hidden' );
	if ( $('langchangerbox') ) $('langchangerbox').className = ( step != 6 ? 'ac_block' : 'ac_hidden' );
	var menu = $('updatermenu').getElementsByTagName('li');
	for ( var i = 0; i < menu.length; i++ ) {
		if ( step == i + 1 ) {
			menu[i].className = 'currentstep';
		} else if ( step < i + 1 ) {
			menu[i].className = 'nextstep';
		} else {
			menu[i].className = 'previousstep';
		}
	}
}

function updater_next() {
	if ( step == 4 ) {
		updater_step();
		if ( ask4URL ) {
			//$('murl').focus();
		} else {
			updater_next();
		}
	} else /*if ( step == 5 )*/ {
		plink_send();
	}
}


function plink_send() {
	ac_ui_api_call(jsLoading);
	ac_ajax_call_cb(apipath, 'instup!plink_send', plink_send_cb, $('murl').value);
}

function plink_send_cb(xml) {
	var ary = ac_dom_read_node(xml, ac_b64_decode);
	ac_ui_api_callback();
	if ( ary.succeeded && ary.succeeded == 1 ) {
		ac_result_show(ary.message);
		updater_step();
		// start it!
		$('updateriframe').src = plink + "/ac_global/scripts/updateri.php";
	} else {
		ac_error_show(ary.message);
	}
}


