// api.js


function ac_api_call(action, callback, msg, vars) {
	ac_ui_api_call(msg);
	if ( typeof callback != 'function' ) callback = null;
	ac_ajax_call_cb('api.php', action, callback, vars);
}

function ac_api_post(action, callback, msg, vars) {
	ac_ui_api_call(msg);
	if ( typeof callback != 'function' ) callback = null;
	ac_ajax_post_cb('api.php', action, callback, vars);
}

function ac_api_handle(xml, txt, func) {
	var ary = ac_dom_read_node(xml, null);
	ac_ui_api_callback();
	if ( typeof ary.error != 'undefined' ) {
		// internal error?
	}
	if ( typeof func == 'function' ) func(ary);
}

function ac_api_result(xml) {
	var ary = ac_dom_read_node(xml, null);
	ac_ui_api_callback();
	if ( ary.succeeded && ary.succeeded == 1 ) {
		ac_result_show(ary.message);
	} else {
		ac_error_show(ary.message);
	}
}
