<?php
if (!@ini_get("zlib.output_compression")) @ob_start("ob_gzhandler");

// require main include file
require_once(dirname(__FILE__) . '/prepend.inc.php');
require_once(ac_global_functions('smarty.php'));

/*
	== permission checks go here! ==
*/
if ( !ac_admin_isadmin() ) {
	echo 'You are not logged in.';
	exit;
}

// Preload the language file
ac_lang_get('admin');

// collect input
$mid = (int)ac_http_param('m');
$tid = (int)ac_http_param('t');

$source = "";
$subject = "";

if ( $mid ) {
	$message = ac_sql_select_row("SELECT * FROM #message WHERE id = '$mid'");
	$subject = $message["subject"];
	$source = $message["html"];

	if ($source == "")
		$source = $message["text"];
} elseif ($tid) {
	$template = ac_sql_select_row("SELECT * FROM #template WHERE id = '$tid'");
	$source = $template["content"];
	$subject = $template["name"];
}

# No source?
if ($source == "") {
	$source = "<div style='font-size: 64px; color: #ccc'>" . _a("No Preview") . "</div>";
	$subject = _a("No Preview");
}

// Smarty Template system setup
$smarty = new AC_Smarty('admin');

// assigning smarty reserved vars
$smarty->assign('site', $site);
$smarty->assign('admin', $admin);
$smarty->assign('source', $source);
$smarty->assign('subject', $subject);
$smarty->assign('build', $thisBuild);
$smarty->assign("version", str_replace(".", "", $site['version']));

$smarty->display('htmlpreview.htm');

?>
