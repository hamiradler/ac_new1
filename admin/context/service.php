<?php

require_once ac_admin("functions/service.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
class service_context extends ACP_Page {

	function service_context() {
		$this->pageTitle = _a("External Services");
		$this->sideTemplate = "side.form.htm";
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);

		$smarty->assign("content_template", "service.htm");
		$smarty->assign("ac_admin_ismaingroup", ac_admin_ismaingroup());

		$admin = ac_admin_get();
		$site = ac_site_get();

		$serial_hash = md5($site["serial"]);
		$smarty->assign("serial_hash", $serial_hash);

		// coming back from Shopify (after authorizing)
		$shopify_shop = ac_http_param("shopify_shop");
		$shopify_api_password = ac_http_param("shopify_api_password");
		if ($shopify_shop && $shopify_api_password) {
			$data_existing = ac_sql_select_one("SELECT `data` FROM #service WHERE id = 5");
			if ($data_existing) {
				$data_existing = unserialize($data_existing);
				// save/update just the shop URL and API password
				$data_existing[ $admin["id"] ]["shop_url"] = "http://" . $shopify_shop;
				$data_existing[ $admin["id"] ]["api_password"] = $shopify_api_password;
				$data = $data_existing;
			}
			else {
				$data = array(
					$admin["id"] => array(
						"shop_url" => "http://" . $shopify_shop,
						"api_password" => $shopify_api_password,
					),
				);
			}
			$data = serialize($data);
			$ary = array("data" => $data);
			$sql = ac_sql_update("#service", $ary, "id = 5");
			// redirect because we dont want the extra URL params visible
			ac_http_redirect("main.php?action=service#form-5");
		}

		// get global custom fields
		$fields = list_get_fields(array(), true); // no list id's, but global
		$smarty->assign("fields", $fields);

		// get ALL custom fields (for DOM reference later)
		$fields_all = list_field_select_nodata_rel("all");
		$smarty->assign("fields_all", $fields_all);

		$user_password = ac_sql_select_one("SELECT password FROM acp_globalauth WHERE username = '" . $GLOBALS['admin']['username'] . "'");
		$smarty->assign("user_password", $user_password);
		$smarty->assign("handshake", md5($GLOBALS['admin']['username'] . $user_password));

		$smarty->assign('p_link_encoded', urlencode($GLOBALS['site']['p_link']));

		$admin = ac_admin_get();
		$liststr = implode("','", $admin["lists"]);
		$lists = ac_sql_select_array("SELECT id, name, p_use_twitter, twitter_token, twitter_token_secret, p_use_facebook, facebook_session FROM #list WHERE id IN ('$liststr') ORDER BY name");

		foreach ($lists as $k => $list) {
			$list_twitter_accounts = array();
			$list_facebook_accounts = array();
			if ((int)$list["p_use_twitter"] && $list["twitter_token"] && $list["twitter_token_secret"]) {
				$twitter_account = list_twitter_oauth_verifycredentials($list["twitter_token"], $list["twitter_token_secret"]);
				$lists[$k]["twitter_accounts_toupdate"] = $twitter_account["screen_name"];
			}
			if ((int)$list["p_use_facebook"] && $list["facebook_session"]) {
				$facebook_session_object = unserialize($list["facebook_session"]);
				// the root (main) account - store it's ID and name
				$list_facebook_accounts[$facebook_session_object->id] = $facebook_session_object->name;
				if (isset($facebook_session_object->accounts->data) && $facebook_session_object->accounts->data) {
					// loop through all available Facebook accounts for this Facebook user
					foreach ($facebook_session_object->accounts->data as $account) {
						// all sub-accounts - store it's ID and name
						$list_facebook_accounts[$account->id] = $account->name;
					}
				}
				// store in main array
				$lists[$k]["facebook_session_accounts"] = $list_facebook_accounts;
				if (isset($facebook_session_object->accounts_toupdate) && $facebook_session_object->accounts_toupdate) {
					foreach ($facebook_session_object->accounts_toupdate as $accountid) {
						$lists[$k]["facebook_session_accounts_toupdate"][] = $list_facebook_accounts[$accountid];
					}
					// make it a string so it is easy to display
					$lists[$k]["facebook_session_accounts_toupdate"] = implode(", ", $lists[$k]["facebook_session_accounts_toupdate"]);
				}
			}
		}
//dbg($lists);

		$smarty->assign("lists", $lists);

		$forms = ac_sql_select_array("
			SELECT
				f.id,
				f.name
			FROM
				#form f
			WHERE
				f.target = '1'	-- FORM_SUBSCRIBE
			AND
				(SELECT COUNT(*) FROM #form_list L WHERE L.formid = f.id AND L.listid IN ('$liststr')) > 0
		");
		$smarty->assign("forms", $forms);

		$pconf = new Config("public");
		$smarty->assign("pconf", $pconf->export());

		$services_connected = service_connected();
		$smarty->assign("services_connected", $services_connected);

		$so = new AC_Select;
		$so->count();
		$total = (int)ac_sql_select_one(service_select_query($so));
		$count = $total;

		$paginator = new Pagination($total, $count, 20, 0, 'main.php?action=service');
		$paginator->allowLimitChange = true;
		$paginator->ajaxAction = 'service.service_select_array_paginator';
		$smarty->assign('paginator', $paginator);

	}
}

?>
