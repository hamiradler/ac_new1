<?php

require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
require_once ac_admin("functions/form.php");
require_once ac_admin("functions/message.php");
require_once ac_admin("functions/list.php");

class formunsub_conf_context extends ACP_Page {
	function formunsub_conf_context() {
		$this->pageTitle = _a("Unsubscribe Confirmation Form");
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);

		$admin = ac_admin_get();
		$listid = (int)ac_http_param("listid");
		$smarty->assign("ac_load_editor", "1");
		if (!$listid) {
			$listid = current($admin["lists"]);
			if (!$listid) ac_http_redirect("main.php?action=form");
			ac_http_redirect("main.php?action=formunsub_conf&listid=$listid");
		}

		$form = form_select_row_listid($listid, FORM_UNSUBSCRIBE);

		if (!$form) {
			if (!$this->admin["pg_form_edit"]) {
				$smarty->assign('content_template', 'noaccess.htm');
				return;
			}

			$listname = (string)ac_sql_select_one("SELECT name FROM #list WHERE id = '$listid'");
			$name = sprintf(_a("%s Unsubscribe"), $listname);

			$ins = array(
				"name" => $name,
				"theme" => "grey",
				"target" => FORM_UNSUBSCRIBE,
			);

			ac_sql_insert("#form", $ins);
			$id = (int)ac_sql_insert_id();

			# Set up list relation
			$ins = array(
				"formid" => $id,
				"listid" => $listid,
			);

			ac_sql_insert("#form_list", $ins);

			# Set up opt-in message.
			$ins = array(
				"userid" => $admin["id"],
				"=cdate" => "NOW()",
				"=mdate" => "NOW()",
				"fromname" => $admin["first_name"] . " " . $admin["last_name"],
				"fromemail" => $admin["email"],
				"reply2" => $admin["email"],
				"priority" => 3,
				"charset" => "utf-8",
				"encoding" => "quoted-printable",
				"format" => "mime",
				"subject" => _a("Your unsubscription confirmation"),
				"html" => '<body>
<div style="font-size: 12px; font-family: Arial, Helvetica;"><strong>Hi,</strong></div>
<div style="font-size: 12px; font-family: Arial, Helvetica;"><b><br></b></div>
<div style="font-size: 12px; font-family: Arial, Helvetica;"><b>This is a confirmation email letting you know about your subscription update with our mailing list %LISTNAME%</b></div>
<div style="padding: 15px; font-size: 12px; background: #F2FFD8; border: 3px solid #E4F4C3; margin-bottom: 0px; margin-top: 15px; font-family: Arial, Helvetica;">You have been unsubscribed and will not receive any further emails.</div>
</body>',
				"htmlfetch" => "now",
				"textfetch" => "now",
			);

			$ins["text"] = ac_htmltext_convert($ins["html"]);

			ac_sql_insert("#message", $ins);
			$messageid = (int)ac_sql_insert_id();

			$up = array(
				"messageid" => $messageid,
			);

			ac_sql_update("#form", $up, "id = '$id'");
			ac_http_redirect("main.php?action=formunsub_conf&listid=$listid");
		}

		$message = message_select_row($form["messageid"]);
		$fields = ac_cfield_select_nodata_rel("#field", "#field_rel", "r.relid IN ('0', '$listid')");

		$adminlists = implode("','", $admin["lists"]);
		$lists = ac_sql_select_array("SELECT id, name FROM #list WHERE id IN ('$adminlists') ORDER BY name");

		$smarty->assign("lists", $lists);
		$smarty->assign("form", $form);
		$smarty->assign("list", list_select_row($listid));
		$smarty->assign("listid", $listid);
		$smarty->assign("message", $message);
		$smarty->assign("fields", $fields);
		$smarty->assign("content_template", "formunsub_conf.htm");

		ac_smarty_submitted($smarty, $this);
	}

	function formProcess(&$smarty) {
		$id = (int)ac_http_param("id");
		$listid = (int)ac_http_param("listid");
		$messageid = (int)ac_http_param("messageid");
		$manage = (int)ac_http_param("managetext");

		$up = array(
			"managetext" => $manage,
		);

		ac_sql_update("#form", $up, "id = '$id'");

		$up = array(
			"fromname" => ac_http_param("fromname"),
			"fromemail" => ac_http_param("fromemail"),
			"reply2" => ac_http_param("reply2"),
			"subject" => ac_http_param("subject"),
			"html" => ac_http_param("html"),
			"text" => ac_http_param("text"),
		);

		# If they're not managing their own text version, do a quick convert on the html.
		if (!$manage) {
			$up["text"] = ac_htmltext_convert($up["html"]);
		}

		ac_sql_update("#message", $up, "id = '$messageid'");

		// Update the list field for the optout conf.

		$up = array(
			"optoutconf" => (int)ac_http_param("optoutconf"),
		);

		ac_sql_update("#list", $up, "id = '$listid'");
		ac_http_redirect("main.php?action=formunsub_conf&listid=$listid");
	}
}

?>
