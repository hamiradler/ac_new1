<?php

require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
require_once ac_admin("functions/form.php");

class form_code_context extends ACP_Page {
	function form_code_context() {
		$this->pageTitle = _a("Form Settings");
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);

		if (!ac_http_param("id")) {
			ac_http_redirect("main.php?action=form#list-01-0-0");
		}

		$site = ac_site_get();

		$id = (int)ac_http_param("id");
		$form = form_select_row($id);
		$qr_download = (ac_http_param_exists("qr") && ac_http_param("qr") == "download");
		if ($qr_download) {
			$dimensions = ac_http_param("dimensions");
		  $qr_url = "https://chart.googleapis.com/chart?chid=&cht=qr&chs=" . $dimensions . "&chl=" . urlencode($site["p_link"] . "index.php?action=form&id=" . $id);
			$qr_image = file_get_contents($qr_url);
			echo $qr_image;
			header("Content-disposition: attachment; filename=qr_code_form" . $id . "_" . $dimensions . ".png");
		}

		if (!$form)
			ac_http_redirect("main.php?action=form#list-01-0-0");

		$GLOBALS["form_compile_view"] = "working";
		$form["parts"] = form_compile($form["id"]);
		$html = form_html($form);
		$smarty->assign("html", $html);

		$admin = ac_admin_get();
		$liststr = implode("','", $admin["lists"]);

		$lists = ac_sql_select_array("
			SELECT
				l.id,
				l.name,
				(SELECT COUNT(*) FROM #form_list f WHERE f.listid = l.id AND f.formid = '$id') > 0 AS `selected`
			FROM
				#list l
			WHERE
				l.id IN ('$liststr')
		");

		$fb_s = md5($GLOBALS["site"]["serial"]);
		$fb_c = 'https://facebook.ac-app.com/c.php?s='.$fb_s.'&u='.urlencode($GLOBALS["site"]["p_link"]).'&f='.urlencode($form["id"]).'&n='.urlencode($form["name"]);

		if($GLOBALS["site"]["general_url_rewrite"]){
			$qrurl = $GLOBALS["site"]["p_link"].'/form/'.$form["id"];
		}else{
			$qrurl = $GLOBALS["site"]["p_link"].'/index.php?action=form&id='.$form["id"];
		}
		
		
		
		if($site["general_url_rewrite"]){
			$url_encoded = urlencode($site["p_link"].'/form/'.$form["id"]);
		}
		else{
			$url_encoded = urlencode($site["p_link"].'/index.php?action=form&id='.$form["id"]);
		}
		$smarty->assign("md5", form_md5($form["id"]));
		$smarty->assign("form", $form);
		$smarty->assign("lists", $lists);
		$smarty->assign("fb_c", $fb_c);
		$smarty->assign("url_encoded", $url_encoded);
		$smarty->assign("qrurl", urlencode($qrurl));
		$smarty->assign("content_template", "form_code.htm");

		ac_smarty_submitted($smarty, $this);
	}

	function formProcess(&$smarty) {
		switch (ac_http_param("next")) {
			default:
			case "list":
				ac_http_redirect("main.php?action=form#list-01-0-0");
				exit;

			case "edit":
				ac_http_redirect("main.php?action=form_edit&id=$id");
				exit;

			case "theme":
				ac_http_redirect("main.php?action=form_edit&id=$id&theme=1");
				exit;

			case "optin":
				ac_http_redirect("main.php?action=form_optin&id=$id");
				exit;
		}
	}
}

?>
