<?php

require_once ac_admin("functions/settings.php");
require_once ac_admin("functions/feedbackloop.php");
require_once ac_admin("functions/api.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
require_once ac_global_functions('ajax.php');
require_once ac_global_functions("group.php");

class settings_context extends ACP_Page {

	function settings_context() {
		$this->pageTitle = _a("Settings");
		//$this->sideTemplate = "side.settings.htm";
		$this->ACP_Page();
	}

	function process(&$smarty) {

		$this->setTemplateData($smarty);

		// default tab selected on page load
		$smarty->assign("tab_select", "account");

		$api_credentials = api_credentials_get();
		$smarty->assign("apiurl", $api_credentials["url"]);

		$this->setApiExplorer($smarty);

		$smarty->assign("zones", tz_box());

		if ( ac_admin_ismaingroup() ) {

			$smarty->assign("rwCheck", ac_php_rewrite_check());


			$URI = ( isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF'] );
			$URI = substr($URI, 0, strpos($URI, '/admin/main.php'));
			$smarty->assign("URI", $URI);
			$smarty->assign("htaccess", ac_base('.htaccess'));

			$mailconnections = array();
			$sql = ac_sql_query("SELECT * FROM #mailer ORDER BY corder");
			while ( $row = mysql_fetch_assoc($sql) ) {
				$row['pass'] = ( $row['pass'] == '' ? '' : base64_decode($row['pass']) ); // decoding mail password
				$row['groups'] = ac_sql_select_box_array("SELECT groupid, groupid FROM #group_mailer WHERE mailerid = '$row[id]'");
				$row['groupslist'] = implode(',', $row['groups']);
				$mailconnections[$row['id']] = $row;
			}
			$smarty->assign("mailconnections", $mailconnections);
			$smarty->assign("mailconnectionscnt", count($mailconnections));


			// what is our upload limit?
			$smarty->assign('uploadLimit', ini_get('upload_max_filesize'));
			// what is our post limit?
			$smarty->assign('postLimit', ini_get('post_max_size'));
			$smarty->assign("hash" , md5($this->site['serial']));

			$so = new AC_Select;
			$so->count();
			$total = (int)ac_sql_select_one(ac_group_select_query($so));
			$count = $total;

			$paginator_abuse = new Pagination($total, $count, 20, 0, 'main.php?action=group');
			$paginator_abuse->ajaxAction = 'abuse.abuse_select_array_paginator';
			$smarty->assign('paginator_abuse', $paginator_abuse);

			$so = new AC_Select;
			$so->count();
			$total = (int)ac_sql_select_one(feedbackloop_select_query($so));
			//$count = $total;
			$smarty->assign('fblcnt', $total);

			$so->orderby("f.tstamp DESC");
			$so->limit("0, 50");
			$feedbackloops = ac_sql_select_array(feedbackloop_select_query($so));
			$smarty->assign('feedbackloops', $feedbackloops);

			// get groups
			$so = new AC_Select;
			//$so->push("AND id > 1");				# Exclude the Visitors group
			$so->push("AND p_admin = 1");				# Exclude the non-admin groups
			$so->orderby("title");
			$groups = ac_group_select_array($so);
			$smarty->assign('groupsList', $groups);

			$header_lines = array(
			  "
			  <style type=\"text/css\">
			  	#ac_help_div5,
			  	#ac_help_div7
			  	{
			  	  width: 100px;
			  	}
			  	#ac_help_div8
			  	{
			  	  width: 50px;
			  	}
			  </style>
			  ",
			);
			$smarty->assign("header_lines", $header_lines);

			$smarty->assign("cnamefail", "");
		}

		$api_files = array();
		if ( $handle = opendir( ac_base("docs/api-examples") ) ) {
			while ( false !== ($filename = readdir($handle)) ) {
				$filename = ac_file_basename($filename);
				if ($filename) {
					$contents = file_get_contents( ac_base("docs/api-examples/" . $filename) );

					$title = preg_match("/Title:\s[^\n]*/", $contents, $matches);
					if (isset($matches[0]) && $matches[0]) $title = preg_replace("/Title:\s/", "", $matches[0]);

					$description = preg_match("/Description:\s[^\n]*/", $contents, $matches);
					if (isset($matches[0]) && $matches[0]) $description = preg_replace("/Description:\s/", "", $matches[0]);

					$post_check1 = preg_match("/post = array\(/", $contents);
					$post_check2 = preg_match("/CURLOPT_POSTFIELDS/", $contents);
					$request_method = ($post_check1 && $post_check2) ? "POST" : "GET";
					$post_array_text = "";
					if ($request_method == "POST") {
						// try to grab just the $post array text
						preg_match("/\$post\s=\sarray\([^\);]*/i", $contents, $post_array_text);
						if (isset($post_array_text[0]) && $post_array_text[0]) {
							$post_array_text = $post_array_text[0];
						}
					}
					$file_array = array(
						"name" => $filename,
						"title" => $title,
						"description" => $description,
						"example_file_content" => $contents,
						"post_array_text" => $post_array_text,
						"request_method" => $request_method,
					);
					$action = substr($filename, 0, strlen($filename) - 4);
					$api_files[$action] = $file_array;
				}
			}
			ksort($api_files);
			closedir($handle);
		}
//dbg($api_files);
		$smarty->assign("api_files", $api_files);

		ac_smarty_submitted($smarty, $this);
		$smarty->assign("content_template", "settings.htm");
	}

	function formProcess(&$smarty) {
		if ( ac_admin_ismaingroup() ) {
			$r_site = $this->formProcessSite($smarty);
			if ( !$r_site['succeeded'] ) return $r_site;
		}
		$r_admin = $this->formProcessAdmin($smarty);
		if ( !$r_admin['succeeded'] ) return $r_admin;

		return ac_ajax_api_result(1, _a("Settings saved."));
	}

	function formProcessAdmin(&$smarty) {
		if ($_POST['pass'] != '' and $_POST["pass"] != $_POST["pass_r"]) {
			return ac_ajax_api_result(0, _a("Please confirm both password fields match"));
		}

		// standard account array
		$update = array(
			'lang'                        => $_POST['lang_ch'],
			"local_zoneid"                => $_POST["local_zoneid"],
		);

		// deal with standard stuff here

		// update the user record
		$result['status'] = ac_sql_update('#user', $update, "id = '{$this->admin['id']}'");
		// if password has changed, add it to authenticator
		if ( !ac_auth_isconnected() ) ac_auth_connect();
		if ( isset($_POST['username']) ) unset($_POST['username']); // can't change his username
		if (preg_match('/^[ \t\r\n]+$/', $_POST['pass'])) {
			return ac_ajax_api_result(0, _a("You cannot use a password consisting only of spaces"));
		}
		$_POST['pass'] = trim($_POST['pass']);
		if ( $_POST['pass'] != '' ) $_POST['password'] = md5($_POST['pass']);
		ac_auth_update($_POST, $this->admin['absid']);

		$up = array(
			//'username' => $_POST['username'],
			'first_name' => $_POST['first_name'],
			'last_name' => $_POST['last_name'],
			'email' => $_POST['email'],
		);
		ac_sql_update("#user", $up, "absid = '{$this->admin['absid']}'");
		ac_sql_update('acp_globalauth', $up, "id = '{$this->admin['absid']}");

		// fetch new info
		ac_session_drop_cache();
		$this->admin = ac_admin_get_totally_unsafe($this->admin["id"]);
		$GLOBALS["admin"] = $this->admin;
		$smarty->assign("admin", $this->admin);

		cache_clear("prepend_admin");
		return ac_ajax_api_result(1, _a("Settings saved."));
	}

	function formProcessSite(&$smarty) {
		if (isset($GLOBALS['__is_our_demo']))
			return;

		// prepare data
		$update = array();

		if ( isset($_POST["general_url_rewrite"]) ) {
			$rwCheck = ac_php_rewrite_check();
			if ( !$rwCheck['apache'] ) {
				return ac_ajax_api_result(0, _a("You cannot enable the search-friendly URL feature on non-Apache web servers"));
			}
			if ( !$rwCheck['configured'] ) {
				$content = (string)ac_file_get(ac_base('example.htaccess'));
				$appURI = ( isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF'] );
				$appURI = substr($appURI, 0, strpos($appURI, '/admin/main.php'));
				$content = str_replace('/12allURI', $appURI, $content);
				if ( $rwCheck['htaccess'] ) {
					$content = ac_file_get(ac_base('.htaccess')) . "\n\n\n" . $content;
				}
				$created = @ac_file_put(ac_base('.htaccess'), $content);
				if ( !$created ) {
					// create
					return ac_ajax_api_result(0, _a("The system could not create/update your .htaccess file. Please configure the .htaccess file first before saving this setting."));
				}
			}
		}

		# Admin

		if (isset($_POST["maxuploadfilesize"]))
		$update["maxuploadfilesize"]             = intval($_POST["maxuploadfilesize"]);

		# Public

		$update["general_url_rewrite"]           = intval(isset($_POST["public_url_rewrite"]) || isset($GLOBALS['_hosted_account']) );

		# Local

		$update["dateformat"]                    = $_POST["dateformat"];
		$update["timeformat"]                    = $_POST["timeformat"];
		$update["datetimeformat"]                = $_POST["datetimeformat"];

		$update["mail_abuse"]              	     = intval(isset($_POST["mail_abuse"]) || isset($GLOBALS['_hosted_account']));


		$r = ac_sql_update('#backend', $update);


		if (!$r) {
			return ac_ajax_api_result(0, _a("Settings could not be saved. "));
		}

		ac_session_drop_cache();
		$GLOBALS['site'] = ac_site_unsafe();
		$smarty->assign("site", $GLOBALS["site"]);

		cache_clear("prepend_site");
		return ac_ajax_api_result(1, _a("Settings Saved."));
	}

	function setApiExplorer(&$smarty) {
		// sets API Explorer specific stuff

		$admin = ac_admin_get();

		// find all users for this admin's groups (useful when pulling/showing data that is specific to the logged-in user's groups)
		$admin_groups_userids = array();
		foreach ($admin["groups"] as $groupid) {
			$users_from_group = ac_sql_select_list("SELECT userid FROM #user_group WHERE groupid = '{$groupid}'");
			$admin_groups_userids = array_merge($users_from_group, $admin_groups_userids);
		}
//dbg($admin_groups_userids);
		$admin_groups_userids_str = implode("','", $admin_groups_userids);

		$api_explorer_listids = implode("','", $admin["lists"]);
		$api_explorer_fields = ac_cfield_select_nodata_rel("#field", "#field_rel", "r.relid IN ('$api_explorer_listids','0')", "", "r.dorder, f.title");
		$smarty->assign("api_explorer_fields", $api_explorer_fields);

		$admin_lists = array();
		foreach ($admin["lists"] as $listid) {
			$list = list_select_row($listid);
			$admin_lists[$listid] = $list;
		}
		$smarty->assign("admin_lists", $admin_lists);

		// pull five random messages for this admin user (used for campaign_create)
		$message_ids = ac_sql_select_list("SELECT id FROM #message m WHERE m.userid = '" . $admin["id"] . "' ORDER BY RAND() LIMIT 5");
		$messages = message_select_array(null, $message_ids);
		$smarty->assign("messages", $messages);

		// pull some campaigns created by this user's group(s) - helpful when having to use campaignid as a parameter - we can show them a listing
		$campaigns = ac_sql_select_array("SELECT id, name FROM #campaign WHERE userid IN ('{$admin_groups_userids_str}') ORDER BY RAND() LIMIT 5");
//dbg($campaigns);
		$smarty->assign("campaigns", $campaigns);
	}
}

?>
