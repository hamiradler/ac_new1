<?php

require_once ac_admin("functions/bounce_code.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
class bounce_code_context extends ACP_Page {

	function bounce_code_context() {
		$this->pageTitle = _a("Bounce Codes");
		$this->sideTemplate = "";
		$this->ACP_Page();
	}

	function process(&$smarty) {

		$this->setTemplateData($smarty);

		if ( !ac_admin_ismain() ) {
			// assign template
			$smarty->assign('content_template', 'noaccess.htm');
			return;
		}

		$smarty->assign("content_template", "bounce_code.htm");

		$so = new AC_Select;
		$so->count();
		$total = (int)ac_sql_select_one(bounce_code_select_query($so));
		$count = $total;

		$paginator = new Pagination($total, $count, 20, 0, 'main.php?action=bounce_code');
		$paginator->allowLimitChange = true;
		$paginator->ajaxAction = 'bounce_code.bounce_code_select_array_paginator';
		$smarty->assign('paginator', $paginator);

		$sections = array(
			array("col" => "b.code", "label" => _a("Bounce Code")),
			array("col" => "b.match", "label" => _a("Matching String")),
			array("col" => "b.type", "label" => _a("Bounce Type")),
			array("col" => "b.descript", "label" => _a("Description")),
		);
		$smarty->assign("search_sections", $sections);
	}
}

?>
