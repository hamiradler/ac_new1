<?php

require_once ac_admin("functions/list.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");
require_once ac_admin("functions/template.php");
require_once ac_admin("functions/facebook.php");
class list_context extends ACP_Page {

	function list_context() {
		$this->pageTitle = _a("Lists");
		$this->ACP_Page();
	}

	function process(&$smarty) {

		$this->setTemplateData($smarty);

		$site = ac_site_get();
		$admin = ac_admin_get();

		if (!$this->admin["pg_list_add"] && !$this->admin["pg_list_edit"] && !$this->admin["pg_list_delete"]) {
			$smarty->assign('content_template', 'noaccess.htm');
			return;
		}

		$smarty->assign("content_template", "list.htm");
		//$smarty->assign("side_content_template", "side.list.htm");

		$so = new AC_Select;
		$so->count();
		$so->greedy = true;
		$total = (int)ac_sql_select_one(list_select_query($so));
		$count = $total;

		$paginator = new Pagination($total, $count, 10 /*$this->admin['lists_per_page']*/, 0, 'main.php?action=list');
		//$paginator->allowLimitChange = true;
		$paginator->ajaxAction = 'list.list_select_array_paginator';
		$smarty->assign('paginator', $paginator);

/*
		$so = new AC_Select();
		$so->count();
		$listscnt = (int)ac_sql_select_one(campaign_select_query($so));
		$smarty->assign('campaignscnt', $campaignscnt);
*/

		// get all groups
		$groups = group_get_all(0, "g.p_admin = 1");
		foreach ( $groups as $k => $v ) {
			$groups[$k]['admins'] = array();
			if ( ac_admin_ismaingroup() ) {
				$groups[$k]['admins'] = ac_sql_select_array("SELECT u.* FROM #user u, #user_group g WHERE u.id = g.userid AND g.groupid = '$k'");
			}
		}
		//dbg($groups);
		$smarty->assign('groupsList', $groups);
		$smarty->assign('groupsListCnt', count($groups));

		$smarty->assign("addresses", list_addresses());

		// oauth for auto-sharing campaigns
		$pass = function_exists('curl_init') && function_exists('hash_hmac') && (int)PHP_VERSION > 4;
		if ($pass) {
			require_once ac_global_functions("json.php");
			if ( ac_http_param_exists("oauth_token") && ac_http_param_exists("oauth_verifier") && ac_http_param_exists("id") ) {
				// coming back from twitter after authorizing
				$access = list_twitter_oauth_init2( ac_http_param("id"), ac_http_param("oauth_token"), $_SESSION["twitter_oauth_token_secret"], ac_http_param("oauth_verifier") );
				header( "Location: " . $site["p_link"] . "/admin/main.php?action=list&settingsid=" . ac_http_param("id") );
			}
			if (ac_http_param_exists('facebook_logout') && ac_http_param('facebook_logout') != '') {
				ac_sql_query("UPDATE #list SET facebook_session = NULL WHERE id = '" . ac_http_param('facebook_logout') . "' LIMIT 1");
			}
			elseif ( ac_http_param_exists("code") ) {
				// coming back after authorizing on Facebook
				$listid = ac_http_param("formid");
				$redirect_url = $GLOBALS["site"]["p_link"] . "/admin/main.php?action=list&formid=" . $listid;
				$facebook_access_token = facebook_oauth_get_useraccesstoken($redirect_url, ac_http_param("code"));
				$facebook_me = facebook_graph($facebook_access_token, "me");
				$facebook_me = json_decode($facebook_me);
				$facebook_me->access_token = $facebook_access_token;
				$facebook_me_accounts = facebook_graph($facebook_access_token, "me/accounts");
				$facebook_me->accounts = json_decode($facebook_me_accounts);
				$facebook_me->accounts_toupdate = array();
				$facebook_me_data = serialize($facebook_me);
				ac_sql_query("UPDATE #list SET facebook_session = '$facebook_me_data' WHERE id = $listid LIMIT 1");
			}

			// when coming back from facebook (after logging in or out), we could not use #form-1 in the URL (facebook does not like it), so we use &formid=1.
			// we then properly capture that, and redirect to #form-1.
			if ( ac_http_param_exists("formid") && ac_http_param("formid") != "" ) {
				ac_http_redirect($site["p_link"] . "/admin/main.php?action=list&settingsid=" . (int)ac_http_param("formid"));
			}
		}

		$smarty->assign('pass', $pass);


		$smarty->assign('newform', ac_http_param_exists('new'));
		$smarty->assign('settingsid', (int)ac_http_param('settingsid'));

	}
}

?>
