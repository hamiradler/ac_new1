<?php

require_once ac_admin("functions/campaign.php");
require_once ac_global_classes("select.php");
require_once ac_global_classes("pagination.php");

class campaign_context extends ACP_Page {

	function campaign_context() {
		$this->pageTitle = _a("Campaigns");
		$this->sideTemplate = "side.campaign.htm";
		$this->ACP_Page();
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$smarty->assign("content_template", "campaign.htm");


		if (!$this->admin["pg_message_add"] && !$this->admin["pg_message_edit"] && !$this->admin["pg_message_delete"] && !$this->admin["pg_message_send"]) {
			$smarty->assign('side_content_template', '');
			$smarty->assign('content_template', 'noaccess.htm');
			return;
		}
		elseif ( list_get_cnt() == 0 ) {
			$smarty->assign('side_content_template', '');
			$smarty->assign('content_template', 'nolists.htm');
			return;
		}

		if ( isset($_GET['reports']) ) {
			ac_http_redirect("main.php?action=report");
		}

		$so = new AC_Select;

		// list filter
		$filterArray = campaign_filter_post();
		$filter = $filterArray['filterid'];
		if ($filter > 0) {
			$conds = ac_sql_select_one("SELECT conds FROM #section_filter WHERE id = '$filter' AND userid = '{$this->admin['id']}' AND sectionid = 'campaign'");
			$so->push($conds);
		}
		$smarty->assign("filterid", $filter);
		$smarty->assign("listfilter", ( isset($_SESSION['nla']) ? $_SESSION['nla'] : null ));

		$so->count();
		$total = (int)ac_sql_select_one(campaign_select_query($so));
		$count = $total;

		$paginator = new Pagination($total, $count, 10, 0, 'main.php?action=campaign');
		//$paginator->allowLimitChange = true;
		$paginator->ajaxAction = 'campaign.campaign_select_array_paginator';
		$smarty->assign('paginator', $paginator);


		$so = new AC_Select();
		$so->count();
		$campaignscnt = (int)ac_sql_select_one(campaign_select_query($so));
		$smarty->assign('campaignscnt', $campaignscnt);


		$campaign_statuses = campaign_statuses();
		$smarty->assign("campaign_statuses", $campaign_statuses);

		$types = campaign_types();
		$smarty->assign("types", $types);

		$recur_intervals = campaign_recur_intervals();
		$smarty->assign("recur_intervals", $recur_intervals);


		campaign_sidemenu($smarty, 'campaign');

	}
}

?>
