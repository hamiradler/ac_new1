<?php
  /**
   *
   * @ This file is created by Decodeby.US
   * @ deZender Public (PHP5 Decompiler)
   *
   * @	Version			:	1.0.0.2
   * @	Author			:	Ps2Gamer & Cyko
   * @	Release on		:	25.07.2011
   * @	Official site	:	http://decodeby.us
   *
   */
  
  $sitename = "ActiveCampaign Email Marketing Software";
  $a        = dirname(dirname(__FILE__)) . "/ac_global/includes/install.php";
  if (!file_exists($a)) {
      exit("Corrupted installation - please reupload all the files before continuing.");
  }
  define("REQUIRE_MYSQLVER", "4.1");
  require_once($a);
?>