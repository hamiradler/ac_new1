var personalization_table = new ACTable();
var personalization_list_sort = "02";
var personalization_list_offset = "0";
var personalization_list_filter = {jsvar var=$filterid};
var personalization_list_sort_discerned = false;

{literal}
personalization_table.setcol(0, function(row) {
	return Builder.node("input", { type: "checkbox", name: "multi[]", value: row.id, onclick: "ac_form_check_selection_none(this, $('acSelectAllCheckbox'), $('selectXPageAllBox'))" });
});

personalization_table.setcol(1, function(row) {
	var edit = Builder.node("a", { href: sprintf("#form-%d", row.id) }, jsOptionEdit);
	var dele = Builder.node("a", { href: sprintf("#delete-%d", row.id) }, jsOptionDelete);

	var ary = [];

	if (ac_js_admin.pg_template_edit) {
		ary.push(edit);
		ary.push(" ");
	}

	if (ac_js_admin.pg_template_delete) {
		ary.push(dele);
	}

	return Builder.node("div", { className: "ac_table_row_options" }, ary);
});

personalization_table.setcol(2, function(row) {
	// name
	return '%' + row.tag + '%';
});

personalization_table.setcol(3, function(row) {
	// name
	return row.name;
});

personalization_table.setcol(4, function(row) {
	// format
	return row.format;
});

personalization_table.setcol(5, function(row) {
	// lists
	return row.lists;
});

function personalization_list_anchor() {
	return sprintf("list-%s-%s-%s", personalization_list_sort, personalization_list_offset, personalization_list_filter);
}

function personalization_list_tabelize(rows, offset) {
	if (rows.length < 1) {
		if (!personalization_list_filter || personalization_list_filter == 0) {
			ac_ui_api_callback();
			ac_ui_anchor_set('form-0');
			return;
		}
		// We may have some trs left if we just deleted the last row.
		ac_dom_remove_children($("list_table"));

		$("list_noresults").className = "ac_block";
		if ($("list_delete_button") !== null)
			$("list_delete_button").className = "ac_hidden";
		$("loadingBar").className = "ac_hidden";
		ac_ui_api_callback();
		return;
	}
	$("list_noresults").className = "ac_hidden";
	if ($("list_delete_button") !== null)
		$("list_delete_button").className = "ac_inline";
	ac_paginator_tabelize(personalization_table, "list_table", rows, offset);
	$("loadingBar").className = "ac_hidden";
}

// This function should only be run through a paginator (e.g., paginators[n].paginate(offset))
function personalization_list_paginate(offset) {
	if (!ac_loader_visible() && !ac_result_visible() && !ac_error_visible())
		ac_ui_api_call(jsLoading);

	if (personalization_list_filter > 0)
		$("list_clear").style.display = "inline";
	else
		$("list_clear").style.display = "none";

	personalization_list_offset = parseInt(offset, 10);

	ac_ui_anchor_set(personalization_list_anchor());
	$("loadingBar").className = "ac_block";
	ac_ajax_call_cb(this.ajaxURL, this.ajaxAction, paginateCB, this.id, personalization_list_sort, personalization_list_offset, this.limit, personalization_list_filter);

	$("list").className = "ac_block";
}

function personalization_list_clear() {
	personalization_list_sort = "02";
	personalization_list_offset = "0";
	personalization_list_filter = "0";
	personalization_listfilter = null;
	$("JSListManager").value = 0;
	$("list_search").value = "";
	list_filters_update(0, 0, true);
	personalization_search_defaults();
	ac_ui_anchor_set(personalization_list_anchor());
}

function personalization_list_search() {
	var post = ac_form_post($("list"));
	personalization_listfilter = post.listid;
	list_filters_update(0, post.listid, false);
	ac_ajax_post_cb("api.php", "personalization.personalization_filter_post", personalization_list_search_cb, post);
}

function personalization_list_search_cb(xml) {
	var ary = ac_dom_read_node(xml);

	personalization_list_filter = ary.filterid;
	ac_ui_anchor_set(personalization_list_anchor());
}

function personalization_list_chsort(newSortId) {
	var oldSortId = ( personalization_list_sort.match(/D$/) ? personalization_list_sort.substr(0, 2) : personalization_list_sort );
	var oldSortObj = $('list_sorter' + oldSortId);
	var sortObj = $('list_sorter' + newSortId);
	// if sort column didn't change (only direction [asc|desc] did)
	if ( oldSortId == newSortId ) {
		// switching asc/desc
		if ( personalization_list_sort.match(/D$/) ) {
			// was DESC
			newSortId = personalization_list_sort.substr(0, 2);
			sortObj.className = 'ac_sort_asc';
		} else {
			// was ASC
			newSortId = personalization_list_sort + 'D';
			sortObj.className = 'ac_sort_desc';
		}
	} else {
		// remove old personalization_list_sort
		if ( oldSortObj ) oldSortObj.className = 'ac_sort_other';
		// set sort field
		sortObj.className = 'ac_sort_asc';
	}
	personalization_list_sort = newSortId;
	ac_ui_api_call(jsSorting);
	ac_ui_anchor_set(personalization_list_anchor());
	return false;
}

function personalization_list_discern_sortclass() {
	if (personalization_list_sort_discerned)
		return;

	var elems = $("list_head").getElementsByTagName("a");

	for (var i = 0; i < elems.length; i++) {
		var str = sprintf("list_sorter%s", personalization_list_sort.substring(0, 2));

		if (elems[i].id == str) {
			if (personalization_list_sort.match(/D$/))
				elems[i].className = "ac_sort_desc";
			else
				elems[i].className = "ac_sort_asc";
		} else {
			elems[i].className = "ac_sort_other";
		}
	}

	personalization_list_sort_discerned = true;
}


function personalization_list_export(id) {
	ac_dom_toggle_class('personalization_export' + id, 'ac_offer', 'ac_hidden');
	return false;
}

{/literal}
