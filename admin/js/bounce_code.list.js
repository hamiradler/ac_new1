var bounce_code_list_str_type_hard = '{"Hard"|alang|js}';
var bounce_code_list_str_type_soft = '{"Soft"|alang|js}';
{literal}
var bounce_code_table = new ACTable();
var bounce_code_list_sort = "01";
var bounce_code_list_offset = "0";
var bounce_code_list_filter = "0";
var bounce_code_list_sort_discerned = false;

bounce_code_table.setcol(0, function(row) {
	return Builder.node("input", { type: "checkbox", name: "multi[]", value: row.id, onclick: "ac_form_check_selection_none(this, $('acSelectAllCheckbox'), $('selectXPageAllBox'))" });
});

bounce_code_table.setcol(1, function(row) {
	var edit = Builder.node("a", { href: sprintf("#form-%d", row.id) }, jsOptionEdit);
	var dele = Builder.node("a", { href: sprintf("#delete-%d", row.id) }, jsOptionDelete);

	var ary = [];

	if (ac_js_admin.id == 1) {
		ary.push(edit);
		ary.push(" ");
	}

	if (ac_js_admin.id == 1) {
		ary.push(dele);
	}

	return Builder.node("div", { className: "ac_table_row_options" }, ary);
});

bounce_code_table.setcol(2, function(row) {
	return row.code;
});

bounce_code_table.setcol(3, function(row) {
	return row.match;
});

bounce_code_table.setcol(4, function(row) {
	return ( row.type == 'hard' ? bounce_code_list_str_type_hard : bounce_code_list_str_type_soft );
});

bounce_code_table.setcol(5, function(row) {
	return row.descript;
});

function bounce_code_list_anchor() {
	return sprintf("list-%s-%s-%s", bounce_code_list_sort, bounce_code_list_offset, bounce_code_list_filter);
}

function bounce_code_list_tabelize(rows, offset) {
	if (rows.length < 1) {
		// We may have some trs left if we just deleted the last row.
		ac_dom_remove_children($("list_table"));

		$("list_noresults").className = "ac_block";
		if ($("list_delete_button") !== null)
			$("list_delete_button").className = "ac_hidden";
		$("loadingBar").className = "ac_hidden";
		ac_ui_api_callback();
		return;
	}
	$("list_noresults").className = "ac_hidden";
	if ($("list_delete_button") !== null)
		$("list_delete_button").className = "ac_inline";
	ac_paginator_tabelize(bounce_code_table, "list_table", rows, offset);
	$("loadingBar").className = "ac_hidden";
}

// This function should only be run through a paginator (e.g., paginators[n].paginate(offset))
function bounce_code_list_paginate(offset) {
	if (!ac_loader_visible() && !ac_result_visible() && !ac_error_visible())
		ac_ui_api_call(jsLoading);

	if (bounce_code_list_filter > 0)
		$("list_clear").style.display = "inline";
	else
		$("list_clear").style.display = "none";

	bounce_code_list_offset = parseInt(offset, 10);

	ac_ui_anchor_set(bounce_code_list_anchor());
	$("loadingBar").className = "ac_block";
	ac_ajax_call_cb(this.ajaxURL, this.ajaxAction, paginateCB, this.id, bounce_code_list_sort, bounce_code_list_offset, this.limit, bounce_code_list_filter);

	$("list").className = "ac_block";
}

function bounce_code_list_clear() {
	bounce_code_list_sort = "01";
	bounce_code_list_offset = "0";
	bounce_code_list_filter = "0";
	$("list_search").value = "";
	bounce_code_search_defaults();
	ac_ui_anchor_set(bounce_code_list_anchor());
}

function bounce_code_list_search() {
	var post = ac_form_post($("list"));
	ac_ajax_post_cb("api.php", "bounce_code.bounce_code_filter_post", bounce_code_list_search_cb, post);
}

function bounce_code_list_search_cb(xml) {
	var ary = ac_dom_read_node(xml);

	bounce_code_list_filter = ary.filterid;
	ac_ui_anchor_set(bounce_code_list_anchor());
}

function bounce_code_list_chsort(newSortId) {
	var oldSortId = ( bounce_code_list_sort.match(/D$/) ? bounce_code_list_sort.substr(0, 2) : bounce_code_list_sort );
	var oldSortObj = $('list_sorter' + oldSortId);
	var sortObj = $('list_sorter' + newSortId);
	// if sort column didn't change (only direction [asc|desc] did)
	if ( oldSortId == newSortId ) {
		// switching asc/desc
		if ( bounce_code_list_sort.match(/D$/) ) {
			// was DESC
			newSortId = bounce_code_list_sort.substr(0, 2);
			sortObj.className = 'ac_sort_asc';
		} else {
			// was ASC
			newSortId = bounce_code_list_sort + 'D';
			sortObj.className = 'ac_sort_desc';
		}
	} else {
		// remove old bounce_code_list_sort
		if ( oldSortObj ) oldSortObj.className = 'ac_sort_other';
		// set sort field
		sortObj.className = 'ac_sort_asc';
	}
	bounce_code_list_sort = newSortId;
	ac_ui_api_call(jsSorting);
	ac_ui_anchor_set(bounce_code_list_anchor());
	return false;
}

function bounce_code_list_discern_sortclass() {
	if (bounce_code_list_sort_discerned)
		return;

	var elems = $("list_head").getElementsByTagName("a");

	for (var i = 0; i < elems.length; i++) {
		var str = sprintf("list_sorter%s", bounce_code_list_sort.substring(0, 2));

		if (elems[i].id == str) {
			if (bounce_code_list_sort.match(/D$/))
				elems[i].className = "ac_sort_desc";
			else
				elems[i].className = "ac_sort_asc";
		} else {
			elems[i].className = "ac_sort_other";
		}
	}

	bounce_code_list_sort_discerned = true;
}

{/literal}
