{jsvar name=configured var=$configured}
{jsvar name=fields var=$fields}
{jsvar name=cfields var=$cfields}
{jsvar name=require_name var=$require_name}
{jsvar name=lists_select var=$lists_select}
{jsvar name=source_select var=$source_select}

{if isset($sessionlists)}
{jsvar name=sessionlists var=$sessionlists}
sessionlists = sessionlists.toString().split(",");
{/if}

var currentfield = null;

var subscriber_import_no_lists = '{"Please select a list."|alang|js}';
var subscriber_import_no_data = '{"You did not enter any data to import."|alang|js}';
var subscriber_import_no_external = '{"You did not enter an External Service."|alang|js}';
var subscriber_import_no_source = '{"You did not complete setting up the External Service."|alang|js}';
var subscriber_import_no_name = '{"Your list(s) require that all subscribers have a name."|alang|js}\n\n{"Either map a field to the first or last name or edit your list settings to not require a name."|alang|js}';
var subscriber_import_hr_no_url = '{"You did not enter the URL of your Highrise service."|alang|js}';
var subscriber_import_hr_no_token = '{"You did not enter the API token of your Highrise user account."|alang|js}';
var subscriber_import_cfield_no_title = '{"You did not enter the name of your new custom field."|alang|js}';
var subscriber_import_google1 = '{"You have not yet authorized access to your Google account."|alang|js}';
var subscriber_import_freshbooks1 = '{"You did not enter your Freshbooks account."|alang|js}';
var subscriber_import_freshbooks2 = '{"You have not yet authorized access to your Freshbooks account."|alang|js}';
var subscriber_import_salesforce1 = '{"You did not enter your Salesforce username."|alang|js}';
var subscriber_import_salesforce2 = '{"You did not enter your Salesforce password."|alang|js}';
var subscriber_import_salesforce3 = '{"You did not enter your Salesforce token."|alang|js}';
var subscriber_import_sugarcrm1 = '{"You did not enter your SugarCRM URL."|alang|js}';
var subscriber_import_sugarcrm2 = '{"You did not enter your SugarCRM username."|alang|js}';
var subscriber_import_sugarcrm3 = '{"You did not enter your SugarCRM password."|alang|js}';
var subscriber_import_zohocrm2 = '{"You did not enter your Zoho CRM username."|alang|js}';
var subscriber_import_zohocrm3 = '{"You did not enter your Zoho CRM password."|alang|js}';
var subscriber_import_microsoftcrm1 = '{"You did not enter your Microsoft CRM username."|alang|js}';
var subscriber_import_microsoftcrm2 = '{"You did not enter your Microsoft CRM password."|alang|js}';
var subscriber_import_microsoftcrm3 = '{"You did not enter your Microsoft CRM domain."|alang|js}';
var subscriber_import_microsoftcrm4 = '{"You did not enter your Microsoft CRM organization."|alang|js}';
var subscriber_import_capsule1 = '{"You did not enter your Capsule application name."|alang|js}';
var subscriber_import_capsule2 = '{"You did not enter your Capsule API token."|alang|js}';
var subscriber_import_capsule3 = '{"Please leave out the capsulecrm.com portion of the URL. Only include the account name."|alang|js}';
var subscriber_import_tactile1 = '{"You did not enter your Tactile CRM application name."|alang|js}';
var subscriber_import_tactile2 = '{"You did not enter your Tactile CRM API token."|alang|js}';
var subscriber_import_zendesk1 = '{"Please enter your Zendesk account name."|alang|js}';
var subscriber_import_zendesk2 = '{"Please enter your Zendesk username."|alang|js}';
var subscriber_import_zendesk3 = '{"Please enter your Zendesk password."|alang|js}';
var subscriber_import_wufoo1 = '{"Please enter your Wufoo account name."|alang|js}';
var subscriber_import_wufoo2 = '{"Please enter your Wufoo API Key."|alang|js}';
var subscriber_import_batchbook1 = '{"Please enter your Batchbook account."|alang|js}';
var subscriber_import_batchbook2 = '{"Please enter your Batchbook API key/token."|alang|js}';

var cfield_column_index = null;

{literal}

var import_type = "file"; // default on page load

var subimport = {};

function import_process(loc, hist) {
	
	subimport.advoption = function(val) {
		if (val == 3) {
			$J("#all_lists").show();
		} else {
			$J("#all_lists").hide();
			// if "All Lists" is checked, uncheck it, otherwise it will be checked and hidden, thus the page will not alert anything if nothing else is checked
			if ($J("#check_0").attr("checked") == "checked") {
				$J("#check_0").click();
			}
		}
	};
	
	subimport.listcheck = function(val) {
		if (val == 0) {
			$J("div#listdiv input[name!='into[0]']").attr("checked", false);
		} else {
			$J("div#listdiv input[name='into[0]']").attr("checked", false);
		}
	};
	
	return;
	
	if (loc) {
		if ( $("external_div_" + loc) ) {
			$("from_external").checked = "checked";
			import_set_from( $("from_external") );
			set_external(loc);
		}
		else {
			if ( $("from_" + loc) ) {
				$("from_" + loc).checked = "checked";
				import_set_from( $("from_" + loc) );
			}
		}
	}
}

function import_submit_step1_new() {
	var post = ac.prepare('#importCfgForm');
	post.from = import_type;
	$J("#from").val(import_type);

	var lists = [];
	$J(".checkInput").each(function() {
		var id_split = this.id.split("_"); // example ID: "check_17"
		var id = id_split[1]; // obtain just the ID
		if (this.checked) lists.push(id);
	});

	if (lists.length == 0) {
		alert(subscriber_import_no_lists);
		return false;
	}

	post.lists = lists;

	if (post.from == 'file') {
		// don't do any checks if file, let server handle it
	}
	else if (post.from == 'text') {
		// check if text field is left empty
		if ( ac_str_trim(post.text) == '' ) {
			alert(subscriber_import_no_data);
			return false;
		}
	}
	else if (post.from == 'external') {
		// check the global external service settings
		if (post.external == '') {
			alert(subscriber_import_no_external);
			return false;
		}
		if (post.source == '') {
			alert(subscriber_import_no_source);
			return false;
		}
		// check specific connectors
		if (post.external == 'hr') {
			if ( !ac_str_is_url(post.hr_url) ) {
				alert(subscriber_import_hr_no_url);
				return false;
			}
			if (ac_str_trim(post.hr_api) == '') {
				alert(subscriber_import_hr_no_token);
				return false;
			}
		}
		else if (post.external == 'google_contacts') {
			if (post.google_contacts_connected == '0') {
				alert(subscriber_import_google1);
				return false;
			}
		}
		else if (post.external == 'google_spreadsheets') {
			if (post.google_spreadsheets_connected == '0') {
				alert(subscriber_import_google1);
				return false;
			}
		}
		else if (post.external == 'freshbooks') {
			if (ac_str_trim(post.freshbooks_account) == '') {
				alert(subscriber_import_freshbooks1);
				return false;
			}
		}
		else if (post.external == 'tactile') {
			if (ac_str_trim(post.tactile_app) == '') {
				alert(subscriber_import_tactile1);
				return false;
			}
			if (ac_str_trim(post.tactile_token) == '') {
				alert(subscriber_import_tactile2);
				return false;
			}
		}
		else if (post.external == 'capsule') {
			if (ac_str_trim(post.capsule_app) == '') {
				alert(subscriber_import_capsule1);
				return false;
			}
			var regex_match = new RegExp(".capsulecrm.com");
			if ( post.capsule_app.match(regex_match) ) {
				alert(subscriber_import_capsule3);
				return false;
			}
			if (ac_str_trim(post.capsule_token) == '') {
				alert(subscriber_import_capsule2);
				return false;
			}
		}
		else if (post.external == 'zohocrm') {
			if (ac_str_trim(post.zohocrm_username) == '') {
				alert(subscriber_import_zohocrm2);
				return false;
			}
			if (ac_str_trim(post.zohocrm_password) == '') {
				alert(subscriber_import_zohocrm3);
				return false;
			}
		}
		else if (post.external == 'sugarcrm') {
			if (ac_str_trim(post.sugarcrm_url) == '') {
				alert(subscriber_import_sugarcrm1);
				return false;
			}
			if (ac_str_trim(post.sugarcrm_username) == '') {
				alert(subscriber_import_sugarcrm2);
				return false;
			}
			if (ac_str_trim(post.sugarcrm_password) == '') {
				alert(subscriber_import_sugarcrm3);
				return false;
			}
		}
		else if (post.external == 'salesforce') {
			if (ac_str_trim(post.salesforce_username) == '') {
				alert(subscriber_import_salesforce1);
				return false;
			}
			if (ac_str_trim(post.salesforce_password) == '') {
				alert(subscriber_import_salesforce2);
				return false;
			}
			if (ac_str_trim(post.salesforce_token) == '') {
				alert(subscriber_import_salesforce3);
				return false;
			}
		}
		else if (post.external == 'zendesk') {
			if (ac_str_trim(post.zendesk_account) == '') {
				alert(subscriber_import_zendesk1);
				return false;
			}
			if (ac_str_trim(post.zendesk_username) == '') {
				alert(subscriber_import_zendesk2);
				return false;
			}
			if (ac_str_trim(post.zendesk_password) == '') {
				alert(subscriber_import_zendesk3);
				return false;
			}
		}
		else if (post.external == 'wufoo') {
			if (ac_str_trim(post.wufoo_account) == '') {
				alert(subscriber_import_wufoo1);
				return false;
			}
			if (ac_str_trim(post.wufoo_apikey) == '') {
				alert(subscriber_import_wufoo2);
				return false;
			}
		}
		else if (post.external == 'batchbook') {
			if (post.batchbook_version == "new") {
				post.batchbook_account = post.batchbook_account_new;
				post.batchbook_token = post.batchbook_token_new;
			}
		
			if (ac_str_trim(post.batchbook_account) == '') {
				alert(subscriber_import_batchbook1);
				return false;
			}
			if (ac_str_trim(post.batchbook_token) == '') {
				alert(subscriber_import_batchbook2);
				return false;
			}
		}

		if ( $("import_loader_external_options") ) {
			$("import_loader_filters").hide();
			$("import_loader").show();
		}
		else {
			$("import_loader_filters").show();
			$("import_loader").hide();
		}

		$J("form[name=importCfgForm]").submit();
		return;
	}

	$J("import_loader").show();
	$J("form[name=importCfgForm]").submit();
}

function import_submit_step_filters() {
	// clicking "Continue" button from Filters modal
	$('import_loader_external_options').hide();
	$J("form[name=importCfgForm]").submit();
}

function import_back() {
	if ( $('step3') && $('step3').style.display != "none" ) {
		$('step2').show();
		$('step3').hide();
	} else {
		window.location.href = "main.php?action=subscriber_import";
	}
}

function import_set_from(obj) {
	var newval = obj.value;
	var post = ac_form_post($('importCfgForm'));

	if (newval == "file")
		$("import_file").show();
	else
		$("import_file").hide();

	if (newval == "text")
		$("import_text").show();
	else
		$("import_text").hide();

	if (newval == "external") {
		$("import_external").show();
		// But don't show the external_box_configs div yet...
	} else {
		$("import_external").hide();
		$('external_box_configs').hide();
	}
}

function set_external(newval) {
	$("external").value = newval;
	var allsources = [ 'hr', 'google_contacts', 'google_spreadsheets', 'freshbooks', 'salesforce', 'sugarcrm', 'zohocrm', /*'microsoftcrm',*/ 'capsule', 'tactile', 'batchbook', 'heapcrm', 'zendesk', 'wufoo' ];
	if ( ac_js_admin.brand_links == 1 ) allsources.push('hd');
	for ( var i = 0; i < allsources.length; i++ ) {
		var s = allsources[i];
		if ( !$('external_div_' + s) ) continue;
		$('external_div_' + s).className = ( newval == s ? 'import_external_source_selected' : 'import_external_source_notselected' );
		if (newval == s)
			$('external_box_' + s).show();
		else
			$('external_box_' + s).hide();
	}
	$('external_box_configs').show();
	$('external_form_' + newval).show();
	$('external_config_' + newval).hide();
	import_type = 'external'; // reset the global var
}

function import_batchbook_version_change(version) {
	$J("#batchbook_version_new").hide();
	$J("#batchbook_version_classic").hide();
	$J("#batchbook_version_" + version).show();
}

/*
   not used yet
   */
function connect2external(ext) {
	var post = ac_form_post($('importCfgForm'));
	post.external = ext;

	ac_ui_api_call(jsLoading, 600);
	ac_ajax_call_cb(
			'api.php',
			'subscriber_import.subscriber_import_connect',
			function (xml) {
				var ary = ac_dom_read_node(xml);
				ac_ui_api_callback();
				if ( ary.succeeded == 1 ) {
					ac_result_show(ary.message);
					$('external_form_' + ext).hide();
					$('external_config_' + ext).show();
				} else {
					ac_error_show(ary.message);
				}
			},
			post
			);
}


function import_run(isTest) {
	// check if there are no duplicate mappings
	var selects = $$('#importRunForm select');
	var selected = [ ];
	for ( var i = 0; i < selects.length; i++ ) {
		if ( ac_array_has(selected, selects[i].value) ) {
			alert(importDuplicateMapping);
			selects[i].focus();
			return false;
		}
		if ( selects[i].value != 'DNI' ) selected.push(selects[i].value);
	}
	// check if all required (standard) fields are mapped
	for ( var i in fields ) {
		var f = fields[i];
		if ( typeof(f) != 'function' ) {
			if ( fields[i].req && !ac_array_has(selected, fields[i].id) ) {
				alert(sprintf(importMissingMapping, fields[i].name + ' (' + fields[i].id + ')'));
						return false;
						}
						}
						}
						if ( require_name ) {
							if ( !ac_array_has(selected, fields[2].id) && !ac_array_has(selected, fields[3].id) ) {
								alert(subscriber_import_no_name);
								return false;
							}
						}
						$('step2').hide();

						ac_loader_show(jsImporting);
						//ac_ui_api_call(jsImporting, 60);
						// show iframe
						ac_progressbar_set("progressBar", 0);
						if (isTest)
						$('importRunNotice').hide();
						else
						$('importRunNotice').show();

						$('step3').show();

						$('importRunResult').hide();
						$('importRunFrame').show();
						$('importRunFrame').height = ( isTest ? '300' : '1' );
						$('importRunFrame').width = ( isTest ? '100%' : '1' );
						// set test flag
						$('importRunType').value = ( isTest ? 1 : 0 );
						// submit form
						$('importRunForm').submit();
						// show progress bar
						if ( !isTest ) {
							$("buttons").hide();
							//$("backlink").className = "ac_block";
						}
						return false;
}

function import_progressbar_callback(ary) {
	if ( parseInt(ary.percentage) == 100 ) {
		// stop the progressbar
		ac_progressbar_unregister("progressBar");
		$('importRunNotice').hide();
		$('importRunResult').show();
		ac_loader_hide();
	}
}


function import_report() {
	// fetch import logs
	ac_ui_api_call(jsLoading, 60);
	ac_ajax_call_cb('api.php', 'subscriber_import.ac_import_report', import_report_cb, processID);
	return false;
}

function import_report_cb(xml) {
	var ary = ac_dom_read_node(xml, ( paginator_b64 ? ac_b64_decode : null ));
	ac_ui_api_callback();

	// hack?
	if ( ary.counts[0] ) ary.counts = ary.counts[0];
	if ( ary.lists[0]  ) ary.lists  = ary.lists[0];

	// fill the modal panel

	// set counts
	ary.total0 = parseInt(ary.total, 10);
	ary.total  = parseInt($('report_count').innerHTML, 10);
	ary.total1 = ary.total - ary.total0;
	for ( var i in ary.counts ) {
		if ( typeof ary.counts[i] != 'function' ) {
			ary.counts[i] = parseInt(ary.counts[i], 10);
		}
	}

	$('report_count0').innerHTML = ary.total0;
	$('report_count1').innerHTML = ary.total1;

	if ( typeof(ihook_import_report) == 'function' ) ihook_import_report(ary);

	// show it
	//ac_dom_toggle_display('import_report', 'block');
	ac_dom_display_block('import_report');
}

function cfield_open(obj) {
	if ( obj.value != 'NEW' ) return;
	field_modal_lists = sessionlists;
	$("field_modal_addtype").show();
	currentfield = obj.id;
	return;

	var cfield_column_index = obj.id.replace('column_', '');
	obj.selectedIndex = 0;
	$('cfield_select_source').value = obj.id; // save the select list ID that triggered this
	ac_dom_display_block('import_cfield');
	$('cfield_title').focus();
}

function field_modal_save_after(ary) {
	var selects = $$('#importRunForm select');
	for ( var i = 0; i < selects.length; i++ ) {
		var s = selects[i];
		var s_current = s.value;
		var colid = s.id.replace('column_', '');
		var rel = $('customfieldsoptgroup_' + colid);
		rel.appendChild(Builder.node('option', { value: ary.id }, [ Builder._text(ary.title) ]));
		if (s.id != currentfield) {
			s.value = s_current;
		} else {
			// select the new custom field <option>
			s.value = ary.id;
		}
	}
}

function advanced_options_toggle() {
	if ( $('advanced').style.display == 'none' ) {
		$('advanced').show();
	}
	else {
		$('advanced').hide();
	}
}

function google_spreadsheets_toggle(spreadsheet_id) {
	var selects = $("import_loader_external_options").getElementsByTagName("select");
	var worksheet_select = selects[1];
	var worksheet_select_options = worksheet_select.getElementsByTagName("option");
	for (var i = 0; i < worksheet_select_options.length; i++) {
		var current_option = worksheet_select_options[i];
		if (current_option.value.substring(0, spreadsheet_id.length) == spreadsheet_id) {
			// show the <option>s that belong to selected spreadsheet
			current_option.style.display = "";
			current_option.selected = true;
		}
		else {
			// hide the <option> if it does not belong to chosen spreadsheet
			current_option.style.display = "none";
		}
	}
}

function import_freshbooks_authorize() {
	if ( !$('freshbooks_account').value ) {
		alert(subscriber_import_freshbooks1);
		return;
	}
	window.location = ac_js_site.p_link + '/admin/main.php?action=subscriber_import&freshbooks_account=' + $('freshbooks_account').value;
}

function toggle_import_type(import_type_) {
	$J("#file_option").toggle(import_type_ == 'file');
	$J("#file_option_link").toggle(import_type_ == 'text');
	$J("#copy_option").toggle(import_type_ == 'text');
	$J("#copy_option_link").toggle(import_type_ == 'file');
	import_type = import_type_; // reset the global var
}

var subimport = {};

subimport.showresults = function(importid) {
	ac.get("Import::log", [importid], function(data) {
		$J("#successnum").html(data.success);
		$J("#failnum").html(data.failed);

		if (data.failed > 0) {
			var area = [];
			$J(data.failrows).each(function() {
				area.push(sprintf("%s (%s)", this.email, this.msg));
			});

			$J("#failrows").html(area.join("\n"));
			$J("#failures").show();
		} else {
			$J("#failures").hide();
		}

		$J("#resultsModal").modal('show');
	});
};

subimport.refreshimports = function() {
	ac.get("Import::current", [], function(data) {
		$J(".importListDropdown").empty();

		var goagain = false;

		for (var i = 0; i < data.length; i++) {
			var imp = data[i];
			var html = "";

			html = sprintf("<li><div class='listName'>%s</div>", imp.name);
			html += "<div class='statusInfo'>";

			if (imp.percent >= 100) {
				html += "<a href='#' onclick='subimport.showresults(%s)'>" + {/literal}'{"View Results"|alang|js}'{literal} + "</a>";
			} else {
				goagain = true;
				html += sprintf("<div class='graphcont'><div class='graph'><strong class='bar' style='width:%s%%;'>%s%%</strong></div></div>", imp.percent, imp.percent);
			}

			html += "</div></li>";

			$J(".importListDropdown").append(html);
		}

		if (!ac.hidden(".importListDropdown") && goagain) {
			ac.wait(5, function() {
				subimport.refreshimports();
			});
		}
	});
};

{/literal}
