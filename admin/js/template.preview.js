var template_preview_txt1 = '{"Edit"|alang}';
var template_preview_txt2 = '{"Export"|alang}';
var template_preview_txt3 = '{"Delete"|alang}';
var template_preview_txt4 = '{"XML"|alang}';
var template_preview_txt5 = '{"HTML"|alang}';
var template_preview_txt6 = '{"Delete Selected"|alang}';

var template_which = "tdisplay";
var template_searchkey = "";
var template_style = "images";
var template_tag = "";
var template_list = 0;
var template_offset = 0;
var template_length = 50;
var preview_timeout = 0;
var templates_selected = 0;

{literal}

function template_preview_anchor() {
	return "preview";
}

function template_display() {
	var searchkey = template_searchkey;

	templates_selected = 0;

	if (template_which == "cdisplay")
		ac_ajax_call_cb("api.php", "template.template_selector_cdisplay", ac_ajax_cb(template_cdisplay_cb), template_tag, searchkey, template_offset, template_length);
	else
		ac_ajax_call_cb("api.php", "template.template_selector_tdisplay", ac_ajax_cb(template_tdisplay_cb), template_tag, searchkey, template_offset, template_length, "template", template_list);
}

function template_tdisplay_cb(ary) {
	if (typeof ary.row == "undefined")
		return;

	if (ary.loadmore)
		$("loadmore").show();
	else
		$("loadmore").hide();

	for (var i = 0; i < ary.row.length; i++) {
		var clickelem = "a";
		var clickprops = {
			href: "#",
			onclick: sprintf("window.open('htmlpreview.php?t=%s', 'preview', 'height=600,width=800,menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes'); return false", ary.row[i].id)
		};

		var graphic = "images/preview-16-16.gif";

		if (!ary.row[i].haspreview) {
			graphic = "images/dimmed-preview-16-16.gif";
			clickelem = "span";
			clickprops = {};
		}

		if (template_style == "list") {
			$("choices").appendChild(Builder.node("div", { id: sprintf("choice_template_%s", ary.row[i].id), style: "height: 16px; margin: 5px; padding: 5px;", className: "campaign_template_notselected" }, [
				Builder.node("div", [
					Builder.node(clickelem, clickprops, [
						Builder.node("img", { src: graphic, style: "float: right", border:0 })
					]),
					Builder.node("div", { style: "float: left; width: 95%; cursor: pointer", onclick: sprintf("template_basetemplate(%s);", ary.row[i].id), ondblclick: sprintf("template_basetemplate(%s);", ary.row[i].id)  }, ac_str_shorten(ary.row[i].name, 25))
				])
			]));
		}
		else {

			var preview_div = Builder.node("div",
			  {
					id: sprintf("choice_template_%s", ary.row[i].id),
					style: "background-image: url(" + ary.row[i].url + ");",
					className: "template_preview campaign_template_notselected template_notselected",
					onclick: "template_choice_event(event, this.id); return false;"
				},
				[

					Builder.node("div", {id: sprintf("choice_template_hover_%s", ary.row[i].id), className: "template_preview_hover", style: "display: none;"}, [

            Builder.node("div", { id: sprintf("choice_template_hover_menu_%s", ary.row[i].id), className: "template_preview_hover_menu" }, [

              Builder.node(
                "span",
                {id: "choice_template_hover_edit", onclick: "window.location = '#form-" + ary.row[i].id + "';"},
                Builder.node("span", {style: "margin: 8px 0 0 8px; display: block;"}, jsEdit)
              ),
              Builder.node("img", {src: "images/template_preview_button.png", style: "margin-left: 113px;", onclick: "window.open('htmlpreview.php?t=" + ary.row[i].id + "', 'preview', 'height=600,width=800,menubar=no,location=no,resizable=yes,scrollbars=yes,status=yes'); return false;"})


            ])

  				])

		    ]
			);

			var template_name_div = Builder.node("div", {className: "tpl_selector_name", style: "background-color: white; text-align: center; width: 190px;"}, [
				Builder.node(clickelem, clickprops, [
					/*Builder.node("img", { src: graphic, style: "float: right", border:0 })*/
				]),
				Builder.node("div", { style: "font-size: 12px;" }, ac_str_shorten(ary.row[i].name, 25))
			]);
			
			var complete_div = Builder.node("div", { style: "float: left; height: 310px;" }, [preview_div, template_name_div]);

			$("choices").appendChild(complete_div);

			$J("#choice_template_" + ary.row[i].id).bind(
			  "mouseenter mouseleave",
				function (event) {
			  	template_choice_event(event, $(this).id);
				}
			);
		}
	}

	if (ary.row.length == 0) {
		$("loadmore").hide();
		if (template_tag == "" && template_list == 0) {
			$("emptysearch").hide();
			$("emptylist").show();
		} else {
			$("emptysearch").show();
			$("emptylist").hide();
		}
		$('template_clearfix').hide();
	} else {
		$("emptysearch").hide();
		$("emptylist").hide();
		$('template_clearfix').show();
	}

	template_basetemplate($("basetemplateid").value);
}

function template_basetemplate(id) {
	if ($("choice_template_" + id)) {
		$("basemessageid").value = 0;
		$("basetemplateid").value = id;

		if ( $J("#choice_template_" + id).hasClass("campaign_template_notselected") && $J("#choice_template_" + id).hasClass("template_notselected") ) {
			// select (highlight it)
			$J("#choice_template_" + id).removeClass("campaign_template_notselected template_notselected");
			$J("#choice_template_" + id).addClass("campaign_template_selected");
			templates_selected++;
		}
		else if ( $J("#choice_template_" + id).hasClass("campaign_template_selected") ) {
			// de-select it
			$J("#choice_template_" + id).removeClass("campaign_template_selected");
			$J("#choice_template_" + id).addClass("campaign_template_notselected template_notselected");
			templates_selected--;
		}
	}
}

function template_basemessage(id) {
	if ($("choice_campaign_" + id)) {
		// Only do this if we have it--which we may not if we've filtered things down...
		if ($("choice_campaign_" + $("basemessageid").value)) {
			if ($("basemessageid").value != 0)
				$("choice_campaign_" + $("basemessageid").value).className = "campaign_template_notselected";
		}
		$("basetemplateid").value = 0;
		$("basemessageid").value = id;
		$("choice_campaign_" + id).className = "campaign_template_selected";
	}
}

function typesearch(filter) {
	if (preview_timeout > 0)
		window.clearTimeout(preview_timeout);

	preview_timeout = window.setTimeout(sprintf("template_searchkey = '%s'; template_clear(); template_display()", encodeURIComponent(filter)), 500);
}

function template_listchange(listid) {
	template_list = listid;
	template_clear();
	template_display();
}

function template_usetag(tagid) {
	template_tag = tagid;

	// We had selected past campaigns, then; switch to images when we use the tag.
	if (template_which != "tdisplay") {
		template_style = "images";
		$("span_images").className = "campaign_template_textselected";
		$("span_list").className = "campaign_template_textnotselected";
	}

	template_which = "tdisplay";
	template_searchkey = "";

	template_clear();
	template_display();
}

function template_view(style) {
	template_clear();

	template_offset = 0;
	template_length = 25;
	template_style = style;

	template_display();

	$("span_images").className = "campaign_template_textnotselected";
	$("span_list").className = "campaign_template_textnotselected";

	$("span_" + style).className = "campaign_template_textselected";
}

function template_clear() {
	ac_dom_remove_children($("choices"));
}

function template_loadmore() {
	template_offset += template_length - 1;
	template_display();
}

function template_choice_event(event, elementid) {
	// elementid is expected to be something like: choice_template_43, where 43 is the actual template ID
	var regex = new RegExp("[0-9]+$");
	var templateid = regex.exec(elementid);
	if (event.type == "mouseover" || event.type == "mouseenter") {
		$J("#choice_template_hover_" + templateid).fadeIn("fast", function() {

		});
	}
	else if (event.type == "mouseout" || event.type == "mouseleave") {
		$J("#choice_template_hover_" + templateid).fadeOut("fast", function () {

		});
	}
	else if (event.type == "click") {

	}
}

function template_choice_hover_menu_export(templateid) {
	$("choice_template_hover_menu_" + templateid).style.bottom = "30px";
	$("choice_template_hover_export_" + templateid).show();
}

function template_choice_selected() {
	var selected = $$(".campaign_template_selected");
	var selected_ids = [];
	for (var i = 0; i < selected.length; i++) {
		var current = selected[i];
		// current.id will look like: choice_template_33 (whatever ID it is)
		var regex = new RegExp("[0-9]+$");
		// just grab the ID off the end
		var current_id = regex.exec(current.id);
		selected_ids.push(current_id);
	}
	return selected_ids;
}

function stop_propagation(event) {
	if (window.event) {
		event.cancelBubble = true;
	}
	else if (event.stopPropagation) {
		event.stopPropagation();
	}
}

{/literal}
