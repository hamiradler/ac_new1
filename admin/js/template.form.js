var template_form_str_cant_insert = '{"You do not have permission to add templates"|alang|js}';
var template_form_str_cant_update = '{"You do not have permission to edit templates"|alang|js}';
var template_form_str_cant_find   = '{"Template not found."|alang|js}';
var template_form_str_noname      = '{"You must give this template a name."|alang|js}';

{if $__ishosted}
var ishosted = true;
{else}
var ishosted = false;
{/if}

var campaign_spamcheck_str_scoreheader = '{"Your campaign scored %s on our spam filter tests."|alang|js}';
var campaign_spamcheck_str_msgheader = '{"Message #%s (%s): %s/%s"|alang|js}';
var campaign_spamcheck_str_noissues = '{"No issues found."|alang|js}';

var template_spamcheck_high = false;

{jsvar name=lists_count var=$lists_count}
{jsvar name=fields var=$fields}
{jsvar name=template_tags var=$tags}

//ac_editor_init_word_object.plugins += ",ota_personalize,ota_activerss,ota_conditional";
//ac_editor_init_word_object.theme_advanced_buttons1_add += ",ota_personalize,ota_activerss,ota_conditional";
ac_editor_init_word_object.language = _twoletterlangid;
ac_editor_init_word_object.plugins += ",fullpage";
ac_editor_init_word();

{literal}

// put available tags in array for use later (auto-complete)
var available_tags = [];
for (var i in template_tags) {
	if (template_tags[i].tag) {
		available_tags.push(template_tags[i].tag);
	}
}

var customFieldsObj = new ACCustomFields({
	sourceType: 'INPUT',
	sourceId: 'p',
	api: 'list.list_field_update',
	responseIndex: 'fields',
	includeGlobals: 0,
	additionalHandler: function(ary) {
		// deal with personalization tags
		form_editor_sender_personalization(ary.personalizations, $('personalizelist'));
		/*
		if (ac_editor_is("templateEditor")) {
			ac_editor_toggle('templateEditor', ac_editor_init_word_object);
			ac_editor_toggle('templateEditor', ac_editor_init_word_object);
		}
		*/
	}
});
customFieldsObj.addHandler('personalizelist', 'links');
customFieldsObj.addHandler('conditionalfield', 'pers');

var template_form_id = 0;

function template_form_defaults() {
	$("form_id").value = 0;

	$('nameField').value = '';
	$('subjectField').value = '';
	if ( template_listfilter && typeof(template_listfilter) == 'object' ) {
		ac_form_select_multiple($('parentsList'), template_listfilter);
	} else if ( template_listfilter > 0 ) {
		if ( $('p_' + template_listfilter) ) $('p_' + template_listfilter).checked = true;
	} else {
		var list_inputs = $('parentsList_div').getElementsByTagName('input');
		// check all lists first
		for (var i = 0; i < list_inputs.length; i++) {
			list_inputs[i].checked = true;
		}
	}

	if (!lists_count) {
		$('template_scope_all').checked = true;
		$('template_form_lists').hide();
		$('template_scope_specific').disabled = true;
	}
	else {
		if ( $('template_scope_specific') ) $('template_scope_specific').checked = true;
		$('template_form_lists').show();
	}
	
	form_editor_defaults('template', 'html', [ 'subscriber', 'sender', 'system' ]);
	form_editor_personalization('conditionalfield', [ 'subscriber' ], 'text', '');
}

function template_form_load(id) {
	template_form_defaults();
	template_form_id = id;

	// adjust the file upload iframe src attribute, so it includes the form ID

	if (id > 0) {
		if (ac_js_admin.pg_template_edit != 1) {
			ac_ui_anchor_set(template_list_anchor());
			alert(template_form_str_cant_update);
			return;
		}

	// show Export and Delete buttons
		$("template_form_options_other").show();

		ac_ui_api_call(jsLoading);
		$("form_submit").className = "ac_button_update";
		$("form_submit").value = jsUpdate;
		ac_ajax_call_cb("api.php", "template.template_select_row", template_form_load_cb, id);
	} else {
		if (ac_js_admin.pg_template_add != 1) {
			ac_ui_anchor_set(template_list_anchor());
			alert(template_form_str_cant_insert);
			return;
		}

		// hide Export and Delete buttons
		$("template_form_options_other").hide();

		// get custom fields for the preselect value for add
		customFieldsObj.fetch(0);

		$J("#tags").tagit({
			availableTags: available_tags
		});

		$("form_submit").className = "ac_button_add";
		$("form_submit").value = jsAdd;
		$("form").className = "ac_block";
	}
}

function template_form_load_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();
	if ( !ary.id ) {
		ac_error_show(template_form_str_cant_find);
		ac_ui_anchor_set(template_preview_anchor());
		return;
	}
	template_form_id = ary.id;
	template_delete_imgurl = ary.url;
	$("form_id").value = ary.id;
	$('nameField').value = ary.name;
	$('subjectField').value = ary.subject;

	var list_inputs = $('parentsList_div').getElementsByTagName('input');

	// if NOT global, uncheck all lists, then check the ones that are saved
	if (ary.listslist != 0) {
		// uncheck all lists first
		for (var i = 0; i < list_inputs.length; i++) {
			list_inputs[i].checked = false;
		}
		var lists = (ary.listslist + '').split('-');
		for (var i = 0; i < lists.length; i++) {
			if ( $('p_' + lists[i]) ) $('p_' + lists[i]).checked = true;
		}

		if ( $('template_scope_specific') ) $('template_scope_specific').checked = true;
	}
	else {
		// check all lists
		for (var i = 0; i < list_inputs.length; i++) {
			list_inputs[i].checked = true;
		}

		// show/hide related sections
		if ( $('template_scope_all') ) $('template_scope_all').checked = true;
		$('template_form_lists').hide();
	}

	ary.html = ary.content;

	// display tags
	// https://github.com/aehlke/tag-it
	$J("#tags").tagit("removeAll");
	if (typeof(ary.tags) == "object") {
		// multiple tags saved
		var tags_string = ary.tags.join(", ");
		$("tags_hidden").innerHTML = tags_string;
		$("tags").value = tags_string;
		for (var i = 0; i < ary.tags.length; i++) {
			$J("#tags").tagit("createTag", ary.tags[i]);
		}
	}
	else if (typeof(ary.tags) == "string") {
		// one (or no) tags saved
		if (ary.tags) {
			$("tags_hidden").innerHTML = ary.tags;
			$("tags").value = ary.tags;
			$J("#tags").tagit("createTag", ary.tags);
		}
	}

	$J("#tags").tagit({
		availableTags: available_tags
	});

	$("form").className = "ac_block";

	form_editor_update('template', ary);
	form_editor_update_fields('conditionalfield', ary, '');
}

function template_form_save(id, altcb) {
	var post = ac_form_post($("form"));
	ac_ui_api_call(jsSaving);

	altcb = altcb || template_form_save_cb;

	if ($("nameField").value == '') {
		alert(template_form_str_noname);
		return;
	}

	// get tags
	var tag_elements = $J(".tagit-label");
	var tag_names = [];
	for (var i = 0; i < tag_elements.length; i++) {
		var tag_name = tag_elements[i].innerHTML;
		tag_names.push(tag_name);
	}
	post.tags = tag_names;

	if (id > 0)
		ac_ajax_post_cb("api.php", "template.template_update_post", altcb, post);
	else
		ac_ajax_post_cb("api.php", "template.template_insert_post", altcb, post);
}

function template_form_save_dummy(xml) {
	ac_ui_api_callback();
}

function template_form_save_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);

		ac_ui_anchor_set(template_preview_anchor());
		template_clear();
		template_display();
	} else {
		ac_error_show(ary.message);
	}
}

function template_form_lists_toggle_scope(value) {
	if (value == 'all') {
		$('template_form_lists').hide();
	}
	else {
		$('template_form_lists').show();
	}
}

function template_send_emailtest() {
	var post = {};

	if(!post.testemail)
		post.testemail = document.getElementById('subscriberEmailTestField').value;

	// check for email validity
	var test_email = $('subscriberEmailTestField').value; // use DOM ID to grab value
	post.testemail = test_email; // reset post value (shows up as "undefined" in IE, for some reason)
	if ( !ac_str_email(test_email) ) {
		alert(strEmailNotEmail);
		$('subscriberEmailTestField').focus();
		return;
	}

	post.templateid = template_form_id;
	ac_ui_api_call(jsSending, 60);
	ac_ajax_handle_text = template_send_emailtest_cb_txt;
	ac_ajax_post_cb("api.php", "campaign.campaign_send_emailtest", template_send_emailtest_cb, post);
}

function template_send_emailtest_cb_txt(txt) {
	ac_ui_error_mailer(txt);
}

function template_send_emailtest_cb(xml) {
	// now reset the text handler
	ac_ajax_handle_text = null;
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded != "0") {
		ac_result_show(ary.message);
		// don't show any results here
	} else {
		ac_error_show(ary.message);
	}
}

function template_open_inboxpreview() {
	var url = 'preview_client.php?t=' + template_form_id;
	var w = window.open(
		url,
		'previewclient',
		'width=900,height=600,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,copyhistory=yes,resizable=no'
	);
	if ( !w ) {
		alert('Popup could not be opened.');
	}
	if ( w.focus ) w.focus();
	return;
}

function template_run_spamcheck() {
	//ac_ui_api_call(jsChecking, 60);
	//ac_ajax_handle_text = campaign_spamcheck_cb_txt;
	ac_ajax_call_cb("api.php", "campaign.campaign_template_spamcheck", template_spamcheck_cb, template_form_id);
}

function template_spamcheck_cb(xml) {
	// now reset the text handler
	ac_ajax_handle_text = null;
	var ary = ac_dom_read_node(xml);
	//ac_ui_api_callback();

	if (ary.succeeded == 1) {

		var total_score  = 0;
		var total_maxres = 0;
		template_spamcheck_high = false;

		//var message_rules = {};

		var rel = $('spamcheck_table');

		for ( var index = 0; index < ary.messages.length; index++ ) {
			//message_rules[index] = ary.messages[index];

			if ( !ary.messages[index].succeeded ) {
				alert(ary.messages[index].message);
				continue;
			}

			// get the subject
			var subject = $("subjectField").value;

			// get the score
			//var score = parseFloat(ary.messages[index].score);
			total_score  += parseFloat(ary.messages[index].score);
			total_maxres += parseFloat(ary.messages[index].max);
			// set the rules
			var mpart_alt_diff = null;
			var mime_html_only = null;
			var rules = 0;
			var finalrules = [];
			for ( var i = 0; i < ary.messages[index].rules.length; i++ ) {
				var r = ary.messages[index].rules[i];
				if ( r.score == '0.0' || r.score == '0.00' || r.score == '0' ) continue;
				r.score = parseFloat(r.score);
				if ( r.name == 'MPART_ALT_DIFF' ) {
					if ( !isNaN(parseInt(mime_html_only, 10)) ) {
						finalrules[mime_html_only].score += r.score;
						continue;
					}
					mpart_alt_diff = finalrules.length;
				} else if ( r.name == 'MIME_HTML_ONLY' ) {
					if ( !isNaN(parseInt(mpart_alt_diff, 10)) ) {
						finalrules[mpart_alt_diff].score += r.score;
						continue;
					}
					mime_html_only = finalrules.length;
				}
				finalrules.push(r);
				rules++;
			}

			var msgbox  = [];
			// build header
			if ( ary.messages.length > 1 ) {
				msgbox.push(Builder.node('strong', [ sprintf(campaign_spamcheck_str_msgheader, index+1, subject, ary.messages[index].score, ary.messages[index].max) ]));
				msgbox.push(Builder.node('br'));
			}
			if ( rules > 0 ) {
				// build the header
				var rulebox = [];

				// build table body
				for ( var i = 0; i < finalrules.length; i++ ) {
					var r = finalrules[i];
					rulebox.push(
						Builder.node(
							'tr',
							[
								Builder.node('td', { width: 25, title: r.name }, [ Builder._text(r.score) ]),
								Builder.node('td', { title: r.name }, [ Builder._text(r.descript) ]),
							]
						)
					);
				}

				// build table
				msgbox.push(Builder.node('table', { width: '100%', border: 0, cellpadding: 0, cellspacing: 0 }, rulebox));

				template_spamcheck_high = template_spamcheck_high || parseFloat(ary.messages[index].score) / parseFloat(ary.messages[index].max) >= .40;

			} else {
				msgbox.push(Builder.node('div', [ campaign_spamcheck_str_noissues ]));
			}
			rel.appendChild(Builder.node('div', { className: 'spamcheck_message', style: 'margin-top:10px;' }, msgbox));
		}

		// set the score
		var scorestring = total_score + '/' + total_maxres;
		//$('spamcheck_score').innerHTML = scorestring;
		$('spamcheck_result').innerHTML = sprintf(campaign_spamcheck_str_scoreheader, scorestring);
		// set the scene
		if ( total_score > 0 ) {
			//$('spamcheck_details_link').show();
			$('spamcheck_details').show();
			//$('campaign_spamcheck').className = 'campaign_summary_top';
		} else {
			$('spamcheck_result').innerHTML = {/literal}'{"Your campaign passed our basic spam filter checks."|alang}'{literal};
			//$('spamcheck_details').hide();
			//$('campaign_spamcheck').className = 'campaign_summary';
		}

		$('template_check_spam').show();

		if ( template_spamcheck_high ) {
			$('spamcheck_details').className = 'campaign_summary_red_bottom';
		} else {
			$('spamcheck_details').className = 'campaign_summary_bottom';
		}

		//ac_result_show(ary.message);
	} else {
		ac_error_show(ary.message);
	}
}

function campaign_spamcheck_cb_txt(txt) {
	ac_ui_error_mailer(txt);
}

{/literal}
