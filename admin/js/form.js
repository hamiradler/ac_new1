var lists = {jsvar var=$lists};
var form_str1 = '{"Changes saved"|alang|js}';

{literal}

var total_lists_user = 0;
for (var i in lists) {
	total_lists_user++;
}

function form_switch_page(val) {
	if (val == "form")
		window.location.href = "main.php?action=form#list-01-0-0";
	else
		window.location.href = "main.php?action=" + val;
}

function form_switch_list(val) {
	if (val != "0") {
		window.location.href = "main.php?action=form&listid=" + val;
	}
	else {
		window.location.href = "main.php?action=form";
	}
}

function form_setdefault(id) {
	ac.post("form.form_set_default", { key: "public.defaultform", val: id });
	ac_result_show(form_str1);
}

{/literal}
