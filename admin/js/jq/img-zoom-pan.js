/* 
SlashC jQuery Image Zoom and Pan plugin
hi@slashc.com, www.slashc.com
*/
(function($)
{	
	/* plugin methods */
	var methods =
	{
		/* intialization */
		init : function()
		{
			return this.each(function()
			{
				var izp = $(this); // image zoom pan instance		
				if(!izp.data('d'))
				{
					var img = izp.find('img').first().css('z-index', 2),
					//miw = img.width(), // minimum width
					//mih = img.height(), // minimum height
					miw = '103', // minimum width
					mih = '73', // minimum height
					
					imgz = img.next() // zoom image
					.css('position', 'absolute').css('z-index', 1); // move zoomed image under
				//	var maw = imgz.width(), // maximum width
				//	mah = imgz.height(); // maximum height
					
					var maw = '200', // maximum width
					mah = '250'; // maximum height
					
					
					izp.css('width', miw).css('height', mih) // set viewport width and height
					.data('d', { imgz: imgz, img: img, miw: miw, mih: mih, maw: maw, mah: mah }) // store data
					.bind('mousemove.slashcImgZoomPan', $.proxy(function(e) // pan on mouse move
					{
						var d = this.data('d'), 
						tol = 20, // edge tolerance 
						px = Math.max(0, Math.min(1, (e.pageX - this.offset().left - tol) / (d.miw - 2 * tol))), // x percentage
						py = Math.max(0, Math.min(1, (e.pageY - this.offset().top - tol) / (d.mih - 2 * tol))); // y percentage
						d.imgz.css('left', (d.miw - d.maw) * px).css('top', (d.mih - d.mah) * py); // pan
					}, izp)).bind('mouseenter.slashcImgZoomPan', $.proxy(function(e) // on mouse enter
					{
						this.data('d').img.stop().fadeTo(750, 0); // fade small image out
					}, izp)).bind('mouseleave.slashcImgZoomPan', $.proxy(function() // on mouse leave
					{
						this.data('d').img.stop().fadeTo(250, 1); // fade small image in
					}, izp));
				}
			});
		}
	};
	
	/* Image zoom and pan plugin */
	$.fn.slashcImgZoomPan = function(p1, p2)
	{
		/* p1 - method name, p2 - method options */
		/* p1 - options for init method, p2 - undefined */
		if (typeof p2 === 'undefined')
		{
			if (typeof p1 === 'undefined') return methods.init.call(this);
			else if (methods[p1]) return methods[p1].call(this);
			else return methods.init.call(this, p1);			
		}
		else if (methods[p1])
		{
			if (p2) return methods[p1].call(this, p2);
			else return methods[p1].call(this);
		}
		return this;
	};
	
	/* Attach plugin to all the HTML instances */
	$(window).load(function()
	{
		$('.slashc-img-zoom-pan').slashcImgZoomPan();
	});
	
})(jQuery);