{literal}

function service_form_fields_save(button_label) {
	var post = ac_form_post($("form"));
	ac_ui_api_call(jsSaving);
	var all_custom_fields = [];
	var custom_fields_global_inputs = $("custom_fields_trs").getElementsByTagName("input");
	var custom_fields_list_inputs = $("custom_fields_table").getElementsByTagName("input");
	for (var i = 0; i < custom_fields_global_inputs.length; i++) {
		var current = custom_fields_global_inputs[i];
		if (current.type != "checkbox") continue;
		// only add to new array if checked, and there is a value
		if (current.checked) {
			all_custom_fields.push(current.value);
		}
		else {
			all_custom_fields.push(0);
		}
	}
	for (var i = 0; i < custom_fields_list_inputs.length; i++) {
		var current = custom_fields_list_inputs[i];
		if (current.type != "checkbox") continue;
		// only add to new array if checked, and there is a value
		if (current.checked) {
			all_custom_fields.push(current.value);
		}
		else {
			all_custom_fields.push(0);
		}
	}
	post.custom_fields_all = all_custom_fields;
	post.button_label = button_label;
	ac_ajax_post_cb("api.php", "service.service_fields_save", service_form_fields_save_cb, post);
}

function service_form_fields_save_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded && ary.succeeded == "1") {
		ac_result_show(ary.message);
		if (ary.save_type == "save") {
			// connected - have custom fields mapped
			service_form_fields_display_additional_fields(ary.additional_fields);
		}
		else if (ary.save_type == "reset") {
			// not connected - custom fields no longer mapped
			var ary_empty = [];
			service_form_fields_display_additional_fields(ary_empty);
		}
		alert(service_form_str12);
	}
	else {
		ac_error_show(ary.message);
	}
}

function service_form_fields_display_additional_fields(fields) {
	if ( typeof(fields) != "undefined" ) {
		var fields_ul = $("service_fields_connected").getElementsByTagName("ul");
		fields_ul = fields_ul[0];
		if ( typeof(fields[0]) != "undefined" ) {
			var additional_fields = fields[0];
			ac_dom_remove_children(fields_ul);
			for (var i in additional_fields) {
				if (typeof(i) == "string") {
					if ( $("fields_all_" + additional_fields[i]) ) {
						var field_title = $("fields_all_" + additional_fields[i]).value;
						var fields_ul_li = Builder.node(
						  "li",
						  {},
						  [
						    Builder.node("span", { id: "custom" + additional_fields[i] + "Field_textbox_value", className: "customfield_external_value" }, i),
						    Builder._text(" (" + service_form_str_shopify9 + ")"),
						    Builder._text(" => "),
						    Builder.node( "span", {}, field_title + " (" + service_form_str_shopify7 + " " + additional_fields[i] + ")" )
						  ]
						);
						fields_ul.appendChild(fields_ul_li);
					}
				}
			}
		}
		else {
			// before removing everything, re-fill textboxes with values that were just mapped, so its easier to EDIT
			var mapped_fields_external = $$(".customfield_external_value");
			for (var i = 0; i < mapped_fields_external.length; i++) {
				var current = mapped_fields_external[i];
				var regex = new RegExp("[0-9]+"); // find just the numeric portion of the string (IE: custom10Field_textbox_value)
				var old_mapped_field_id = regex.exec(current.id);
				$("custom" + old_mapped_field_id + "Field_textbox").value = current.innerHTML;
			}
			ac_dom_remove_children(fields_ul);
		}
		var webhook_fields_inputs = $("service_webhook_fields_savebutton").getElementsByTagName("input");
		var save_button = webhook_fields_inputs[1];
		// count how many <li>'s were added
		var fields_ul_lis = fields_ul.getElementsByTagName("li");
		if (!fields_ul_lis.length) {
			// none added
			$("fields_form_submit_type").value = "save";
			$("service_webhook_customfields").show();
			$("service_fields_connected").hide();
			save_button.value = service_form_str11;
		}
		else {
			// some were added
			$("fields_form_submit_type").value = "reset";
			$("service_webhook_customfields").hide();
			$("service_fields_connected").show();
			save_button.value = service_form_str10;
		}
	}
}

{/literal}