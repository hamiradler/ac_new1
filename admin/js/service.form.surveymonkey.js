{literal}

var surveymonkey_access_token = "";

function service_form_load_cb_surveymonkey(ary) {
	if ( $("form_submit") ) $('form_submit').hide();
	$("service_surveymonkey").show();
	if (typeof(ary.data[0]) != "undefined" && typeof(ary.data[0].oauth_token) != "undefined") {
		// connected - there is a token saved already
		surveymonkey_access_token = ary.data[0].oauth_token;
		$("service_surveymonkey_oauth_unconfirmed").hide();
		$("service_surveymonkey_oauth_confirmed").show();
		if (typeof(ary.data[0].surveys) == "undefined" || ary.data[0].surveys.length == 0) {
			// re-fetch survey data
			service_form_surveymonkey_get_surveys();
		}
		else {
			// display cached survey data
			var survey_uls = $("service_surveymonkey_surveys").getElementsByTagName("ul");
			var survey_ul = survey_uls[0]; // first one in the div
			service_form_surveymonkey_display_surveys(ary.data[0].surveys, survey_ul, "li");
		}
	}
	else {
		// not connected to surveymonkey at all (no oauth token)
		// re-check every few seconds
		timeouts.push( setTimeout( function() { service_form_load(6); }, 5000) );
		$("service_surveymonkey_oauth_unconfirmed").show();
		$("service_surveymonkey_oauth_confirmed").hide();
	}
}

function service_form_surveymonkey_logout() {
	ac_ajax_call_cb("api.php", "service.service_surveymonkey_logout", service_surveymonkey_logout_cb);
}

function service_form_surveymonkey_get_surveys() {
	if (!surveymonkey_access_token) return;
	ac_ui_api_call(jsLoading);
	ac_ajax_call_cb("api.php", "service.service_surveymonkey_get_surveys", service_form_surveymonkey_get_surveys_cb, surveymonkey_access_token);
}

function service_form_surveymonkey_get_surveys_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();
	
	if (ary.succeeded && ary.succeeded == "1") {
		//ac_result_show(ary.message);
		if (typeof(ary.surveys) != "undefined") {
			if (ary.surveys.length) {
				// display fetched survey data
				var survey_uls = $("service_surveymonkey_surveys").getElementsByTagName("ul");
				var survey_ul = survey_uls[0]; // first one in the div
				service_form_surveymonkey_display_surveys(ary.surveys, survey_ul, "li");
			}
			else {
				$("service_surveymonkey_surveys").hide();
				$("service_surveymonkey_surveys0").show();
			}
		}
	}
	else {
		//ac_error_show(ary.message);
	}
}

function service_form_surveymonkey_display_surveys(surveys, parent, element) {
	//$("service_surveymonkey_surveys").show();
	$("service_surveymonkey_surveys0").hide();
	ac_dom_remove_children(parent);
	for (var i = 0; i < surveys.length; i++) {
		var survey = surveys[i];
		var survey_element = Builder.node(element, {}, survey.title);
		if (typeof(survey.url) != "undefined") {
			var survey_url = survey.url;
			var survey_element_a = Builder.node("a", { href: survey_url, target: "_blank" }, survey.title);
			survey_element = Builder.node(element, {}, survey_element_a); // re-create it
		}
		parent.appendChild(survey_element);
	}
}

function service_surveymonkey_logout_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();
	service_form_load(6);
}
{/literal}