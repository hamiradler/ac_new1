{literal}

function service_form_load_cb_shopify(ary) {
	$("service_shopify").show();
	if ( $("form_submit") ) $("form_submit").hide();
	// if webhook ID is in the serialized data
	if (typeof(ary.data[0]) != "undefined" && typeof(ary.data[0].webhook_id) != "undefined") {
		// connected - values have been saved already
		$("service_shopify_auth_link").hide();
		$("shopify_form_webhook_id").value = ary.data[0].webhook_id;
		$("service_shopify_shop_url").value = ary.data[0].shop_url;
		$("service_shopify_api_password").value = ary.data[0].api_password;
		$("service_shopify_password_no").hide();
		$("service_shopify_password_yes").show();
		$("shopify_form_submit").value = service_form_str_shopify1;
		$("service_shopify_lists").hide();
		$("service_shopify_lists_connected").show();
		$("service_shopify_lists_line1").hide();
		$("service_shopify_customfields").hide();
		$("service_shopify_customfields_connected").show();
		$("service_shopify_import_previous_div").hide();
		var shopify_lists = ary.data[0].lists + "";
		if ( shopify_lists.indexOf(",") != -1 ) {
			shopify_lists = shopify_lists.split(",");
		}
		else {
			shopify_lists = [shopify_lists];
		}
		for (var i = 0; i < shopify_lists.length; i++) {
			// pre-check the lists they already chose/saved
			if ( $("p_shopify_" + shopify_lists[i]) ) $("p_shopify_" + shopify_lists[i]).checked = true;
		}
		service_form_shopify_display_lists(ary.data[0].lists);
		service_form_shopify_display_additional_fields(ary.data[0].additional_fields);
	}
	else {
		// not connected (but shop and API key might still be saved - if they connected once before, then disconnected)

		// no data and have not authorized on Shopify yet
		$("service_shopify_auth_link").hide();
		$("service_shopify_lists_line1").hide();
		$("service_shopify_lists").hide();
		$("service_shopify_customfields").hide();
		$("service_shopify_import_previous_div").hide();
		$("shopify_form_submit_div").hide();
		
		$("service_shopify_lists_connected").hide();
		$("service_shopify_customfields_connected").hide();

		// if they Disconnect, we still leave the URL and API password as serialized in the database
		if (typeof(ary.data[0]) != "undefined" && typeof(ary.data[0].shop_url) != "undefined") {
			$("service_shopify_shop_url").value = ary.data[0].shop_url;
		}
		if (typeof(ary.data[0]) != "undefined" && typeof(ary.data[0].api_password) != "undefined") {
			$("service_shopify_api_password").value = ary.data[0].api_password;
		}
		
		if ($("service_shopify_shop_url").value != "" && $("service_shopify_api_password").value != "") {
			// if they both have values, then they already authorized on Shopify, and can now choose their Lists and custom field mapping
			$("service_shopify_lists_line1").show();
			$("service_shopify_lists").show();
			$("service_shopify_customfields").show();
			$("service_shopify_import_previous_div").show();
			$("shopify_form_submit_div").show();
		}
		
		var inputs = $("parentsList_div_shopify").getElementsByTagName("input");
		inputs[0].checked = true; // check the first list by default
		customFieldsObj.fetch(0);
	}
}

function service_form_shopify_save() {
	var post = ac_form_post($("form"));
	ac_ui_api_call(jsSaving);
	var all_custom_fields = [];
	var custom_fields_global_inputs = $("custom_fields_global_shopify").getElementsByTagName("input");
	var custom_fields_list_inputs = $("custom_fields_list_shopify").getElementsByTagName("input");
	for (var i = 0; i < custom_fields_global_inputs.length; i++) {
		var current = custom_fields_global_inputs[i];
		// only add to new array if checked
		if (current.checked) {
			all_custom_fields.push(current.value);
		}
		else {
			all_custom_fields.push(0);
		}
	}
	for (var i = 0; i < custom_fields_list_inputs.length; i++) {
		var current = custom_fields_list_inputs[i];
		// only add to new array if checked
		if (current.checked) {
			all_custom_fields.push(current.value);
		}
		else {
			all_custom_fields.push(0);
		}	
	}
	post.custom_fields_all = all_custom_fields;
	ac_ajax_post_cb("api.php", "service.service_shopify_save", service_form_shopify_save_cb, post);
}

function service_form_shopify_save_cb(xml) {
	var ary = ac_dom_read_node(xml);
	ac_ui_api_callback();

	if (ary.succeeded && ary.succeeded == "1") {
		ac_result_show(ary.message);
		if (ary.save_type == "add webhook") {
			// connected
			$("service_shopify_auth_link").hide();
			$("service_shopify_password_no").hide();
			$("service_shopify_password_yes").show();
			$("shopify_form_submit").value = service_form_str_shopify1;
			$("shopify_form_webhook_id").value = ary.webhook_id;
			$("service_shopify_lists").hide();
			$("service_shopify_lists_connected").show();
			$("service_shopify_lists_line1").hide();
			$("service_shopify_customfields").hide();
			$("service_shopify_customfields_connected").show();
			$("service_shopify_import_previous_div").hide();
			service_form_shopify_display_lists(ary.lists);
			service_form_shopify_display_additional_fields(ary.additional_fields);
		}
		else if (ary.save_type == "remove webhook") {
			// not connected
			service_form_load(5);
			$("shopify_form_submit").value = service_form_str_shopify2;
			/*
			$("service_shopify_auth_link").show();
			$("shopify_form_webhook_id").value = "";
			$("service_shopify_password_yes").hide();
			$("shopify_form_submit").value = service_form_str_shopify2;
			$("service_shopify_lists").hide();
			$("service_shopify_lists_connected").hide();
			$("service_shopify_lists_line1").hide();
			$("service_shopify_customfields").hide();
			$("service_shopify_customfields_connected").hide();
			$("service_shopify_import_previous_div").hide();
			$("shopify_form_submit_div").hide();
			*/
		}
	}
	else {
		ac_error_show(ary.message);
	}
}

//after saving/connecting with Shopify, we display the lists in a <div> and <ul>
//this loads on page refresh, and also after clicking Save (no page refresh, just DOM updates)
function service_form_shopify_display_lists(lists) {
	if ( typeof(lists) != "undefined" ) {
		var lists_ul = $("service_shopify_lists_connected").getElementsByTagName("ul");
		lists_ul = lists_ul[0];
		ac_dom_remove_children(lists_ul);
		
		lists = lists + "";
		if (lists.indexOf(",") != -1) {
			lists = lists.split(",");
		}
		else {
			lists = [lists];
		}

		for (var i = 0; i < lists.length; i++) {
			var list_name = $("lists_all_" + lists[i]).value;
			var lists_ul_li = Builder.node("li", { style: "margin: 0 0 7px 0;" }, list_name);
			lists_ul.appendChild(lists_ul_li);
		}
	}
}

// after saving/connecting with Shopify, we display the custom field mappings in a <div> and <ul>
// this loads on page refresh, and also after clicking Save (no page refresh, just DOM updates)
function service_form_shopify_display_additional_fields(fields) {
	if ( typeof(fields) != "undefined" ) { 
		var additional_fields = fields[0];
		// fields_ul is the <ul> where we show existing mapped custom fields
		var fields_ul = $("service_shopify_customfields_connected").getElementsByTagName("ul");
		fields_ul = fields_ul[0];
		ac_dom_remove_children(fields_ul);
		for (var i in additional_fields) {
			if ( typeof(i) == "string" && (i == "company" || i == "billing_address" || i == "phone" || i == "order_number" || i == "total_price") ) {
				var field_title = $("fields_all_" + additional_fields[i]).value;
				var fields_ul_li = Builder.node("li", { style: "margin: 0 0 7px 0;" }, i + " (" + service_form_str_shopify6 + ") => " + field_title + " (" + service_form_str_shopify7 + " " + additional_fields[i] + ")");
				fields_ul.appendChild(fields_ul_li);
			}
		}
		// count how many <li>'s were added
		var fields_ul_lis = fields_ul.getElementsByTagName("li");
		if (!fields_ul_lis.length) {
			// append a default message, if none
			var fields_ul_li = Builder.node("li", {}, service_form_str_shopify8);
			fields_ul.appendChild(fields_ul_li);
		}
	}
}

function service_shopify_shop_url_check(input_value) {
	
	var valid = ( input_value && ac_str_is_url(input_value) );
	
	var regex_match = new RegExp("/$");
	
	// if they have already connected
	if ( $("shopify_form_webhook_id").value ) valid = false;
	
	if (valid) {
		if ( input_value.match(regex_match) ) {
			input_value = input_value.replace(regex_match, "");
		}
		$("service_shopify_auth_link").show();
		var landing_page = "https://shopify.ac-app.com?";
		landing_page += "ac_user=" + ac_js_admin.id;
		landing_page += "&ac_url=" + encodeURIComponent(ac_js_site.p_link);
		landing_page += "&ac_serial=" + serial_hash;
		landing_page += "&shopify_url=" + encodeURIComponent(input_value);
		// example URL (goes to landing page first, then saves some info, then redirects to Shopify store where they authorize):
		// http://paininthetech.com/ac_apps/shopify/?fromac=1&shopify_url=http%3A%2F%2Fspencer-simonis-and-kuphal2795.myshopify.com%2Fadmin%2Fapi%2Fauth%3Fapi_key%3Da9e07120f6eccc2b98d9da316bf7e9da
		$("service_shopify_auth_url").href = landing_page;
	}
	else {
		$("service_shopify_auth_link").hide();
		$("service_shopify_auth_url").href = "";
	}
}
{/literal}