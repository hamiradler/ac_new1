var campaign_message_str_noinfo = '{"Please enter something in the From Name and From Email fields before continuing."|alang|js}';
var campaign_message_str_nomail_from = '{"Please enter a valid email address in the From Email field before continuing."|alang|js}';
var campaign_message_str_nomail_reply2 = '{"Please enter a valid email address in the Reply To field before continuing."|alang|js}';
var campaign_message_str_nosubj = '{"Please enter something in the Subject field before continuing."|alang|js}';
var campaign_message_str_nobody = '{"Please enter something in the content area of your message before continuing."|alang|js}';
var campaign_message_str_activerss_nofeed = '{"You do not have RSS personalization tags included into your messsage, or you have more than one RSS feed referenced. Please make sure that you have one RSS feed placed into your message before continuing."|alang|js}';
var campaign_message_str_activerss_noloop = '{"It seems like you haven't entered any LOOP tags to display your RSS feed items. Please add LOOP tags to your message before continuing."|alang|js}';
var campaign_message_str_activerss_mismatch = '{"It seems like you haven't entered valid RSS personalization tags. Some tags seem to be missing, and your RSS feed would not be properly displayed. Please correct this before continuing."|alang|js}';

var campaign_fetch_str_insert = '{"Insert"|alang|js}';
var campaign_fetch_str_save = '{"Save"|alang|js}';

ac_editor_init_word_object.plugins += ",fullpage";
{jsvar var=$message name=message_obj}
{jsvar var=$message.html name=default_editor_value}
{jsvar var=$serial_hash name=serial_hash}
{jsvar var=$surveymonkey_pass name=surveymonkey_pass}

{literal}

var surveymonkey_access_token = "";
var timeouts = []; // used for any setTimeouts (such as SurveyMonkey integration)

function campaign_validate(aftersave) {
	if ( aftersave == "next" ) {
		if ($("campaign_fromname").value == "" || $("campaign_fromemail").value == "") {
			alert(campaign_message_str_noinfo);
			return false;
		}

		$("campaign_fromemail").value = ac_str_trim($("campaign_fromemail").value);
		if (!ac_str_email($("campaign_fromemail").value)) {
			alert(campaign_message_str_nomail_from);
			$("campaign_fromemail").focus();
			return false;
		}
		$("campaign_reply2").value = ac_str_trim($("campaign_reply2").value);
		if ($("campaign_reply2").value != '' && !ac_str_email($("campaign_reply2").value)) {
			alert(campaign_message_str_nomail_reply2);
			$("campaign_reply2").focus();
			return false;
		}

		if ($("fetchwhen").value == "now" && $("campaign_subject").value == "") {
			alert(campaign_message_str_nosubj);
			return false;
		}

		var html = ac_form_value_get($("messageEditor"));
		if (strip_tags(html) == "" && !html.match(/<img/i) ) {
			alert(campaign_message_str_nobody);
			return false;
		}

		// activerss checks
		if ( campaign_obj.type == 'activerss' ) {
			if ( !ac_str_is_url($("activerss_url").value) ) {
				alert(strURLNotURL);
				$('activerss_url').focus();
				return false;
			}
			// check if exactly one rss feed is found
			var occur_feed_opening = html.match(/%RSS-FEED\|/g);
			var occur_feed_closing = html.match(/%RSS-FEED%/g);
			var occur_loop_opening = html.match(/%RSS-LOOP\|/g);
			var occur_loop_closing = html.match(/%RSS-LOOP%/g);
			// check if ONLY ONE of feed opening and closing tags is found
			if ( !occur_feed_opening || !occur_feed_closing || occur_feed_opening.length != 1 || occur_feed_closing.length != 1 ) {
				alert(campaign_message_str_activerss_nofeed);
				return false;
			}
			// check if NO loop tags are found
			if ( !occur_loop_opening || !occur_loop_closing ) {
				alert(campaign_message_str_activerss_noloop);
				return false;
			}
			// check if any mismatched tags are found
			if ( occur_feed_opening.length != occur_feed_closing.length || occur_loop_opening.length != occur_loop_closing.length ) {
				alert(campaign_message_str_activerss_mismatch);
				return false;
			}
		}
	}

	return true;
}

function campaign_changed() {
	if (ac_form_value_get($("messageEditor")) != default_editor_value && !default_editor_value.match(/^fetch:/)) {
		return true;
	}

	return false;
}

function campaign_changed_safe() {
	default_editor_value = ac_form_value_get($("messageEditor"));
}

// set onload
ac_dom_onload_hook(campaign_changed_safe);

function campaign_attachafile() {
	$("attachafile").hide();
	$("attachmentsBox").show();
}

function campaign_managetext(val) {
	$("campaign_managetextid").value = val;

	$("askmanagetext").hide();
	$("willmanagetext").hide();

	if (val)
		$("willmanagetext").show();
	else
		$("askmanagetext").show();
}

// Personalization
function campaign_personalization_show(id) {
	$("personalize_subinfo").hide();
	$("personalize_message").hide();
	$("personalize_socmedia").hide();
	$("personalize_other").hide();

	$("subinfo_tab").className = "othertab";
	$("message_tab").className = "othertab";
	$("socmedia_tab").className = "othertab";
	$("other_tab").className = "othertab";

	switch (id) {
		case "personalize_subinfo":
			$("subinfo_tab").className = "currenttab"; break;
		case "personalize_message":
			$("message_tab").className = "currenttab"; break;
		case "personalize_socmedia":
			$("socmedia_tab").className = "currenttab"; break;
		case "personalize_other":
			$("other_tab").className = "currenttab"; break;
	}

	$(id).show();
}

function campaign_personalization_open() {
	$('personalize4').value = 'html';
	$('personalize2').value = 'messageEditor';

	if (surveymonkey_pass) campaign_personalization_service_get(6);
	
	$('message_personalize').toggle();
}

function campaign_personalize_build(val) {
	// what type of code to build
	var type = $('personalize4').value;
	// now handle custom (html?) cases
	var text = '';
	// only today tag should be reset
	if ( val.match( /^%TODAY[+-]\d+%$/ ) ) {
		val = '%TODAY*%';
	}
	if ( val.match( /^SERVICE-SURVEYMONKEY/ ) ) {
		var split_value = val.split("||"); // IE: survey_url_id + "||" + survey.title
		var link_href = "%SERVICE-SURVEYMONKEY-" + split_value[1] + "%";
		text = strSurveyText + ": " + split_value[2];
		entered = prompt(strEnterText, text);
		if ( !entered ) entered = text;
		val = '<a href="' + link_href + '" title="' + strSurveyText2 + ': ' + split_value[2] + '">' + entered + '</a>';
		return val;
	}
	if ( val == '%CONFIRMLINK%' ) {
		text = strConfirmLinkText;
	} else if ( val == '%UNSUBSCRIBELINK%&ALL' ) {
		text = strUnsubscribeAllText;
	} else if ( val == '%UNSUBSCRIBELINK%' ) {
		text = strUnsubscribeText;
	} else if ( val == '%UPDATELINK%' ) {
		text = strSubscriberUpdateText;
	} else if ( val == '%WEBCOPY%' ) {
		text = strWebCopyText;
	} else if ( val == '%FORWARD2FRIEND%' ) {
		text = strForward2FriendText;
	} else if ( val == '%SOCIALSHARE%' ) {
		//text = strForward2FriendText; // don't prompt for anything, just use val
	} else if ( val == '%TODAY*%' ) {
		var entered = prompt(strEnterRange, '+1');
		if ( !entered ) return;
		if ( !entered.match( /^[-+]?\d+$/ ) ) {
			alert(strEnterRangeInvalid);
			return;
		}
		if ( !entered.match(/^[-+].*$/) ) {
			entered = '+' + entered;
		}
		val = '%TODAY' + entered + '%';
	}
	if ( type == 'html' && text != '' ) {
		entered = prompt(strEnterText, text);
		if ( !entered ) entered = text;
		val = '<a href="' + val + '">' + entered + '</a>';
	}
	return val;
}

function campaign_personalization_insert(value) {
	if ( value == '' ) {
		alert(strPersMissing);
		return;
	}
	// close the modal
	$('message_personalize').toggle();
	// build the code
	var code = campaign_personalize_build(value);
	if ( code == '' ) return;
	// push it into needed editor
	ac_editor_insert($('personalize2').value, ( $('personalize4').value == 'html' ? nl2br(code) : code ));

	// clear any setTimeouts running. (one is used for SurveyMonkey integration)
	campaign_message_cleartimeouts();
}

// Conditional
function campaign_conditional_open() {
	// set type
	$('conditional4').value = 'html';
	$('conditional2').value = 'messageEditor';
	// set data
	$('conditionalfield').value = '';
	$('conditionalcond' ).selectedIndex = 0;
	$('conditionalvalue').value = '';
	// open modal
	$('message_conditional').toggle();
}

function campaign_conditional_insert() {
	if ( $('conditionalfield').value == '' ) {
		alert(strPersMissing);
		$('conditionalfield').focus();
		return;
	}
	// close the modal
	$('message_conditional').toggle();
	// build the code
	var code = campaign_conditional_build();
	if ( code == '' ) return;
	// push it into needed editor
	if ( $('conditional4').value == 'html' ) {
		var ed = tinyMCE.activeEditor;
		ed.execCommand('mceInsertContent', false, nl2br(code));
	} else {
		ac_form_insert_cursor($($('conditional2').value), code);
	}
}

function campaign_conditional_build() {
	// what type of code to build
	var type = $('conditional4').value;
	// what value to use
	var field = $('conditionalfield').value;
	var cond  = $('conditionalcond' ).value;
	var value = $('conditionalvalue').value;
	field = '$' + field.replace(/%/g, '').replace(/-/g, '_');
	value = value.replace(/%/g, '~PERCENT~');
	value = "'" + value.replace(/'/g, '\\\'') + "'";
	if ( cond.indexOf('CONTAINS') != -1 ) {
		var expr = 'in_string(' + value + ', ' + field + ')';
		if ( cond == 'DCONTAINS' ) expr = '!' + expr;
	} else {
		var expr = field + ' ' + cond + ' ' + value;
	}
	var code =
		'%IF ' + expr + '%\n' + editorConditionalText + '\n%ELSE%\n' + editorConditionalElseText + '\n%/IF%\n'
	;
	return code;
}

// ActiveRSS
function activerss_add() {
	// what url to fetch
	var url = $('activerss_url').value;
	if ( !ac_str_is_url(url) ) {
		alert(strURLNotURL);
		$('activerss_url').focus();
		return;
	}
	// how many to show
	if ( !ac_ui_numbersonly($('activerss_items')) ) {
		$('activerss_items').value = 10;
	}

	$('activerss_loading').show();

	// ajax call
	ac_ajax_call_cb("api.php", "activerss.activerss_checkfeed", ac_ajax_cb(activerss_add_cb), ac_b64_encode($("activerss_url").value));
}

function activerss_add_cb(ary) {

	$('activerss_loading').hide();

	if ( ary.succeeded == 1 ) {

		// if some redirection occurred and the URL is now different, update the input field
		if ( ary.url && ary.url != $('activerss_url').value ) {
			$('activerss_url').value = ary.url;
		}

		// what url to fetch
		var url = $('activerss_url').value;
		// how many items to loop through
		var loop = $('activerss_items').value;
		// what to show
		var show = 'NEW'; // ALL/NEW

		var code = campaign_rss_build(url, show, loop);

		$('activerss_preview').value = code;

		// flip the views
		$('activerss_add').hide();
		$('activerss_use').show();

		// push it into needed editor
		//ac_editor_insert("messageEditor", nl2br(code));

		var tbody = $('activerss_tags');
		for ( var i in ary.tags[0] ) {
			var s = ary.tags[0][i];
			if ( typeof s == 'function' ) continue;
			var section_title = 'RSS ' + i.charAt(0).toUpperCase() + i.slice(1);
			if ( i == 'item' ) section_title += ' (these can only be used within %RSS-LOOP% tags)';
			tbody.appendChild(Builder.node('tr', [ Builder.node('th', { colspan: 2 }, [ Builder._text(section_title) ]) ]));
			tbody.appendChild(
				Builder.node(
					'tr',
					[
						Builder.node('th', { width: '30%' }, [ Builder._text("Tag") ]),
						Builder.node('th', { width: '70%' }, [ Builder._text("Example") ])
					]
				)
			);
			for ( var j = 0; j < s.length; j++ ) {
				var t = s[j];
				var cells = [];
				var lnk = Builder.node(
					'a',
					{ href: '#', onclick: "ac_editor_insert('messageEditor', '" + t.key + "');return false;" },
					[ Builder._text(t.key) ]
				);
				cells.push(Builder.node('td', { valign: 'top' }, [ lnk ]));
				cells.push(Builder.node('td', { valign: 'top' }, [ Builder._text(ac_str_shorten(t.val, 300)) ]));
				tbody.appendChild(Builder.node('tr', cells));
			}
		}

		$('activerss_preview').focus();
		$('activerss_preview').select();
	} else {
		ac_error_show(ary.message);
	}
}

function activerss_reset() {
	$('activerss_use').hide();
	$('activerss_add').show();

	$('activerss_url').value = 'http://';
	$('activerss_items').value = 10;

	// try to take out the old one
	var content = ac_form_value_get($('messageEditor'));
	content = content.replace(/%RSS-FEED\|.*%RSS-FEED%/, '');
	ac_form_value_set($('messageEditor'), content);
}

function campaign_activerss_loop_changed() {
	window.setTimeout(
		function() {
			ac_ui_numbersonly($('activerssloop'), true);
		},
		100
		);
}

function campaign_activerss_open(type, insertObj) {
	if ( !insertObj ) insertObj = '';
	// set data
	$('activerssurl').value = 'http://';
	$('activerssloop').value = '10';
	$('activersspreviewbox').className = 'ac_hidden';
	// open modal
	$('message_activerss').toggle();
}

function campaign_activerss_insert() {
	// close the modal
	$('message_activerss').toggle();
	// build the code
	var code = campaign_activerss_build();
	if ( code == '' ) return;
	// push it into needed editor
	ac_editor_insert("messageEditor", nl2br(code));
}

function campaign_activerss_preview() {
	// build the code
	var code = campaign_activerss_build();
	if ( code == '' ) return;
	code = nl2br(code);
	// push it into preview box
	$('activersspreview').value = code;
	$('activersspreviewbox').className = 'ac_block';
}

function campaign_activerss_build() {
	// what url to fetch
	var url = $('activerssurl').value;
	if ( !ac_str_is_url(url) ) {
		alert(strURLNotURL);
		$('activerssurl').focus();
		return '';
	}
	// how many to show
	if ( !ac_ui_numbersonly($('activerssloop')) ) {
		$('activerssloop').value = 0;
	}
	var loop = $('activerssloop').value;
	// what to show
	var show = 'ALL'; // ALL/NEW

	return campaign_rss_build(url, show, loop);
}

function campaign_rss_build(url, show, loop) {
	var code =
		'%RSS-FEED|URL:' + url + '|SHOW:' + show + '%\n\n' + // start feed section
		'%RSS:CHANNEL:TITLE%\n\n' + // print out title
		'%RSS-LOOP|LIMIT:' + loop + '%\n\n' + // start item section
		'%RSS:ITEM:DATE%\n' + // within a section
		'%RSS:ITEM:TITLE%\n' +
		'%RSS:ITEM:SUMMARY%\n' +
		'%RSS:ITEM:LINK%\n\n' +
		'%RSS-LOOP%\n\n' +
		'%RSS-FEED%\n' // end section
	;
	return code;
}

function ac_editor_activerss_click() {
	campaign_activerss_open('html');
}

function campaign_fetch_open() {
	$('message_fetch').show();
}

function campaign_fetch_stop() {
	ac_form_value_set($("messageEditor"), "");
	$("fetchurl").value = "http://";
	$("fetchwhat").value = "http://";
	$("fetchwhen").value = "now";
	$("editorfetch").hide();
	$("editordiv").show();
}

function campaign_fetch_radiochoose(val) {
	if (val == "now")
		$("message_fetch_ok").value = campaign_fetch_str_insert;
	else
		$("message_fetch_ok").value = campaign_fetch_str_save;
}

function campaign_fetch_insert() {
	if ( !ac_str_is_url($("fetchurl").value) ) {
		alert(strURLNotURL);
		$('fetchurl').focus();
		return;
	}

	$("fetchwhen").value = $("fetchnow").checked ? 'now' : 'send';
	$("fetchwhat").value = $("fetchurl").value;

	if ($("fetchsend").checked) {
		ac_form_value_set($("messageEditor"), "fetch:" + $("fetchurl").value);

		$("fetchhelplink").href = $("fetchurl").value;
		$("fetchhelplink").innerHTML = $("fetchurl").value;

		$("message_fetch").hide();
		$("editordiv").hide();
		$("editorfetch").show();
		return;
	}

	// If we get here, that means fetchnow was checked.
	ac_ajax_call_cb("api.php", "message.message_fetch_url", ac_ajax_cb(campaign_fetch_insert_cb), ac_b64_encode($("fetchurl").value), "html");
}

function campaign_fetch_insert_cb(ary) {
	if (ary.data) {
		ac_form_value_set($("messageEditor"), ary.data);
	}

	$("message_fetch").hide();
	$("editordiv").show();
	$("editorfetch").hide();
}

// Editor functions
function campaign_message_toggle_editor(id, action, settings) {
	if ( action == ac_editor_is(id + 'Editor') ) return false;
	ac_editor_toggle(id + 'Editor', settings);
	$(id + 'EditorLinkOn').className  = ( action ? 'currenttab' : 'othertab' );
	$(id + 'EditorLinkOff').className = ( !action ? 'currenttab' : 'othertab' );
	if ( action != ( ac_js_admin.htmleditor == 1 ) )
		$(id + 'EditorLinkDefault').show();
	else
		$(id + 'EditorLinkDefault').hide();
	/*
	if ( !$(id + 'Editor') ) tmpEditorContent = ac_form_value_get($(id + '_form')); else // heavy hack!!!
	tmpEditorContent = ac_form_value_get($(id + 'Editor'));
	*/
	return false;
}

function campaign_message_setdefaulteditor(id) {
	var isEditor = ac_editor_is(id + 'Editor');
	if ( isEditor == ( ac_js_admin.htmleditor == 1 ) ) return false;
	// send save command
	// save new admin limit remotelly
	ac_ajax_call_cb('api.php', 'user.user_update_value', null, 'htmleditor', ( isEditor ? 1 : 0 ));
	$(id + 'EditorLinkDefault').hide();
	ac_js_admin.htmleditor = ( isEditor ? 1 : 0 );
	return false;
}

{/literal}
