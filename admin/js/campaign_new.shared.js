var campaign_obj = {jsvar var=$campaign};

var campaign_step = {jsvar var=$step};

var campaign_header_str_type = '{"Type"|alang|js}';
var campaign_header_str_list = '{"Lists"|alang|js}';
var campaign_header_str_message = '{"Message"|alang|js}';
var campaign_header_str_messages = '{"Messages"|alang|js}';
var campaign_header_str_summary = '{"Summary & Options"|alang|js}';
var campaign_header_str_done = '{"Done"|alang|js}';
var campaign_header_str_noname = '{"Create a New Campaign"|alang|js}';
var campaign_header_str_name = '{"Campaign: %s"|alang|js}';

{literal}

// Set this variable to the milliseconds in which we'll try to automatically save the campaign.
var campaign_save_autotime = 30000;

// Leave this alone, please.  It's the timer id for our auto saves.
var campaign_save_autoid = 0;

// If this variable is true, we should not perform any auto-save.  Very handy
// in avoiding race conditions when beginning to actually POST (by clicking
// Next, Save, etc).
var campaign_save_noautosave = false;

// This can be set to true with the campaign_different function.
var campaign_obviously_changed = false;

// This should be set to true when we save with any action.  If this is false,
// campaign_changed() can be used to trigger warnings.
var campaign_doing_something = false;

// Set this to true in your context JS file if you need a check AJAX function
// run first.  Generally, this isn't necessary, but we do use it for checking
// segments on the list page.
var campaign_save_needscheck = false;

function campaign_save(aftersave) {
	if (campaign_save_needscheck && typeof campaign_save_check == "function") {
		campaign_save_check(aftersave);
		return;
	}

	campaign_doing_something = true;
	campaign_save_noautosave = true;
	$("campaign_aftersave").value = aftersave;

	if (aftersave == "next") {
		if (typeof campaign_validate == "function") {
			if (!campaign_validate(aftersave))
				return;
		}
	}

	if (typeof campaign_fixpost == "function")
		campaign_fixpost();

	campaign_safe();
	$("campaignform").submit();
}

function campaign_save_auto() {
	if (campaign_save_noautosave)
		return;

	var post = $("campaignform").serialize(true);

	// No auto-saves on page 1 or on the tpl selector.
	if (post.action == "campaign_new")
		return;

	// Don't save anything if we don't have a name.
	if (post.id == 0 && typeof post.name != "undefined" && post.name == "") {
		campaign_save_auto_runagain();
		return;
	}

	switch (post.action) {
		case "campaign_new_message":
		case "campaign_new_splitmessage":
			post.html = ac_form_value_get($("messageEditor"));
			break;

		case "campaign_new_text":
		case "campaign_new_splittext":
			post.text = ac_form_value_get($("messageEditor"));
			break;

		default:
			break;
	}

	if (typeof post["attach[]"] != "undefined") {
		post.attach = post["attach[]"];
	}

	post.aftersave = "nothing";
	post.isauto = 1;
	$J(".saving").show("drop", {}, 500);

	// Are we on the template step?  Just skip saving.
	if (window.location.href.match('/_template/')) {
		// Fake the fade-out.
		window.setTimeout('$J(".saving").hide("drop", {}, 500)', 1500);
	}

	ac_ajax_post_cb("api.php", "campaign.campaign_save", ac_ajax_cb(campaign_save_auto_cb), post);
}

function campaign_save_auto_runagain() {
	if (campaign_save_autoid > 0)
		window.clearTimeout(campaign_save_autoid);

	campaign_save_autoid = window.setTimeout('campaign_save_auto()', campaign_save_autotime);
}

function campaign_save_auto_cb(ary) {
	campaign_safe();
	campaign_save_auto_runagain();
	window.setTimeout('$J(".saving").hide("drop", {}, 500)', 1500);
}

function campaign_progress_linkup() {
	// Add links to the progress bar.  This is going to look a bit funky --
	// we're having some fun with fallthroughs.
	switch (campaign_obj.laststep) { 
		case "result": 
		case "summary":
			$J(".step4").wrapInner(campaign_progress_linkstr("summary"));

		case "message":
		case "splitmessage":
		case "splittext":
		case "text":
		case "template":
			$J(".step3").wrapInner(campaign_progress_linkstr("message"));

		case "list":
			$J(".step2").wrapInner(campaign_progress_linkstr("list"));

		default:
			$J(".step1").wrapInner(campaign_progress_linkstr("type"));
	}

	var cn = "active";

	// Let's do this again, but in the other order.
	switch (campaign_obj.laststep) { 
		case "result": 
			$J(".step5").addClass(cn);
			cn = "done";

		case "summary":
			$J(".step4").addClass(cn);
			cn = "done";

		case "message":
		case "splitmessage":
		case "splittext":
		case "text":
		case "template":
			$J(".step3").addClass(cn);
			cn = "done";

		case "list":
			$J(".step2").addClass(cn);
			cn = "done";

		default:
			$J(".step1").addClass(cn);
	}
}

function campaign_progress_linkstr(action) {
	if (action == "message") {
		if (campaign_obj.type == "split")
			action = "splitmessage";
		if (campaign_obj.type == "text")
			action = "text";

		if (campaign_obj.laststep == "template")
			action = "template";
	}

	if (action == "type")
		action = "";
	else
		action = "_" + action;

	return sprintf("<a href='main.php?action=campaign_new%s&id=%d'></a>", action, campaign_obj.id);
}

// Header
function campaign_header_build(highlight, id) {
	var divs = [];

	var div;
	var inner;
	var max = campaign_header_max();

	if (campaign_obj.name == "")
		div = Builder.node("h1", campaign_header_str_noname);
	else
		div = Builder.node("h1", sprintf(campaign_header_str_name, campaign_obj.name));

	divs.push(div);

	// Turn off the site name div; kind of a hack.  We hope they are using our
	// theme_header_software class, but it's possible they aren't.
	var sitename = $$(".theme_header_software");

	if (typeof sitename[0] != "undefined") {
		sitename[0].innerHTML = "&nbsp;";
	}

	if (max < 4)
		inner = Builder.node("a", { href: sprintf("main.php?action=campaign_new%s", id > 0 ? "&id=" + id : "") }, [ campaign_header_str_type ]);
	else
		inner = campaign_header_str_type;
	div = Builder.node("div", inner);
	if (highlight == 0)
		div.className = "selected";
	divs.push(div);

	if (max >= 1 && max < 4)
		inner = Builder.node("a", { href: sprintf("main.php?action=campaign_new_list&id=%s", id) }, [ campaign_header_str_list ]);
	else
		inner = campaign_header_str_list;

	div = Builder.node("div", inner);
	if (highlight == 1)
		div.className = "selected";
	divs.push(div);

	// Link to template; if we already chose a template, that will move us to message (or text, if a text based campaign).
	if (max >= 2 && max < 4) {
		if (campaign_obj.type == "split") {
			inner = Builder.node("a", { href: sprintf("main.php?action=campaign_new_splitmessage&id=%s", id) }, [ campaign_header_str_messages ]);
		} else {
			if (campaign_step == "template")
				inner = Builder.node("a", { href: sprintf("main.php?action=campaign_new_template&id=%s", id) }, [ campaign_header_str_message ]);
			else {
				if (campaign_obj.type == "text")
					inner = Builder.node("a", { href: sprintf("main.php?action=campaign_new_text&id=%s", id) }, [ campaign_header_str_message ]);
				else
					inner = Builder.node("a", { href: sprintf("main.php?action=campaign_new_message&id=%s", id) }, [ campaign_header_str_message ]);
			}
		}
	}
	else
		inner = campaign_header_str_message;

	div = Builder.node("div", inner);
	if (highlight == 2)
		div.className = "selected";
	divs.push(div);

	if (max >= 3 && max < 4)
		inner = Builder.node("a", { href: sprintf("main.php?action=campaign_new_summary&id=%s", id) }, [ campaign_header_str_summary ]);
	else
		inner = campaign_header_str_summary;

	div = Builder.node("div", inner);
	if (highlight == 3)
		div.className = "selected";
	divs.push(div);

	inner = campaign_header_str_done;

	div = Builder.node("div", inner);
	if (highlight == 4) {
		div.className = "selected";
		div.style.color = "white";
	}
	divs.push(div);

	return divs;
}

function campaign_header(highlight, id) {
	var divs = campaign_header_build(highlight, id);

	ac_dom_remove_children($("campaign_new_progress"));
	for (var i = 0; i < divs.length; i++) {
		$("campaign_new_progress").appendChild(divs[i]);
	}
}

function campaign_header_max() {
	switch (campaign_obj.laststep) {
		case "type":
		default:
			return 0;

		case "list":
			return 1;

		case "template":
			return 2;

		case "message":
			return 2;

		case "splitmessage":
			return 2;

		case "splittext":
			return 2;

		case "text":
			return 2;

		case "summary":
			return 3;

		case "result":
			// Hack: in cases where you can click "edit" to get into a
			// campaign, pretend our last step was the summary page.
			if ( ac_array_has([1, 3, 6], campaign_obj.status) )
				return 3;

			return 4;
	}
}

// Change tracking
function campaign_different() {
	campaign_obviously_changed = true;
}

function campaign_safe() {
	campaign_obviously_changed = false;
	if (typeof campaign_changed_safe == "function")
		campaign_changed_safe();
}

function campaign_unload() {
	if (campaign_obviously_changed)
		return messageLP;

	if (!campaign_doing_something) {
		if (typeof campaign_changed == "function" && campaign_changed())
			return messageLP;
	}
}

// set unload
ac_dom_unload_hook(campaign_unload);

{/literal}
