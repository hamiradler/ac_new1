// We intentionally do not use literal tags here.  Do not use an {include} smarty tag with this file
// directly.  Instead, load it with <script src="...">.
var ac = {};

ac.post = function(func, data, cb) {
	$J.post(apipath + "?jq=1&f=" + func + "&rand=" + ac.str.cast(Math.random()), ac.clean(data), cb, 'json');
};

ac.get = function(func, args, cb) {
	var url = apipath + "?jq=1&f=" + func + "&rand=" + ac.str.cast(Math.random());
	var ary = [];

	args = args || [];

	for (var i = 0; i < args.length; i++) {
		url += "&p[]=" + ac.str.cast(args[i]);
	}

	$J.get(url, cb, 'json');
};

// Given a jQuery selector string, serialize all form elements contained within
// return a properly-formatted associative array ready to be sent via ac.post.
ac.prepare = function(sel) {
	var rval = {};
	var incs = {};

	// if form is not passed, make sel be a FORM
	sel = $J(sel);
	if ( !sel ) return rval;
	if ( sel.length == 1 && sel[0].tagName != 'FORM' ) {
		sel = $J("<form></form>").html(sel.html());
	}
	$J.each(sel.serializeArray(), function(k, v) {
		if (v.name.match(/\[]$/)) {
			if (!incs[v.name]) {
				incs[v.name] = 0;
			}

			rval[v.name.replace(/\[]$/, sprintf("[%d]", incs[v.name]))] = v.value;
			incs[v.name]++;
		} else {
			rval[v.name] = v.value;
		}
	});

	return rval;
};

ac.clean = function(ary) {
	for (var i in ary) {
		if (typeof ary[i] == "object" && ary[i])
			ary[i] = ac.clean(ary[i]);

		if (ary[i] == null)
			ary[i] = "";
	}

	return ary;
};

ac.graph = function(graph, args, cb) {
	if ( typeof args == 'function' && typeof cb == 'undefined' ) {
		cb = args;
		args = {};
	}

	var url = ac_js_site.p_link + "/admin/graph.php?json=1&g=" + graph + "&rand=" + ac.str.cast(Math.random());

	args = args || {};
	for (var i in args) {
		if(typeof args[i] == 'function')continue;
		url += "&" + ac.str.cast(i) + "=" + ac.str.cast(args[i]);
	}

	$J.get(url, cb, 'json');
};

ac.row = function(context, id, cb) {
	id = parseInt(id, 10);
	if ( isNaN(id) ) return;
	if ( typeof cb != 'function' ) return;
	ac.get(
		context + '.' + context + '_select_row', // func
		[ id ],                                  // args
		cb                                       // callback
	);
};

ac.hidden = function(sel) {
	return $J(sel).css("display") == "none";
};

ac.highlight = function(elem) {
	elem.focus();
	elem.select();
};

// Prototype-inheritance.  See
// http://en.wikipedia.org/wiki/Prototype-based_programming for an example of
// how it works.
ac.inherit = function(obj) {
	var rval = {};

	if (obj) {
		rval.__proto__ = obj;
	}

	return rval;
};

// Event stuff.

// Wait a number of seconds before running cb().
ac.wait = function(seconds, cb) {
	var timeid = window.setTimeout(cb, seconds * 1000);
	return timeid;
};

// Stop any wait-event that was queued with timeid.
ac.stop = function(timeid, isinterval) {
	isinterval = isinterval || false;

	if (isinterval)
		window.clearInterval(timeid);
	else
		window.clearTimeout(timeid);
};

// Run at an interval.
ac.interval = function(seconds, cb) {
	var intid = window.setInterval(cb, seconds * 1000);
	return intid;
};

// Add a function that should run before unloading the page.
ac.before_unload_hooks = [];
ac.before_unload = function(cb) {
	this.before_unload_hooks.push(cb);
};

ac.before_unload_do = function() {
	var str = "";

	// Use ac. here, as this function will be assigned to window.onbeforeunload.
	for (var i = 0; i < ac.before_unload_hooks.length; i++) {
		var rval = ac.before_unload_hooks[i]();

		if (rval) {
			if (str == "")
				str = rval;
			else
				str = str + "; " + rval;
		}
	}

	if (str != "")
		return str;
};

ac.old_before_unload = window.onbeforeunload;
if ( typeof window.onbeforeunload != 'function' ) {
	window.onbeforeunload = ac.before_unload_do;
} else {
	window.onbeforeunload = function() {
		ac.old_before_unload();
		ac.before_unload_do();
	}
}

// String functions
// --

ac.str = {};

// If s is undefined, return def.  If def is undefined, assume def is a blank
// string.  Whatever s ends up being, return its string representation.
ac.str.cast = function(s, def) {
	def = def || "";
	s = s || def;
   	return s.toString();
};

// Trim left.
ac.str.ltrim = function(s, ch) {
	s = this.cast(s);
	ch = this.cast(ch, "\\s");
	return s.replace(new RegExp("^[" + ch + "]+", "g"), "");
};

// Trim right.
ac.str.rtrim = function(s, ch) {
	s = this.cast(s);
	ch = this.cast(ch, "\\s");
	return s.replace(new RegExp("[" + ch + "]+$", "g"), "");
};

// Trim both ends.
ac.str.trim = function(s, ch) {
	return this.ltrim(this.rtrim(s, ch), ch);
};

// Shorten the string to a desired length
ac.str.shorten = function(s, len) {
	if ( !len || len == 0 ) return s;
    var sLength = s.length;
    s += ' ';
    s = s.substr(0, len);
    var lastSpacePos = s.lastIndexOf(' ');
    if ( lastSpacePos != -1 )
        s = s.substr(0, lastSpacePos);
    if ( sLength > s.length )
        s += '...';

    return s;
}

// Return true if addr looks like a real email address.
ac.str.valid_email = function(addr) {
	addr = this.cast(addr);
    return addr.match(/^[\+_a-z0-9-'&=]+(\.[\+_a-z0-9-']+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,7})$/i);
};

/* Made by Mathias Bynens <http://mathiasbynens.be/> */
ac.str.number_format = function(a, b, c, d) {
 if(!b)b=0;if(!c)c='.';if(!d)d=',';
 a = Math.round(a * Math.pow(10, b)) / Math.pow(10, b);
 e = a + '';
 f = e.split('.');
 if (!f[0]) {
  f[0] = '0';
 }
 if (!f[1]) {
  f[1] = '';
 }
 if (f[1].length < b) {
  g = f[1];
  for (i=f[1].length + 1; i <= b; i++) {
   g += '0';
  }
  f[1] = g;
 }
 if(d != '' && f[0].length > 3) {
  h = f[0];
  f[0] = '';
  for(j = 3; j < h.length; j+=3) {
   i = h.slice(h.length - j, h.length - j + 3);
   f[0] = d + i +  f[0] + '';
  }
  j = h.substr(0, (h.length % 3 == 0) ? 3 : (h.length % 3));
  f[0] = j + f[0];
 }
 c = (b <= 0) ? '' : c;
 return f[0] + c + f[1];
};
