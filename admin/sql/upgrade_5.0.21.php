<?php

spit(_a('Relating default confirmation sets: '), 'em');
$sql = ac_sql_query("SELECT id FROM #list");
$done = true;
if ($sql) {
	while ($row = ac_sql_fetch_assoc($sql)) {
		$ins = array(
			"listid" => $row["id"],
			"emailconfid" => 1,
		);

		$done = ac_sql_insert("#optinoptout_list", $ins);

		if (!$done)
			break;
	}
}
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

spit(_a('Relating all confirmation sets in use: '), 'em');
$sql = ac_sql_query("SELECT id, optinoptout FROM #list WHERE optinoptout > 1");
$done = true;
if ($sql) {
	while ($row = ac_sql_fetch_assoc($sql)) {
		$ins = array(
			"listid" => $row["id"],
			"emailconfid" => $row["optinoptout"],
		);

		$done = ac_sql_insert("#optinoptout_list", $ins);

		if (!$done)
			break;
	}
}
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

?>
