<?php

require_once(dirname(dirname(__FILE__)) . '/functions/form.php');
require_once(dirname(dirname(__FILE__)) . '/functions/config.php');
require_once(dirname(dirname(dirname(__FILE__))) . '/ac_global/functions/htmltext.php');
require_once ac_global_functions("admin.php");
require_once ac_global_functions("site.php");
require_once ac_admin("functions/user.php");
require_once ac_admin("functions/permission.php");

$old_optin_1 = <<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>My document title</title>
</head>
<body>
<p>Thank you for subscribing. Click here to confirm your subscription:<br><a href="%CONFIRMLINK%">%CONFIRMLINK%</a></p>
</body>
</html>
EOF;

$old_optin_2 = <<<EOF
<body>
<div style="font-size: 12px; font-family: Arial, Helvetica;"><strong>Thank you for subscribing to %LISTNAME%!</strong></div>
<div style="padding: 15px; font-size: 12px; background: #F2FFD8; border: 3px solid #E4F4C3; margin-bottom: 0px; margin-top: 15px; font-family: Arial, Helvetica;">To confirm that you wish to be subscribed, please click the link below:<br><br><a href="%CONFIRMLINK%"><strong>Confirm My Subscription</strong></a></div>
</body>
EOF;

$admin = ac_admin_get();
$site = ac_site_get();

$need_def_form = array();
$listform = array();
$all_lists = array();

$rs = ac_sql_query("SELECT * FROM #list");
while ($row = ac_sql_fetch_assoc($rs)) {
	// You mean, this sets up everything...?
	$all_lists[] = $row["id"];

	$count = (int)ac_sql_select_one("SELECT COUNT(*) FROM #oldform_list WHERE listid = '$row[id]'");
	if ($count == 0)
		$need_def_form[] = $row["id"];
}

$rs = ac_sql_query("SELECT * FROM #oldform");
while ($row = ac_sql_fetch_assoc($rs)) {
	// List relations.
	$listrel = array();
	$msgid = 0;
	$_rs = ac_sql_query("SELECT * FROM #oldform_list WHERE formid = '$row[id]'");

	while ($_row = ac_sql_fetch_assoc($_rs)) {
		$listrel[] = array(
			"formid" => $_row["formid"],
			"listid" => $_row["listid"],
		);

		// Let's see if any of the lists on this form need redirects or custom messages,
		// excepting the successful subscription action which is handled later.
		$listform[] = array($_row["listid"], FORM_SUBSCRIBE_RESULT, $row["sub1_type"], $row["sub1_value"]);
		$listform[] = array($_row["listid"], FORM_SUBSCRIBE_CONFIRM_RESULT, $row["sub3_type"], $row["sub3_value"]);
		$listform[] = array($_row["listid"], FORM_SUBSCRIBE_ERROR, $row["sub4_type"], $row["sub4_value"]);
		$listform[] = array($_row["listid"], FORM_UNSUBSCRIBE_RESULT, $row["unsub1_type"], $row["unsub1_value"]);
		$listform[] = array($_row["listid"], FORM_UNSUBSCRIBE, $row["unsub2_type"], $row["unsub2_value"]);
		$listform[] = array($_row["listid"], FORM_UNSUBSCRIBE_ERROR, $row["unsub4_type"], $row["unsub4_value"]);
		$listform[] = array($_row["listid"], FORM_DETAILS_REQUEST, $row["up1_type"], $row["up1_value"]);
		$listform[] = array($_row["listid"], FORM_DETAILS, $row["up2_type"], $row["up2_value"]);
	}

	// See what opt-in message they may have.
	$optin = 0;
	if ($row["optinoptout"] > 0) {
		$optin = (int)$row["optinoptout"];
	} else {
		// Ok.  Let's try their lists.
		foreach ($listrel as $v) {
			$optin = (int)ac_sql_select_one("SELECT optinoptout FROM #list WHERE id = '$v[listid]'");
			if ($optin != 0)
				break;
		}
	}

	if ($optin) {
		// We found something.  Add that message data.
		$opt = ac_sql_select_row("SELECT * FROM #optinoptout WHERE id = '$optin'");

		if ($opt) {
			if ( !$opt['optin_confirm'] ) {
				// fields are empty, populate defaults
				$opt["optin_from_name"] = $admin["first_name"] . " " . $admin["last_name"];
				$opt["optin_from_email"] = $admin["email"];
				$opt["html"] = form_message_default('form_subscribe.tpl');
				$opt["text"] = ac_htmltext_convert($opt["html"]);
			}
			$ins = array(
				"userid" => 1,
				"=cdate" => "NOW()",
				"=mdate" => "NOW()",
				"fromname" => $opt["optin_from_name"],
				"fromemail" => $opt["optin_from_email"],
				"reply2" => $opt["optin_from_email"],
				"priority" => 3,
				"charset" => "utf-8",
				"encoding" => "quoted-printable",
				"format" => "mime",
				"subject" => _a("Confirm your subscription"),
				"html" => $opt["optin_html"],
				"text" => $opt["optin_text"],
				"htmlfetch" => "now",
				"textfetch" => "now",
				"hidden" => 0,
				"preview_mime" => "",
				"preview_data" => "",
			);

			if ($ins["html"] == $old_optin_1 || $ins["html"] == $old_optin_2) {
				$ins["html"] = form_message_default('form_subscribe.tpl');
				$ins["text"] = ac_htmltext_convert($ins["html"]);
			}

			if ($ins["text"] == "")
				$ins["text"] = ac_htmltext_convert($ins["html"]);

			ac_sql_insert("#message", $ins);
			$msgid = (int)ac_sql_insert_id();
		} else {
			$ins = array(
				"userid" => 1,
				"=cdate" => "NOW()",
				"=mdate" => "NOW()",
				"fromname" => $admin["first_name"] . " " . $admin["last_name"],
				"fromemail" => $admin["email"],
				"reply2" => $admin["email"],
				"priority" => 3,
				"charset" => "utf-8",
				"encoding" => "quoted-printable",
				"format" => "mime",
				"subject" => _a("Confirm your subscription"),
				"html" => form_message_default('form_subscribe.tpl'),
				"htmlfetch" => "now",
				"textfetch" => "now",
				"hidden" => 0,
				"preview_mime" => "",
				"preview_data" => "",
			);

			$ins["text"] = ac_htmltext_convert($ins["html"]);
			ac_sql_insert("#message", $ins);
			$msgid = (int)ac_sql_select_id();
		}
	} else {
		// Just push something default in there.
		$ins = array(
			"userid" => 1,
			"=cdate" => "NOW()",
			"=mdate" => "NOW()",
			"fromname" => $admin["first_name"] . " " . $admin["last_name"],
			"fromemail" => $admin["email"],
			"reply2" => $admin["email"],
			"priority" => 3,
			"charset" => "utf-8",
			"encoding" => "quoted-printable",
			"format" => "mime",
			"subject" => _a("Confirm your subscription"),
			"html" => form_message_default('form_subscribe.tpl'),
			"htmlfetch" => "now",
			"textfetch" => "now",
			"hidden" => 0,
			"preview_mime" => "",
			"preview_data" => "",
		);

		$ins["text"] = ac_htmltext_convert($ins["html"]);

		ac_sql_insert("#message", $ins);
		$msgid = (int)ac_sql_select_id();
	}

	// The form itself.
	$ins = array(
		"id" => $row["id"],
		"name" => $row["name"],
		"target" => 1,				// FORM_SUBSCRIBE
		"messageid" => $msgid,
		"conflistid" => 0,
		"theme" => "simple-blue",
		"sendoptin" => ( $optin && $optin['optin_confirm'] ) ? 1 : 0,
	);

	if ($row["sub2_type"] == "redirect")
		$ins["redirecturl"] = $row["sub2_value"];

	ac_sql_insert("#form", $ins);
	$formid = (int)ac_sql_insert_id();
	$order = 0;

	// Built-ins: email, possibly name
	$ins = array(
		"formid" => $formid,
		"builtin" => "email",
		"header" => _a("Email"),
		"ordernum" => $order++,
	);
	ac_sql_insert("#form_part", $ins);

	if ($row["ask4fname"] || $row["ask4lname"]) {
		$ins = array(
			"formid" => $formid,
			"builtin" => "fullname",
			"header" => _a("Name"),
			"ordernum" => $order++,
		);
		ac_sql_insert("#form_part", $ins);
	}

	// Custom fields.
	if ($row["fields"] != "") {
		$expl = explode(",", $row["fields"]);
		foreach ($expl as $fieldid) {
			$title = (string)ac_sql_select_one("SELECT title FROM #field WHERE id = '$fieldid'");
			$ins = array(
				"formid" => $formid,
				"fieldid" => $fieldid,
				"header" => $title,
				"ordernum" => $order++,
			);
			ac_sql_insert("#form_part", $ins);
		}
	}

	// More built-ins: list selector, maybe captcha, subscribe button.
	if (($formid == 1000 && count($need_def_form) > 1) || count($listrel) > 1) {
		$ins = array(
			"formid" => $formid,
			"builtin" => "listselector",
			"content" => _a("Select the lists you wish to subscribe to"),
			"ordernum" => $order++,
		);
		ac_sql_insert("#form_part", $ins);
	}

	if ($row["captcha"]) {
		$ins = array(
			"formid" => $formid,
			"builtin" => "captcha",
			"header" => _a("Enter the following to confirm your subscription"),
			"ordernum" => $order++,
		);
		ac_sql_insert("#form_part", $ins);
	}

	$ins = array(
		"formid" => $formid,
		"builtin" => "subscribe",
		"content" => _a("Subscribe"),
		"ordernum" => $order++,
	);
	ac_sql_insert("#form_part", $ins);

	// List relations.
	$_rs = ac_sql_query("SELECT * FROM em_oldform_list WHERE formid = '$row[id]'");
	while ($_row = ac_sql_fetch_assoc($_rs)) {
		$ins = array(
			"formid" => $_row["formid"],
			"listid" => $_row["listid"],
		);

		ac_sql_insert("#form_list", $ins);
	}
}

$GLOBALS['old_upgrade'] = 1;
Config::assign("public.defaultform", $site["general_public"] ? 1000 : 0);

foreach ($all_lists as $listid) {
	form_list_init($listid);
}

foreach ($listform as $ary) {
	_update_listform($ary[0], $ary[1], $ary[2], $ary[3]);
}

// If any lists didn't have a subscription form, then past behavior would have
// been to use the default form.  Keep that up.
foreach ($need_def_form as $listid) {
	$ins = array(
		"formid" => 1000,
		"listid" => $listid,
	);

	ac_sql_insert("#form_list", $ins);
}

function _update_listform_redir($listid, $target, $url) {
	$listid = (int)$listid;
	$target = (int)$target;
	$formids = ac_sql_select_list("SELECT formid FROM #form_list WHERE listid = '$listid'");
	$formstr = implode("','", $formids);

	$up = array(
		"redirecturl" => $url,
	);

	ac_sql_update("#form", $up, "id IN ('$formstr') AND target = '$target'");
}

function _update_listform_custom($listid, $target, $msg) {
	$listid = (int)$listid;
	$target = (int)$target;
	$formids = ac_sql_select_list("SELECT formid FROM #form_list WHERE listid = '$listid'");
	$formstr = implode("','", $formids);

	$formid = (int)ac_sql_select_one("SELECT id FROM #form WHERE id IN ('$formstr') AND target = '$target'");
	$up = array(
		"content" => $msg,
	);

	ac_sql_update("#form_part", $up, "formid = '$formid' AND builtin = 'freeform'");
}

function _update_listform($listid, $target, $type, $value) {
	if ($type == "redirect") {
		_update_listform_redir($listid, $target, $value);
	} elseif ($type == "custom") {
		_update_listform_custom($listid, $target, $value);
	}
}

?>
