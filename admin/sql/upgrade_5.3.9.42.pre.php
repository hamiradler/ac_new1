<?php

spit(_a("Removing duplicate subscribers: "), 'em');

// OK... here we go.

$rs = ac_sql_query("
	SELECT
		email
	FROM
		#subscriber s
	GROUP BY
		s.email
	HAVING
		COUNT(s.email) > 1
");

while ($row = ac_sql_fetch_assoc($rs)) {
	// Now loop through each email, and pick up the list relations.
	$subs = array();
	$uniq = array();
	$first = 0;

	$email_esc = ac_sql_escape($row['email']);
	$_rs = ac_sql_query("SELECT id FROM #subscriber s WHERE email = '$email_esc'");

	while ($_row = ac_sql_fetch_assoc($_rs)) {
		if ($first == 0)
			$first = $_row['id'];
		$subs[$_row['id']] = ac_sql_select_list("SELECT listid FROM #subscriber_list l WHERE l.subscriberid = '$_row[id]'");
	}

	foreach ($subs as $subid => $rels) {
		foreach ($rels as $listid) {
			if ($subid != $first)
				ac_sql_query("UPDATE #subscriber_list SET subscriberid = '$first' WHERE subscriberid = '$subid' AND listid = '$listid'");
		}

		if ($subid != $first) {
			mysql_query("DELETE FROM em_subscriber_list WHERE subscriberid = '$subid'");
			mysql_query("DELETE FROM em_subscriber_responder WHERE subscriberid = '$subid'");

			list($count) = mysql_fetch_row(mysql_query("SELECT COUNT(*) FROM em_subscriber_list WHERE subscriberid = '$subid'"));

			if ($count == 0) {
				mysql_query("DELETE FROM em_subscriber WHERE id = '$subid'");
				mysql_query("DELETE FROM em_list_field_value WHERE relid = '$subid'");
			}
		}
	}
}

spit(_a('Done'), 'strong|done', 1);

?>
