INSERT INTO `em_service` (`id`, `name`, `description`) VALUES (5, 'Shopify', 'Configure Shopify integration settings.');
ALTER TABLE `em_service` ADD COLUMN `data` TEXT NULL AFTER `description`;
ALTER TABLE `em_subscriber_import_service`  ENGINE=InnoDB;