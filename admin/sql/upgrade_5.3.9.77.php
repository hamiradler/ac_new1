<?php

require_once(dirname(dirname(__FILE__)) . '/functions/form.php');
require_once(dirname(dirname(__FILE__)) . '/functions/config.php');
require_once(dirname(dirname(dirname(__FILE__))) . '/ac_global/functions/htmltext.php');
require_once ac_global_functions("admin.php");
require_once ac_global_functions("site.php");
require_once ac_admin("functions/user.php");
require_once ac_admin("functions/permission.php");


$admin = ac_admin_get();
$site = ac_site_get();


$done = true;
spit(_a('Fixing optin forms that were turned off: '), 'em', 1);
$query = "
	SELECT
		*,
		m.id AS mid,
		f.id AS fid
	FROM
		#message m,
		#form f
	WHERE
		m.id = f.messageid
#	AND
#		f.sendoptin = 1
	AND
		m.html = ''
";
$sql = ac_sql_query($query);
if ( !$sql ) $done = false;
while ( $row = mysql_fetch_assoc($sql) ) {
	// update message
	$up = array();
	$up["fromname"] = $admin["first_name"] . " " . $admin["last_name"];
	$up["reply2"] =
	$up["fromemail"] = $admin["email"];
	$up["html"] = form_message_default('form_subscribe.tpl');
	$up["text"] = ac_htmltext_convert($up["html"]);
	$done = ac_sql_update("#message", $up, "id = '$row[mid]'");
	if ( !$done ) break;
	// update form
	$done = ac_sql_update_one("#form", "sendoptin", 0, "id = '$row[fid]'");
	if ( !$done ) break;

}

if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

?>