# preserving campaign counts after deletion
CREATE TABLE `em_campaign_count` ( `id` int(10) NOT NULL  AUTO_INCREMENT, `campaignid` int(10) NOT NULL DEFAULT '0', `userid` int(10) NOT NULL DEFAULT '0', `groupid` int(10) NOT NULL DEFAULT '0', `amt` int(10) NOT NULL DEFAULT '0', `tstamp` datetime NULL, PRIMARY KEY (`id`), KEY `groupid` (`groupid`), KEY `userid` (`userid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE = utf8_general_ci;

# twitter support
ALTER TABLE `em_list` ADD `p_use_twitter` TINYINT( 1 ) DEFAULT '0' NOT NULL AFTER `p_use_analytics_link` ;
ALTER TABLE `em_list` ADD `twitter_user` VARCHAR( 64 ) NOT NULL AFTER `analytics_ua` ;
ALTER TABLE `em_list` ADD `twitter_pass` VARCHAR( 128 ) NOT NULL AFTER `twitter_user` ;
ALTER TABLE `em_campaign` ADD `tweet` TINYINT( 1 ) DEFAULT '0' NOT NULL AFTER `analytics_campaign_name` ;
