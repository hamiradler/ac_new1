ALTER TABLE em_list ADD fulladdress TEXT NULL DEFAULT NULL AFTER sender_phone;
ALTER TABLE em_list ADD KEY `name` (`name`), ADD KEY `fulladdress` (`fulladdress` (255));

DROP TABLE IF EXISTS em_address;
