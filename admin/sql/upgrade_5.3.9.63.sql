UPDATE em_branding SET public_template_htm = REPLACE(public_template_htm, '%HEADERNAV%', '') WHERE public_template_htm LIKE '%\%HEADERNAV\%%';
UPDATE em_branding SET public_template_htm = REPLACE(public_template_htm, '%FOOTERNAV%', '') WHERE public_template_htm LIKE '%\%FOOTERNAV\%%';
UPDATE em_branding SET public_template_htm = REPLACE(public_template_htm, '%LANGSELECT%', '') WHERE public_template_htm LIKE '%\%LANGSELECT\%%';
UPDATE em_branding SET admin_template_htm = REPLACE(admin_template_htm, '%SEARCHBAR%', '') WHERE admin_template_htm LIKE '%\%SEARCHBAR\%%';

CREATE TABLE IF NOT EXISTS `em_folder` ( `id` int(10) unsigned NOT NULL AUTO_INCREMENT, `folder` varchar(250) NOT NULL, PRIMARY KEY (`id`), UNIQUE KEY `folder` (`folder`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `em_campaign_folder` ( `id` int(10) unsigned NOT NULL AUTO_INCREMENT, `campaignid` int(10) unsigned NOT NULL DEFAULT '0', `folderid` int(10) unsigned NOT NULL DEFAULT '0', PRIMARY KEY (`id`), KEY `campaignid` (`campaignid`), KEY `folderid` (`folderid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;
