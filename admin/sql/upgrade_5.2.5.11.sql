ALTER TABLE `em_campaign` ADD `responder_existing` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `responder_type`;
ALTER TABLE `em_campaign_deleted` ADD `responder_existing` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `responder_type`;
