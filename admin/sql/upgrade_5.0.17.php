<?php

require_once dirname(dirname(__FILE__)) . "/functions/em.php";

spit(_a('Converting campaign counts: '), 'em');
$done = ac_sql_query("
	INSERT INTO
		#campaign_count
	(
		`id`, `campaignid`, `userid`, `groupid`, `amt`, `tstamp`
	)
	SELECT
		0 AS `id`,
		c.id AS `campaignid`,
		c.userid,
		IF(u.groupid IS NULL, 0, u.groupid) AS `groupid`,
		c.total_amt AS `amt`,
		c.sdate AS `tstamp`
	FROM
		#campaign c
	LEFT JOIN
		#user_group u
	ON
		u.userid = c.userid
	WHERE
		c.status != 0
	AND c.sdate < NOW()
");
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

spit(_a('Adding Twitter subscriber: '), 'em');
$insert = array(
	'id' => 0,
	'=cdate' => 'NOW()',
	'email' => 'twitter',
	'first_name' => _a('Twitter'),
	'last_name' => _a('Account'),
	'=ip' => "INET_ATON('127.0.0.1')",
	'hash' => 'twitter',
);

$done = ac_sql_insert('#subscriber', $insert);
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

?>
