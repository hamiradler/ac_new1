UPDATE `em_subscriber` SET `email` = TRIM(`email`) WHERE `email` != TRIM(`email`);
ALTER TABLE `em_link_data` ADD KEY `linkid_subscriberid` (`linkid`, `subscriberid`);
ALTER TABLE `em_bounce_data` ADD `counted` TINYINT( 1 ) NOT NULL DEFAULT '1';
CREATE TABLE `em_error_source` ( `id` int(10) NOT NULL  AUTO_INCREMENT, `tstamp` date NULL, `action` varchar(32) NOT NULL DEFAULT '', `error` varchar(32) NOT NULL DEFAULT '', `source` longtext NULL, PRIMARY KEY (`id`), KEY `action` (`action`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE = utf8_general_ci;
INSERT INTO `em_trapperr` (`id`, `value`) VALUES ('user_error_is_deadly', '0');
ALTER TABLE `em_backend` ADD `log_error_source` TINYINT( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `em_group_limit` CHANGE `limit_mail_type` `limit_mail_type` ENUM( 'day', 'week', 'month', 'month1st', 'monthcdate', 'year', 'ever' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'month';
ALTER TABLE `em_group_limit` CHANGE `limit_campaign_type` `limit_campaign_type` ENUM( 'day', 'week', 'month', 'month1st', 'monthcdate', 'year', 'ever' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'month';
|ALTER TABLE `em_exclusion` ADD KEY `wildcard` (`wildcard`);
