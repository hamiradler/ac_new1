<?php

spit(_a('Converting custom fields into new format: '), 'em');
$types = array(
	1 => 'text',
	2 => 'textarea',
	3 => 'checkbox',
	4 => 'radio',
	5 => 'dropdown',
	6 => 'hidden',
	7 => 'listbox',
	8 => 'checkboxgroup',
	9 => 'date',
);
$done = true;
$sql = ac_sql_query("SELECT * FROM `#list_field` ;");
while ( $row = mysql_fetch_assoc($sql) ) {
	$default_value = in_array((int)$row['type'], array(2,)) ? $row['expl'] : $row['onfocus'];
	$rows = $cols = 0;
	if ( $row['type'] == 2 ) {
		if ( !ac_str_instr('||', $row['onfocus']) ) $row['onfocus'] .= '||' . $row['onfocus'];
		list($cols, $rows) = explode('||', $row['onfocus']);
	} elseif ( in_array($row['type'], array(4,5,7,8)) ) {
		$options = explode("\r\n", $row['expl']);
		$i = 0;
		foreach ( $options as $option ) {
			$i++;
			if ( !ac_str_instr('||', $option) ) $option .= '||' . $option;
			list($label, $value) = explode('||', $option);
			$insert = array(
				'id' => 0,
				'fieldid' => $row['id'],
				'orderid' => $i,
				'value' => $value,
				'label' => $label,
			);
			$done = ac_sql_insert("#field_option", $insert);
			if ( !$done ) break(2);
		}
	}
	$insert = array(
		'id' => $row['id'],
		'title' => $row['title'],
		'descript' => $row['bubble_content'],
		'type' => $types[$row['type']],
		'isrequired' => (int)(bool)$row['req'],
		'perstag' => $row['perstag'],
		'defval' => $default_value,
		'show_in_list' => (int)(bool)$row['show_in_list'],
		'rows' => $rows,
		'cols' => $cols,
	);
	$done = ac_sql_insert("#field", $insert);
	if ( !$done ) break;
}
if ( !$done ) {
	spit(_a('Error'), 'strong|error', 1);
	error_save("QUERY FAILED: " . ac_sql_lastquery() . "\n\n ERROR: " . ac_sql_error(), true);
	return;
} else {
	spit(_a('Done'), 'strong|done', 1);
}

ac_sql_query("DROP TABLE `#list_field` ;");


?>
