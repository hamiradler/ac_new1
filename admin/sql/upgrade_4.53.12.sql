ALTER TABLE `12all_listmembers` ADD `subscription_form_id` INT( 10 ) NOT NULL DEFAULT '0';

ALTER TABLE `12all_admin_p` ADD `admin_ab_split` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `admin_import`;
ALTER TABLE `12all_admin_p` ADD `admin_auto_remind` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `admin_ab_split`;
ALTER TABLE `12all_admin_p` ADD `admin_email_check` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `admin_auto_remind` ;
ALTER TABLE `12all_admin_p` ADD `admin_flash_forms` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `admin_email_check` ;
ALTER TABLE `12all_admin_p` ADD `admin_sync` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `admin_flash_forms` ;

UPDATE `12all_admin_p` SET admin_sync = 1 WHERE id = 1;