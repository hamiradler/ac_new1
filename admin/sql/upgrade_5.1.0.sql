ALTER TABLE `em_group` ADD `pg_reports_trend` TINYINT( 1 ) DEFAULT '1' NOT NULL AFTER `pg_reports_user` ;
ALTER TABLE `em_backend` ADD `mail_abuse` TINYINT( 1 ) NOT NULL DEFAULT '1' ;
CREATE TABLE `em_feedbackloop` ( `id` int(10) NOT NULL  AUTO_INCREMENT, `body` text NULL, `tstamp` datetime NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE = utf8_general_ci;
UPDATE `em_link_data` d, `em_link` l SET d.ua = '' WHERE l.id = d.linkid AND l.link = 'open' ;
UPDATE `em_link_log` d, `em_link` l SET d.ua = '' WHERE l.id = d.linkid AND l.link = 'open' ;

|ALTER TABLE `em_link_data` DROP INDEX `linkid` ;
ALTER TABLE `em_link_data` ADD `uasrc` VARCHAR( 250 ) NOT NULL AFTER `ua` ;
ALTER TABLE `em_link_log` ADD `uasrc` VARCHAR( 250 ) NOT NULL AFTER `ua` ;
