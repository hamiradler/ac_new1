DELETE FROM em_subscriber WHERE email = 'twitter';

ALTER TABLE em_subscriber_data ADD `fb_id` bigint(20) unsigned not null default '0';
ALTER TABLE em_subscriber_data ADD `fb_name` varchar(255) not null default '';
ALTER TABLE em_subscriber_data ADD `tw_id` bigint(20) unsigned not null default '0';

CREATE TABLE IF NOT EXISTS `em_share` (  `id` int(10) NOT NULL AUTO_INCREMENT,  `cdate` datetime DEFAULT NULL,  `subscriberid` int(10) NOT NULL DEFAULT '0',  `campaignid` int(10) NOT NULL DEFAULT '0',  `type` enum('facebook','fblike','twitter') NOT NULL DEFAULT 'facebook',  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',  `user_realname` varchar(250) NOT NULL DEFAULT '',  `user_screenname` varchar(250) NOT NULL DEFAULT '',  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',  `post_content` text DEFAULT NULL,  PRIMARY KEY (`id`),  KEY `subscriberid` (`subscriberid`),  KEY `campaignid` (`campaignid`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;


UPDATE em_subscriber_list SET udate = NULL WHERE udate = '0000-00-00 00:00:00';
UPDATE em_subscriber_list SET udate = NULL WHERE status != 2;
TRUNCATE TABLE em_datecount;