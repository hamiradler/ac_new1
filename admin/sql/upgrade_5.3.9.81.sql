CREATE TABLE em_import ( id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY, groupid INT(10) UNSIGNED NOT NULL DEFAULT '0', processid INT(10) UNSIGNED NOT NULL DEFAULT '0', name VARCHAR(250) NOT NULL DEFAULT '', KEY groupid (groupid)) ENGINE = InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;
ALTER TABLE em_subscriber_import ADD KEY processid_res (processid, res);
