ALTER TABLE `em_campaign` ADD `willrecur` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `recurring`;
ALTER TABLE `em_campaign_deleted` ADD `willrecur` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `recurring`;
