|ALTER TABLE `em_user` ADD `approved` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '1';
|ALTER TABLE `em_user` ADD `username` VARCHAR( 250) NOT NULL DEFAULT '';
|ALTER TABLE `em_user` ADD `first_name` VARCHAR( 250 ) NOT NULL DEFAULT '' AFTER `username`;
|ALTER TABLE `em_user` ADD `last_name` VARCHAR( 250 ) NOT NULL DEFAULT '' AFTER `first_name`;
|ALTER TABLE `em_user` ADD `email` VARCHAR( 250 ) NOT NULL DEFAULT '' AFTER `last_name`;
