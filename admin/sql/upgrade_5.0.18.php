<?php

require_once dirname(dirname(__FILE__)) . "/functions/em.php";

spit(_a('Compiling split message statistics: '), 'em');
$rs = ac_sql_query("SELECT id, total_amt, send_amt FROM #campaign");
while ($row = ac_sql_fetch_assoc($rs)) {
	campaign_update_splittotal($row["id"], $row["total_amt"]);
	campaign_update_splitsend($row["id"], $row["send_amt"]);
}
spit(_a('Done'), 'strong|done', 1);

?>
