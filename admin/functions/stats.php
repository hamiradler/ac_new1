<?php

// How long should a period last (in SQL's INTERVAL clause, e.g. 4 DAY, 1 WEEK, 3 MONTH, 2 YEAR, 5 HOUR, etc).
$GLOBALS["stats_period"] = "2 WEEK";

// How many periods for the first/second periods in stats_recent().
$GLOBALS["stats_recent_first"] = 3;
$GLOBALS["stats_recent_second"] = 6;

// From what date are we measuring this activity?
$GLOBALS["stats_reldate"] = "NOW()";

function stats_id() {
	$id = (int)ac_sql_select_one("=MAX(id)", "#stats", "edate > $GLOBALS[stats_reldate]");
	if ( $id ) return $id;

	$ins = array(
		"=sdate" => "$GLOBALS[stats_reldate]",
		"=edate" => "$GLOBALS[stats_reldate] + INTERVAL $GLOBALS[stats_period]",
	);

	ac_sql_insert("#stats", $ins);
	return (int)ac_sql_insert_id();
}

function stats_inc($field, $amount = 1) {
	if (!isset($GLOBALS["_hosted_account"]))
		return;

	$id = stats_id();
	$up = array(
		"=$field" => "$field + $amount",
	);

	ac_sql_update("#stats", $up, "id = '$id'");
}

function stats_dec($field, $amount = 1) {
	if (!isset($GLOBALS["_hosted_account"]))
		return;

	$id = stats_id();
	$up = array(
		"=$field" => "$field - $amount",
	);

	ac_sql_update("#stats", $up, "id = '$id'");
}

function stats_recent() {
	$p1 = $GLOBALS["stats_recent_first"];
	$p2 = $GLOBALS["stats_recent_second"];

	$rs = ac_sql_query("
		SELECT
			*
		FROM
			em_stats
		ORDER BY
			id DESC
		LIMIT
			$p2
	");

	$p1rs = ac_sql_default_row("#stats");
	$p2rs = $p1rs;

	while ($row = ac_sql_fetch_assoc($rs)) {
		$p2rs["sent"] += $row["sent"];
		$p2rs["tracked"] += $row["tracked"];
		$p2rs["campaign"] += $row["campaign"];
		$p2rs["abuse"] += $row["abuse"];
		$p2rs["unsubscribe"] += $row["unsubscribe"];
		$p2rs["bounce"] += $row["bounce"];
		$p2rs["spamtrap"] += $row["spamtrap"];
		$p2rs["open"] += $row["open"];
		$p2rs["click"] += $row["click"];
		$p2rs["social"] += $row["social"];

		if ($p1) {
			$p1rs["sent"] += $row["sent"];
			$p1rs["tracked"] += $row["tracked"];
			$p1rs["campaign"] += $row["campaign"];
			$p1rs["abuse"] += $row["abuse"];
			$p1rs["unsubscribe"] += $row["unsubscribe"];
			$p1rs["bounce"] += $row["bounce"];
			$p1rs["spamtrap"] += $row["spamtrap"];
			$p1rs["open"] += $row["open"];
			$p1rs["click"] += $row["click"];
			$p1rs["social"] += $row["social"];

			$p1--;
		}
	}

	if ($p1rs["sent"] > 0) {
		$p1rs["abuse_pct"] = $p1rs["abuse"] / $p1rs["sent"];
		$p1rs["bounce_pct"] = $p1rs["bounce"] / $p1rs["sent"];
		$p1rs["unsubscribe_pct"] = $p1rs["unsubscribe"] / $p1rs["sent"];
		$p1rs["spamtrap_pct"] = $p1rs["spamtrap"] / $p1rs["sent"];

		if ($p1rs["tracked"] > 0) {
			$p1rs["open_pct"] = $p1rs["open"] / $p1rs["tracked"];
			$p1rs["click_pct"] = $p1rs["click"] / $p1rs["tracked"];
			$p1rs["social_pct"] = $p1rs["social"] / $p1rs["tracked"];
		}
	}

	if ($p2rs["sent"] > 0) {
		$p2rs["abuse_pct"] = $p2rs["abuse"] / $p2rs["sent"];
		$p2rs["bounce_pct"] = $p2rs["bounce"] / $p2rs["sent"];
		$p2rs["unsubscribe_pct"] = $p2rs["unsubscribe"] / $p2rs["sent"];
		$p2rs["spamtrap_pct"] = $p2rs["spamtrap"] / $p2rs["sent"];

		if ($p2rs["tracked"] > 0) {
			$p2rs["open_pct"] = $p2rs["open"] / $p2rs["tracked"];
			$p2rs["click_pct"] = $p2rs["click"] / $p2rs["tracked"];
			$p2rs["social_pct"] = $p2rs["social"] / $p2rs["tracked"];
		}
	}

	return array($p1rs, $p2rs);
}

function stats_overall() {
	$rval = ac_sql_select_row("
		SELECT
			SUM(sent) AS sent,
			SUM(tracked) AS tracked,
			SUM(campaign) AS campaign,
			SUM(abuse) AS abuse,
			SUM(bounce) AS bounce,
			SUM(unsubscribe) AS unsubscribe,
			SUM(spamtrap) AS spamtrap,
			SUM(open) AS open,
			SUM(click) AS click,
			SUM(social) AS social
		FROM
			em_stats
	");

	if ($rval["sent"] == 0) {
		$rval["abuse_pct"] = 0;
		$rval["bounce_pct"] = 0;
		$rval["unsubscribe_pct"] = 0;
		$rval["spamtrap_pct"] = 0;
		$rval["open_pct"] = 0;
		$rval["click_pct"] = 0;
		$rval["social_pct"] = 0;
		return $rval;
	}

	$rval["abuse_pct"] = $rval["abuse"] / $rval["sent"];
	$rval["bounce_pct"] = $rval["bounce"] / $rval["sent"];
	$rval["unsubscribe_pct"] = $rval["unsubscribe"] / $rval["sent"];
	$rval["spamtrap_pct"] = $rval["spamtrap"] / $rval["sent"];

	if ($rval["tracked"] == 0) {
		$rval["open_pct"] = 0;
		$rval["click_pct"] = 0;
		$rval["social_pct"] = 0;
		return $rval;
	}

	$rval["open_pct"] = $rval["open"] / $rval["tracked"];
	$rval["click_pct"] = $rval["click"] / $rval["tracked"];
	$rval["social_pct"] = $rval["social"] / $rval["tracked"];

	return $rval;
}

function stats_activity_log($type, $subscriberid, $relid, $dataid = 0) {
	// if fb/tw was logged, don't log click
	if ( $type == 'click' and isset($GLOBALS['stats_activity_dont_log_clicks']) ) return;
	if ( in_array($type, array('facebook', 'twitter')) ) {
		$GLOBALS['stats_activity_dont_log_clicks'] = 1;
	}
	if ( in_array($type, array('facebook', 'twitter', 'forward', 'open', 'click')) ) {
		$listid = 0;
		$campaignid = $relid;
	} else { // if ( in_array($type, array('subscribe', 'unsubscribe', 'update')) ) {
		$listid = $relid;
		$campaignid = 0;
	}

	$groups = array();

	if ( !$subscriberid ) {
		// subscriber not provided
		if ( $campaignid ) {
			$groups = ac_sql_select_box_array("
				SELECT
					g.groupid,
					g.groupid
				FROM
					#list_group g,
					#campaign_list c
				WHERE
					g.listid = c.listid
				AND
					c.campaignid = '$campaignid'
			");
		} else { // if ( $listid ) {
			$groups = ac_sql_select_box_array("
				SELECT
					groupid,
					groupid
				FROM
					#list_group g
				WHERE
					g.listid = '$listid'
			");
		}
	} else {
		// just lists that this sub is in
		if ( $campaignid ) {
			$groups = ac_sql_select_box_array("
				SELECT
					g.groupid,
					g.groupid
				FROM
					#list_group g,
					#campaign_list c,
					#subscriber_list s
				WHERE
					g.listid = c.listid
				AND
					g.listid = s.listid
				AND
					c.campaignid = '$campaignid'
				AND
					s.subscriberid = '$subscriberid'
			");
		} else { // if ( $listid ) {
			$groups = ac_sql_select_box_array("
				SELECT
					g.groupid,
					g.groupid
				FROM
					#list_group g,
					#subscriber_list s
				WHERE
					g.listid = s.listid
				AND
					g.listid = '$listid'
				AND
					s.subscriberid = '$subscriberid'
			");
		}
	}

	if ( !isset($groups[3]) ) $groups[3] = 3;

	// now log activity for each group separately
	foreach ( $groups as $groupid ) {
		stats_activity_log_pergroup($type, $groupid, $dataid);
	}
}

function stats_activity_log_pergroup($type, $groupid, $dataid = 0) {
	// current hour: 2011-12-23 04:00:00
	// made with: CONCAT(CURDATE(), ' ', HOUR(CURTIME()), ':00:00')
	// other uses: CONCAT(CURDATE(), ' ', MAKETIME(HOUR(CURTIME()), 0, 0))
	// tstamp = DATE_FORMAT(NOW(), '%Y-%m-%d %H:00:00')
	$types = stats_activity_types();
	$typeid = isset($types[$type]) ? $types[$type]['id'] : 0;
	$groupid = (int)$groupid;
	$id = ac_sql_select_one("id", "#activity_cache", "type = '$typeid' AND groupid = '$groupid' AND tstamp = CONCAT(CURDATE(), ' ', MAKETIME(HOUR(CURTIME()), 0, 0))");

	if ( !$id ) {
		$in = array(
			'id' => 0,
			'groupid' => $groupid,
			'type' => $typeid,
			'=tstamp' => "CONCAT(CURDATE(), ' ', MAKETIME(HOUR(CURTIME()), 0, 0))",
			'amount' => 1,
			'=minute' => "MINUTE(CURTIME())",
		);
		if ( $dataid ) {
			$in['minid'] = $dataid;
			$in['maxid'] = $dataid;
		}
		ac_sql_insert("#activity_cache", $in);
	} else {
		$up = array(
			'=amount' => 'amount + 1',
			'=minute' => "MINUTE(CURTIME())",
		);
		if ( $dataid ) $up['maxid'] = $dataid;
		ac_sql_update("#activity_cache", $up, "id = '$id'");
	}
}

function stats_activity_types() {
	return array(
		'subscribe' => array(
			'id' => 1,
			'color' => 'unused',
			'class' => 'acalert',
			'text' => _a('New subscribers'),
			'fade' => 0,
			'url' => ac_site_alink("main.php?action=report_list_subscription"),
		),
		'twitter' => array(
			'id' => 2,
			'color' => 'unused',
			'class' => 'twitter',
			'text' => _a('Twitter mentions'),
			'fade' => 0,
			'url' => ac_site_alink("main.php?action=report_campaign"),
		),
		'facebook' => array(
			'id' => 3,
			'color' => 'unused',
			'class' => 'facebook',
			'text' => _a('Facebook mentions'),
			'fade' => 0,
			'url' => ac_site_alink("main.php?action=report_campaign"),
		),
		'unsubscribe' => array(
			'id' => 4,
			'color' => 'unused',
			'class' => 'unsub',
			'text' => _a('Unsubscribed'),
			'fade' => 1,
			'url' => ac_site_alink("main.php?action=report_list_subscription"),
		),
		'open' => array(
			'id' => 5,
			'color' => 'unused',
			'class' => 'acalert',
			'text' => _a('Opened'),
			'fade' => 0,
			'url' => ac_site_alink("main.php?action=report_trend_read"),
		),
		'click' => array(
			'id' => 6,
			'color' => 'unused',
			'class' => 'acalert',
			'text' => _a('Click-thru'),
			'fade' => 0,
			'url' => ac_site_alink("main.php?action=report_campaign"),
		),
		'forward' => array(
			'id' => 7,
			'color' => 'unused',
			'class' => 'acalert',
			'text' => _a('Forward to friends'),
			'fade' => 0,
			'url' => ac_site_alink("main.php?action=report_campaign"),
		),
		'update' => array(
			'id' => 8,
			'color' => 'unused',
			'class' => 'acalert',
			'text' => _a('Account updates'),
			'fade' => 0,
			'url' => ac_site_alink("main.php?action=report_list"),
		),

		'read' => array(
			'id' => 10,
			'color' => 'unused',
			'class' => 'acalert',
			'text' => _a('Opened'),
			'fade' => 0,
			'url' => ac_site_alink("main.php?action=report_trend_read"),
		),
	);
}

function stats_activity_stream() {
	global $admin;
	$gid = current($admin['groups']);
	$offset = (int)ac_http_param('offset');
	$limit = (int)ac_http_param('limit');
	$filter = ac_http_param('filter');
	$stream = stats_activity_get($offset, $limit, $filter);
	$smarty = new AC_Smarty('admin');
	$smarty->assign('site', ac_site_get());
	$smarty->assign('admin', ac_admin_get());
	$smarty->assign('stream', $stream);
//	dbg($filter,1);
//	dbg($stream);
	//echo $smarty->fetch('ajax.activity_stream.htm');exit;

	// get a total
	$total = (int)ac_sql_select_one("=COUNT(*)", "#activity_cache", "groupid = '$gid'");

	// store the timestamp of this activity fetch
	if ( !$offset ) $_SESSION['activity_stream_lastcheck'] = time();

	$empty = false;
	if ( !$offset and !$stream ) {
		$empty = true;
		/*
		if ( is_array($filter) and !array_diff(array_keys(stats_activity_types()), $filter) ) {
			$empty = true;
		} elseif ( !$filter ) {
			$empty = true;
		}
		*/
	}

	if ( $empty ) {
		$html = $smarty->fetch('ajax.activity_stream_nodata.htm');
	} else {
		$html = $smarty->fetch('ajax.activity_stream.htm');
	}

	return array(
		'offset' => $offset,
		'limit' => $limit,
		'total' => $total,
		'hasmore' => $offset + $limit < $total,
		'html' => $html,
		'empty' => $empty,
	);
}

function stats_activity_get($offset = 0, $limit = 6, $filter = null) {
	global $admin;
	$types = stats_activity_types();

	$offset = (int)$offset;
	$limit = (int)$limit;
	if ( !$limit ) $limit = 6;
	if ( !$filter or !is_array($filter) ) {
		$filter = array_keys($types);
	}

	$stream = array();

	$typeslist = array();
	if ( in_array('openclick', $filter) ) {
		$filter[] = 'open';
		$filter[] = 'click';
	}
	foreach ( $filter as $v ) {
		if ( !isset($types[$v]) ) continue;
		$typeslist[(int)$types[$v]['id']] = (int)$types[$v]['id'];
	}

	if ( !$offset ) {
		if ( !isset($_SESSION['activity_stream_lastcheck']) ) $_SESSION['activity_stream_lastcheck'] = 0;
	}

	$liststr = implode("', '", $typeslist);
	$gid = current($admin['groups']);
	$query = "SELECT *, tstamp AS realtstamp FROM #activity_cache WHERE type IN ('$liststr') AND groupid = '$gid' ORDER BY `tstamp` DESC, `minute` DESC LIMIT $offset, $limit";
	$sql = ac_sql_query($query);
	while ( $row = ac_sql_fetch_assoc($sql, array('tstamp')) ) {
		$type = '';
		foreach ( $types as $k => $v ) {
			if ( $v['id'] == $row['type'] ) {
				$type = $k;
				break;
			}
		}
		if ( !$type ) continue;
		$row['typestr'] = $type;
		$row['options']['type'] = $type;
		$row['options'] = $types[$type];
		list($row['last']) = explode(':', $row['tstamp']);
		$row['last'] .= ':' . str_pad($row['minute'], 2, 0, STR_PAD_LEFT) . ':00';
		$row['iscurrent'] = time() - strtotime($row['last']) < 3600;
		$row['timeago'] = ac_date_timeago($row['last'], null, 1, true);
		$timetpl = "AND %s >= '$row[realtstamp]' AND %s < ADDDATE('$row[realtstamp]', INTERVAL 1 HOUR)";
		if ( $row['items'] ) $row['items'] = @ac_str_unserialize($row['items']);
		if ( !$row['items'] ) {
			$row['items'] = stats_activity_list($row['type'], $timetpl, $listid = null, $campaignid = null, $onlyfaces = null, $row);
			if ( !$row['iscurrent'] ) {
				ac_sql_update_one("#activity_cache", "items", serialize($row['items']), "id = '$row[id]'");
			}
		}
		$row['isnew'] = !$offset && $_SESSION['activity_stream_lastcheck'] && strtotime($row['last']) > $_SESSION['activity_stream_lastcheck'];
		$stream[] = $row;
	}

	return $stream;
}

function stats_activity_query($type, $timetpl = "", $listid = null, $campaignid = null, $onlyfaces = null, $rangeids = null) {
	global $admin;
	$query =
	$listtpl =
	$listtpl_sq =
	$camptpl =
	$camptpl_sq = "";
	$listfilter = false; // if we'll filter by list
	if ( $listid = (int)$listid ) $listfilter = true; // if list provided, we will
	if ( !ac_admin_ismaingroup() ) $listfilter = true; // if not main admin, we will
	if ( $listfilter ) { // if using list filter
		if ( ac_admin_ismaingroup() ) { // for main admins
			$lists = array($listid); // just the filtered one
		} elseif ( $listid ) { // if filtered is provided
			$lists = array_intersect(array($listid), $admin['lists']); // check if in allowed lists
		} else { // otherwise
			$lists = $admin['lists']; // just admin's permissions are the filter
		}
		$liststr = implode("', '", $lists);
		$listtpl = "AND %s IN ('$liststr')";
		$listtpl_sq = "AND ( SELECT COUNT(*) FROM %s subx WHERE %s AND subx.listid IN ('$liststr') ) > 0";
	}
	if ( $campaignid = (int)$campaignid ) {
		$camptpl = "AND %s = '$campaignid'";
		$camptpl_sq = "AND ( SELECT COUNT(*) FROM %s subz WHERE %s AND subz.campaignid = '$campaignid' ) > 0";
	}
	$iconcond = "";
	if ( $onlyfaces ) {
		$iconcond = "AND b.gravatar = 2";
	}

	if ( $type == 1 ) { // subscribe
		$timecond = sprintf($timetpl, 'a.sdate', 'a.sdate');
		$listcond = sprintf($listtpl, 'a.listid');
		if ( isset($rangeids['minid']) and isset($rangeids['maxid']) and $rangeids['minid'] and $rangeids['maxid'] ) {
			$timecond = "AND a.id >= '$rangeids[minid]' AND a.id <= '$rangeids[maxid]'";
		} elseif ( $timecond ) {
			$query = "
				SELECT
					a.id
				FROM
					#subscriber_list a
				WHERE
					a.status = 1
				$listcond
				$timecond
				ORDER BY
					a.id ASC
				LIMIT 1
			";
			$minid = (int)ac_sql_select_one($query);
			$query = "
				SELECT
					a.id
				FROM
					#subscriber_list a
				WHERE
					a.status = 1
				$listcond
				$timecond
				ORDER BY
					a.id DESC
				LIMIT 1
			";
			$maxid = (int)ac_sql_select_one($query);
			$timecond = "AND a.id >= $minid AND a.id <= $maxid";
		}
		$query = "
			# subscribe
			SELECT
				b.id,
				b.email,
				b.hash,
				b.gravatar,
				a.sdate AS tstamp,
				a.status AS status,
				MD5(LOWER(b.email)) AS emailhash
			FROM
				#subscriber_list a,
				#subscriber b
			WHERE
				a.subscriberid = b.id
			AND
				a.status = 1
			$listcond
			$timecond
			$iconcond
			ORDER BY
				b.gravatar DESC
			LIMIT 25
		";
	} elseif ( $type == 2 ) { // twitter
		$timecond = sprintf($timetpl, 'a.cdate', 'a.cdate');
		$listcond = sprintf($listtpl_sq, '#campaign_list', 'a.campaignid = subx.campaignid');
		$campcond = sprintf($camptpl, 'a.campaignid');

		if ($campcond != "")
			$listcond = "";
		if ( isset($rangeids['minid']) and isset($rangeids['maxid']) and $rangeids['minid'] and $rangeids['maxid'] ) {
			$timecond = "AND a.id >= '$rangeids[minid]' AND a.id <= '$rangeids[maxid]'";
		} elseif ( $timecond ) {
			$query = "
				SELECT
					a.id
				FROM
					#socialshare a
				WHERE
					a.source = 'twitter'
				$listcond
				$campcond
				$timecond
				ORDER BY
					a.id ASC
				LIMIT 1
			";
			$minid = (int)ac_sql_select_one($query);
			$query = "
				SELECT
					a.id
				FROM
					#socialshare a
				WHERE
					a.source = 'twitter'
				$listcond
				$campcond
				$timecond
				ORDER BY
					a.id DESC
				LIMIT 1
			";
			$maxid = (int)ac_sql_select_one($query);
			$timecond = "AND a.id >= $minid AND a.id <= $maxid";
		}
		$query = "
			# twitter
			SELECT
#				0 AS id,
#				'' AS email,
#				'' AS hash,
#				1 AS gravatar,
				a.cdate AS tstamp,
#				'' AS emailhash,
				a.data
			FROM
				#socialshare a
			WHERE
				a.source = 'twitter'
			$listcond
			$campcond
			$timecond
			ORDER BY
				a.id DESC
			LIMIT 25
		";
	} elseif ( $type == 3 ) { // facebook
		$timecond = sprintf($timetpl, 'a.cdate', 'a.cdate');
		$listcond = sprintf($listtpl_sq, '#campaign_list', 'a.campaignid = subx.campaignid');
		$campcond = sprintf($camptpl, 'a.campaignid');

		if ($campcond != "")
			$listcond = "";

		if ( isset($rangeids['minid']) and isset($rangeids['maxid']) and $rangeids['minid'] and $rangeids['maxid'] ) {
			$timecond = "AND a.id >= '$rangeids[minid]' AND a.id <= '$rangeids[maxid]'";
		} elseif ( $timecond ) {
			$query = "
				SELECT
					a.id
				FROM
					#socialshare a
				WHERE
					a.source = 'facebook'
				$listcond
				$campcond
				$timecond
				ORDER BY
					a.id ASC
				LIMIT 1
			";
			$minid = (int)ac_sql_select_one($query);
			$query = "
				SELECT
					a.id
				FROM
					#socialshare a
				WHERE
					a.source = 'facebook'
				$listcond
				$campcond
				$timecond
				ORDER BY
					a.id DESC
				LIMIT 1
			";
			$maxid = (int)ac_sql_select_one($query);
			$timecond = "AND a.id >= $minid AND a.id <= $maxid";
		}
		$query = "
			# facebook
			SELECT
				b.id,
				b.email,
				b.hash,
				b.gravatar,
				a.cdate AS tstamp,
				MD5(LOWER(b.email)) AS emailhash
			FROM
				#socialshare a,
				#subscriber b
			WHERE
				a.subscriberid = b.id
			AND
				a.source = 'facebook'
			$listcond
			$campcond
			$timecond
			$iconcond
			ORDER BY
				b.gravatar DESC
			LIMIT 25
		";
	} elseif ( $type == 4 ) { // unsubscribe
		$timecond = sprintf($timetpl, 'a.udate', 'a.udate');
		$listcond = sprintf($listtpl, 'a.listid');
		/*
		if ( $timecond ) {
			$query = "
				SELECT
					a.id
				FROM
					#subscriber_list a
				WHERE
					a.status = 2
				$listcond
				$timecond
				ORDER BY
					a.id ASC
				LIMIT 1
			";
			$minid = (int)ac_sql_select_one($query);
			$query = "
				SELECT
					a.id
				FROM
					#subscriber_list a
				WHERE
					a.status = 2
				$listcond
				$timecond
				ORDER BY
					a.id DESC
				LIMIT 1
			";
			$maxid = (int)ac_sql_select_one($query);
			$timecond = "AND a.id >= $minid AND a.id <= $maxid";
		}
		*/
		$query = "
			# unsubscribe
			SELECT
				b.id,
				b.email,
				b.hash,
				b.gravatar,
				a.udate AS tstamp,
				a.status AS status,
				MD5(LOWER(b.email)) AS emailhash
			FROM
				#subscriber_list a,
				#subscriber b
			WHERE
				a.subscriberid = b.id
			AND
				a.status = 2
			$listcond
			$timecond
			$iconcond
			ORDER BY
				b.gravatar DESC
			LIMIT 25
		";
	} elseif ( $type == 5 ) { // open -- sparkline
		$timecond = sprintf($timetpl, 'a.tstamp', 'a.tstamp');
		$listcond = sprintf($listtpl_sq, '#campaign_list', 'a.campaignid = subx.campaignid');
		$campcond = sprintf($camptpl, 'a.campaignid');

		if ($campcond != "")
			$listcond = "";

		if ( isset($rangeids['minid']) and isset($rangeids['maxid']) and $rangeids['minid'] and $rangeids['maxid'] ) {
			$timecond = "AND a.id >= '$rangeids[minid]' AND a.id <= '$rangeids[maxid]'";
		} elseif ( $timecond ) {
			$query = "
				SELECT
					a.id
				FROM
					#link_data a
				WHERE
					a.isread = 1
				$listcond
				$campcond
				$timecond
				ORDER BY
					a.id ASC
				LIMIT 1
			";
			$minid = (int)ac_sql_select_one($query);
			$query = "
				SELECT
					a.id
				FROM
					#link_data a
				WHERE
					a.isread = 1
				$listcond
				$campcond
				$timecond
				ORDER BY
					a.id DESC
				LIMIT 1
			";
			$maxid = (int)ac_sql_select_one($query);
			$timecond = "AND a.id >= $minid AND a.id <= $maxid";
		}
		$query = "
			# open -- sparkline
			SELECT
				MINUTE(a.tstamp) AS min,
				COUNT(*) AS hits
			FROM
				#link_data a
			WHERE
				a.isread = 1
			$listcond
			$campcond
			$timecond
			GROUP BY
				MINUTE(a.tstamp)
			ORDER BY
				MINUTE(a.tstamp)
		";
	} elseif ( $type == 10 ) { // open -- avatars
		$timecond = sprintf($timetpl, 'a.tstamp', 'a.tstamp');
		$listcond = sprintf($listtpl_sq, '#campaign_list', 'a.campaignid = subx.campaignid');
		$campcond = sprintf($camptpl, 'a.campaignid');

		if ($campcond != "")
			$listcond = "";

		if ( isset($rangeids['minid']) and isset($rangeids['maxid']) and $rangeids['minid'] and $rangeids['maxid'] ) {
			$timecond = "AND a.id >= '$rangeids[minid]' AND a.id <= '$rangeids[maxid]'";
		} elseif ( $timecond ) {
			$query = "
				SELECT
					a.id
				FROM
					#link_data a
				WHERE
					a.isread = 1
				$listcond
				$campcond
				$timecond
				ORDER BY
					a.id ASC
				LIMIT 1
			";
			$minid = (int)ac_sql_select_one($query);
			$query = "
				SELECT
					a.id
				FROM
					#link_data a
				WHERE
					a.isread = 1
				$listcond
				$campcond
				$timecond
				ORDER BY
					a.id DESC
				LIMIT 1
			";
			$maxid = (int)ac_sql_select_one($query);
			$timecond = "AND a.id >= $minid AND a.id <= $maxid";
		}
		$query = "
			# open
			SELECT
				b.id,
				b.email,
				b.hash,
				b.gravatar,
				a.tstamp,
				MD5(LOWER(b.email)) AS emailhash
			FROM
				#link_data a,
				#subscriber b
			WHERE
				a.subscriberid = b.id
			AND
				a.isread = 1
			$listcond
			$campcond
			$timecond
			$iconcond
			ORDER BY
				b.gravatar DESC
			LIMIT 25
		";
	} elseif ( $type == 6 ) { // click
		$timecond = sprintf($timetpl, 'a.tstamp', 'a.tstamp');
		$listcond = sprintf($listtpl_sq, '#campaign_list', 'a.campaignid = subx.campaignid');
		$campcond = sprintf($camptpl, 'a.campaignid');

		if ($campcond != "")
			$listcond = "";

		if ( isset($rangeids['minid']) and isset($rangeids['maxid']) and $rangeids['minid'] and $rangeids['maxid'] ) {
			$timecond = "AND a.id >= '$rangeids[minid]' AND a.id <= '$rangeids[maxid]'";
		} elseif ( $timecond ) {
			$query = "
				SELECT
					a.id
				FROM
					#link_data a
				WHERE
					a.isread = 0
				$listcond
				$campcond
				$timecond
				ORDER BY
					a.id ASC
				LIMIT 1
			";
			$minid = (int)ac_sql_select_one($query);
			$query = "
				SELECT
					a.id
				FROM
					#link_data a
				WHERE
					a.isread = 0
				$listcond
				$campcond
				$timecond
				ORDER BY
					a.id DESC
				LIMIT 1
			";
			$maxid = (int)ac_sql_select_one($query);
			$timecond = "AND a.id >= $minid AND a.id <= $maxid";
		}
		$query = "
			# click
			SELECT
				b.id,
				b.email,
				b.hash,
				b.gravatar,
				a.tstamp,
				MD5(LOWER(b.email)) AS emailhash
			FROM
				#link_data a,
				#subscriber b
			WHERE
				a.subscriberid = b.id
			AND
				a.isread = 0
			$listcond
			$campcond
			$timecond
			$iconcond
			ORDER BY
				b.gravatar DESC
			LIMIT 25
		";
	} elseif ( $type == 7 ) { // forward
		$timecond = sprintf($timetpl, 'a.tstamp', 'a.tstamp');
		$listcond = sprintf($listtpl_sq, '#campaign_list', 'a.campaignid = subx.campaignid');
		$campcond = sprintf($camptpl, 'a.campaignid');

		if ($campcond != "")
			$listcond = "";

		if ( isset($rangeids['minid']) and isset($rangeids['maxid']) and $rangeids['minid'] and $rangeids['maxid'] ) {
			$timecond = "AND a.id >= '$rangeids[minid]' AND a.id <= '$rangeids[maxid]'";
		} elseif ( $timecond ) {
			$query = "
				SELECT
					a.id
				FROM
					#forward a
				WHERE
					1
				$listcond
				$campcond
				$timecond
				ORDER BY
					a.id ASC
				LIMIT 1
			";
			$minid = (int)ac_sql_select_one($query);
			$query = "
				SELECT
					a.id
				FROM
					#forward a
				WHERE
					1
				$listcond
				$campcond
				$timecond
				ORDER BY
					a.id DESC
				LIMIT 1
			";
			$maxid = (int)ac_sql_select_one($query);
			$timecond = "AND a.id >= $minid AND a.id <= $maxid";
		}
		$query = "
			# forward
			SELECT
				b.id,
				b.email,
				b.hash,
				b.gravatar,
				a.tstamp,
				MD5(LOWER(b.email)) AS emailhash
			FROM
				#forward a,
				#subscriber b
			WHERE
				a.subscriberid = b.id
			$listcond
			$campcond
			$timecond
			$iconcond
			ORDER BY
				b.gravatar DESC
			LIMIT 25
		";
	} elseif ( $type == 8 ) { // update
		$timecond = sprintf($timetpl, 'a.tstamp', 'a.tstamp');
		$listcond = sprintf($listtpl_sq, '#campaign_list', 'a.campaignid = subx.campaignid');
		$campcond = sprintf($camptpl, 'a.campaignid');

		if ($campcond != "")
			$listcond = "";

		if ( isset($rangeids['minid']) and isset($rangeids['maxid']) and $rangeids['minid'] and $rangeids['maxid'] ) {
			$timecond = "AND a.id >= '$rangeids[minid]' AND a.id <= '$rangeids[maxid]'";
		} elseif ( $timecond ) {
			$query = "
				SELECT
					a.id
				FROM
					#update a
				WHERE
					a.subscriberid = b.id
				$listcond
				$campcond
				$timecond
				ORDER BY
					a.id ASC
				LIMIT 1
			";
			$minid = (int)ac_sql_select_one($query);
			$query = "
				SELECT
					a.id
				FROM
					#update a
				WHERE
					a.subscriberid = b.id
				$listcond
				$campcond
				$timecond
				ORDER BY
					a.id DESC
				LIMIT 1
			";
			$maxid = (int)ac_sql_select_one($query);
			$timecond = "AND a.id >= $minid AND a.id <= $maxid";
		}
		$query = "
			# update
			SELECT
				b.id,
				b.email,
				b.hash,
				b.gravatar,
				a.tstamp,
				MD5(LOWER(b.email)) AS emailhash
			FROM
				#update a,
				#subscriber b
			WHERE
				a.subscriberid = b.id
			$listcond
			$campcond
			$timecond
			$iconcond
			ORDER BY
				b.gravatar DESC
			LIMIT 25
		";
	}

	return $query;
}

function stats_activity_list($type, $timetpl = "", $listid = null, $campaignid = null, $onlyfaces = null, $rangeids = null) {
	require_once(ac_global_functions('gravatar.php'));
	$list = array();
	$query = stats_activity_query($type, $timetpl, $listid, $campaignid, $onlyfaces, $rangeids);
	$sql = ac_sql_query($query);
	if ( !$sql or ( ac_http_param_exists('dbg') and isset($_SESSION['ac_arc_login']) ) ) {
		echo "<!--\n$query\n" . _a("SQL error occurred") . "\n-->";
	}
	if ( $type == 5 ) {
		$list['arr'] = array_fill(0, 60, 0);
	}
	while ( $row = mysql_fetch_assoc($sql) ) {
		if ( $type == 5 ) { // open
			$list['arr'][(int)$row['min']] = (int)$row['hits'];
		} else {
			if ( $type == 2 ) { // twitter
				$row['data'] = ac_str_unserialize($row['data']);
				if ( !isset($row['data']['author']['user']    ) ) $row['data']['author']['user']     = $row['data']['author']['name'];
				if ( !isset($row['data']['author']['justname']) ) $row['data']['author']['justname'] = $row['data']['author']['name'];
				$row['user'] = $row['data']['author']['user'];
				$row['name'] = $row['data']['author']['justname'];
				$row['home'] = 'http://twitter.com/' . $row['data']['author']['user'];
				$row['link'] = $row['data']['link'];
				$row['pic'] = $row['data']['image'];
			} else {
				$row['screenshot'] = gravatar_url($row['emailhash'], 25);
			}
			$row['type'] = $type;
			$list[] = $row;
		}
	}

	if ( $type == 5 ) {
		$list['str'] = implode(',', $list['arr']);
	}
	return $list;
}

function stats_activity_items($filter = array(), $timetpl = "", $listid = null, $campaignid = null, $onlyfaces = null) {
	require_once(ac_global_functions('gravatar.php'));
	$results = array();
	$sorter_gravatar = array();
	$sorter_tstamp = array();
	$types = stats_activity_types();
	$subs = array();
	foreach ( $filter as $type ) {
		if ( !isset($types[$type]) ) continue;
		$typeid = $types[$type]['id'];
		$items = stats_activity_list($typeid, $timetpl, $listid, $campaignid, $onlyfaces, $rangeids = null);
		foreach ( $items as $item ) {
			$key = $typeid == 2 ? 'user' : 'email';
			if ( isset($subs[$item[$key]]) ) continue;
			$subs[$item[$key]] = $item[$key];
			$item['typestr'] = $type;
			$item['type'] = $typeid;
			//if ( $typeid != 2 ) $item['screenshot'] = gravatar_url($item['emailhash'], 25);
			$results[] = $item;
			$sorter_gravatar[] = $type == 'twitter' ? 1 : $item['gravatar'];
			$sorter_tstamp[] = $item['tstamp'];
		}
	}
	// Sort the data with gravatar and tstamp descending
	// Add $data as the last parameter, to sort by the common key
	array_multisort($sorter_gravatar, SORT_DESC, $sorter_tstamp, SORT_DESC, $results);
	$results = array_slice($results, 0, 25);
	return $results;
}

?>
