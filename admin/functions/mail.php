<?php

require_once ac_admin("functions/list.php");
require_once ac_global_functions("log.php");

function mail_responder_send($subscriber, $listids, $type = 'subscribe') {
	require_once ac_admin("functions/campaign.php");

	if ( $type != 'unsubscribe' ) $type = 'subscribe';

	if ( !is_array($listids) ) {
		$listids = array_diff(array_map('intval', explode(',', $listids)), array(0));
	}
	if ( count($listids) > 0 ) {
		$listslist = implode("','", $listids);

		$so = new AC_Select();
		$so->push("AND l.listid IN ('$listslist')");
		$so->push("AND c.type = 'responder'");
		$so->push("AND c.status IN (1, 5)"); // scheduled or completed (not draft, sending, stopped, paused, etc...)
		$so->push("AND c.responder_offset = 0");
		$so->push("AND c.responder_type = '$type'");
		$campaigns = campaign_select_array($so);

		foreach ( $campaigns as $k => $v ) {
			$campaign = campaign_select_prepare($v, true);

			// Figure counts to increment the list_amt field by... since this
			// is a responder campaign, we'll always increment.
			$counts = campaign_subscribers_fetch(explode('-', $campaign['listslist']), $campaign['filterid'], $fetchCount = 2, $offset = 0, $limit = 0, $campaign);
			foreach ( $counts as $k => $v ) {
				ac_sql_update_one('#campaign_list', '=list_amt', "list_amt + $v", "listid = '$k' AND campaignid = '$campaign[id]'");
			}

			campaign_send(null, $campaign, $subscriber, 'send');
		}
	}
}

function mail_campaign_send_last($subscriber, $listids) {
	require_once ac_admin("functions/campaign.php");

	if ( !is_array($listids) ) {
		$listids = array_diff(array_map('intval', explode(',', $listids)), array(0));
	}
	if ( count($listids) == 0 ) return 0;
	# If we just added this subscriber (which is likely), then their filter records won't
	# match anything.  We'll need to analyze them.
	filter_analyze_subscriber_inlist($subscriber["id"], $listids);

	$listslist = implode("','", $listids);
	$so = new AC_Select();
	$so->push("AND l.listid IN ('$listslist')"); // in subscriber's lists
	$so->push("AND c.type NOT IN ('responder', 'reminder', 'special', 'split')"); // real campaigns only
	$so->push("AND c.status = 5"); // completed only
	$so->orderby("c.sdate DESC");
	//dbg(campaign_select_query($so));
	$campaigns = campaign_select_array($so);

	$sent = 0;
	foreach ( $campaigns as $k => $v ) {
		if ($v["filterid"] > 0 && !filter_matches($subscriber["id"], $v["filterid"]))
			continue;
		$campaign = campaign_select_prepare($v, true);
		$sent += (int)campaign_send(null, $campaign, $subscriber, 'send');
		if ( $sent ) break;
	}
	return $sent;
}

// Email to list Admin
function mail_admin_send($subscriber, $lists, $type = 'subscribe') {
	if ( isset($GLOBALS['demoMode']) ) return; // check if demo mode is on
	global $site;
	if ( $type != 'unsubscribe' ) $type = 'subscribe';

	$listids = array();

	foreach ($lists as $list) {
		$listids[] = $list["id"];
	}

	$options = array();
	if ( isset($GLOBALS['_hosted_account']) ) $options['bounce'] = str_replace("@", "-0@", ac_sql_select_one("email", "#bounce", "id = 1"));
	$options['userid'] = $lists[0]['userid'];

	$fields = subscriber_get_fields($subscriber["id"], $listids);

	// call smarty to make an e-mail body
	require_once(ac_global_functions('smarty.php'));
	$smarty = new AC_Smarty('public', true);

	// assign to template
	$smarty->assign('subscriber', $subscriber);
	$smarty->assign('fields', $fields);
	$smarty->assign('lists', $lists);
	$smarty->assign( 'udate_now', date('m/d/Y h:i', strtotime('now')) );
	$text = $smarty->fetch( $type == 'subscribe' ? 'new_subscriber_alert.txt' : 'new_unsubscriber_alert.txt' );

	$r = array();
	$listnames = array();

	foreach ($lists as $v) {
		// Comma-separated list of email addresses from DB table _list.(un)subscription_notify
		$f = $v[ $type == 'subscribe' ? 'subscription_notify' : 'unsubscription_notify' ];
		$arr = explode(",", $f);
		$emails = array_map('trim',$arr);

		$listnames[] = $v["name"];

		// Loop through email address values
		foreach ($emails as $e) {
			//if ( ac_str_is_email($e) ) $r[$e] = $v['name'] . _a(" Administrator");
			if ( ac_str_is_email($e) ) $r[] = $e;
		}
	}

	$subscribername = '';
	if ( isset($subscriber['lists']) and isset($subscriber['lists'][$listids[0]]) ) {
		$subscribername = $subscriber['lists'][$listids[0]]['first_name'] . " " . $subscriber['lists'][$listids[0]]['last_name'];
	}

	$options['headers'] = array();
	$senderheader = false;
	if (isset($GLOBALS["_hosted_account"]))
		$senderheader = true;

	if ($senderheader && $site['onbehalfof'] && isset($GLOBALS["domain"])) {
		$info = $_SESSION[$GLOBALS["domain"]];
		$host = (string)ac_sql_select_one("SELECT host FROM #mailer WHERE id = '1'");

		if (preg_match('/(astirx.com|acemserv.com|acems\d)$/', $host)) {
			$xra = 'Please report abuse at http://www.activecampaign.com/contact/?type=abuse';
			if ( $info['rsid'] ) $xra = 'Please report abuse by forwarding this entire message to abuse@acemserv.com';
			$h = sprintf('<%s@%s>', $info["account"], $host);
			if ( trim($subscribername) ) $h = '"' . trim($subscribername) . '" ' . $h;
			$options['headers'][] = array('name' => "Sender", 'value' => $h);
			$options['headers'][] = array('name' => "X-Sender", 'value' => $h);
			$options['headers'][] = array('name' => "X-Report-Abuse", 'value' => $xra);
		}
	}

	if ( count($r) > 0 ) {
		if ( $type == 'subscribe' ) {
			$subject = implode(", ", $listnames) . ": " . _p("You have a new subscriber to your list.");
		} else {
			$subject = implode(", ", $listnames) . ": " . _p("A subscriber has been removed from your list.");
		}
		//ac_mail_send("text", $site["site_name"], $site["emfrom"], $text, $subject, $r, "");
		$to_name = $v['name'] . _a(" Administrator");
		foreach($r as $email)
		{
			//ac_mail_send("text", $subscriber["first_name"].' '.$subscriber["last_name"], $subscriber["email"], $text, $subject, $email, $to_name, $options);
			ac_mail_send("text", $subscribername, $GLOBALS["admin"]["email"], $text, $subject, $email, $to_name, $options);
		}
	}
}

function mail_sender_info($listid, $body) {
	$list = list_select_row($listid);

	if (!$list || !$list["sender_name"]) {
		return $body;
	}

	# Run these checks on content that has HTML tags stripped out, just in case someone tries to
	# stick %SENDER-INFO% in an HTML comment or somewhere else that it might not be displayed.
	# Literally, run strip_tags everywhere, even if we're not sure or think it shouldn't have
	# tags.

	if (is_string($body)) {
	   	if (ac_str_instr("%SENDER-INFO%", strip_tags($body)) || ac_str_instr("%SENDER-INFO-SINGLELINE%", strip_tags($body)))
			return $body;
		else
			return $body . "\n\n%SENDER-INFO%";
	} elseif (is_array($body)) {
		if (!ac_str_instr("%SENDER-INFO%", strip_tags($body["html"])) && !ac_str_instr("%SENDER-INFO-SINGLELINE%", strip_tags($body["html"])))
			$body["html"] = ac_str_append_html($body["html"], "<br>\n<br>\n%SENDER-INFO%");
		if (!ac_str_instr("%SENDER-INFO%", strip_tags($body["text"])) && !ac_str_instr("%SENDER-INFO-SINGLELINE%", strip_tags($body["text"])))
			$body["text"] .= "\n\n%SENDER-INFO%";

		return $body;
	} else {
		# Huh?
		return $body;
	}
}

?>
