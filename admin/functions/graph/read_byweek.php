<?php

$campaignid = intval(ac_http_param("campaignid"));
$messageid  = intval(ac_http_param("messageid"));
$listid     = intval(ac_http_param("listid"));
$period  = intval(ac_http_param("period"));
$from    = strval(ac_http_param("from"));
$to      = strval(ac_http_param("to"));
$timeline_period  = 24;
$timeline_from    = '00:00:00';
$timeline_to      = 23;

$series = array();
$graph  = array();


$series = array(
	2 => _a("Monday"),
	3 => _a("Tuesday"),
	4 => _a("Wednesday"),
	5 => _a("Thursday"),
	6 => _a("Friday"),
	7 => _a("Saturday"),
	1 => _a("Sunday"),
);

# This MUST be in the same key order as $series, or else the output will be incorrect.
$graph = array(
	2 => 0,
	3 => 0,
	4 => 0,
	5 => 0,
	6 => 0,
	7 => 0,
	1 => 0,
);

//if ( !$from ) $from = date('Y-m-d H:i:s', 0);
//if ( !$to ) $to = AC_CURRENTDATE;

//ac_graph_prepare_timeline($series, $graph, $timeline_period, $timeline_from, $timeline_to);

$cond = "";
if ($messageid > 0)
	$cond .= "AND ld.messageid = '$messageid' ";
//else
//	$cond .= "AND ld.messageid = '0' ";

if ($campaignid > 0)
	$cond .= "AND ld.campaignid = '$campaignid' ";

$listarr = array();
if ( ac_admin_ismain() ) {
	if ( $listid ) $listarr[] = $listid;
} else {
	$admin = ac_admin_get();
	if ( $listid ) {
		if ( isset($admin['lists'][$listid]) ) {
			$listarr[] = $listid;
		} else {
			$listarr = array(0);
		}
	} else {
		$listarr = $admin['lists'];
	}
}
if ( $listarr ) {
	$liststr = implode("', '", $listarr);
	$cond .= "AND ( SELECT COUNT(*) FROM #campaign_list cl WHERE ld.campaignid = cl.campaignid AND cl.listid IN ('$liststr') ) > 0 ";
	//$cond .= "AND ( SELECT COUNT(*) FROM #subscriber_list sl WHERE ld.subscriberid = sl.subscriberid AND sl.listid IN ('$liststr') ) > 0 ";
}

if ( $from ) {
	$cond .= "AND DATE(ld.tstamp) > '$from' ";
}

if ( $to ) {
	$cond .= "AND ld.tstamp <  ('$to' + INTERVAL 1 DAY) ";
}

$query = "
	SELECT
		DATE_FORMAT(ld.tstamp, '%w') + 1 AS tstamp,
		COUNT(*) AS count
	FROM
		#link_data ld
	WHERE
		ld.isread = 1
		$cond
	GROUP BY
		DAYOFWEEK(ld.tstamp)
";
$rs = ac_sql_query($query);

while ($row = ac_sql_fetch_assoc($rs)) {
	$row["tstamp"] = ( $row["tstamp"] ? $row["tstamp"] : 7 );
	$graph[$row["tstamp"]] += $row["count"]; // "+" is here cuz we don't group by DATE(ld.tstamp)
}
//dbg(ac_prefix_replace($query),1);dbg($series,1);dbg($graph);
$smarty->assign("series", $series);
$smarty->assign("graph", $graph);

?>
