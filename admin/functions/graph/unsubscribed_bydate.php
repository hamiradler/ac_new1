<?php

$liststr = implode("','", $admin["lists"]);
$period  = intval(ac_http_param("period"));
$from    = strval(ac_http_param("from"));
$to      = strval(ac_http_param("to"));


$listid    = (int)ac_http_param("id");
$mode      = (string)ac_http_param("mode");
$filterid  = (int)ac_http_param("filterid");

$range     = 'all';

$so = new AC_Select();
if ( $mode == 'report_list' ) {
	if ( $filterid ) {
		$so = select_filter_comment_parse($so, $filterid, $mode);
		if ( isset($so->graphfrom)     ) $from   = $so->graphfrom;
		if ( isset($so->graphto)       ) $to     = $so->graphto;
		if ( isset($so->graphperiod)   ) $period = $so->graphperiod;
		if ( isset($so->graphmode)     ) $range  = $so->graphmode;
	}
}

$series = array();
$graph  = array();
//dbg($so, 1);dbg("$period; $from = $to", 1);

if ( $range == 'day' ) { // this never happens, range is hardcoded to 'all' above
	ac_graph_prepare_timeline($series, $graph, $period, $from, $to);
} else {
	ac_graph_prepare_dateline($series, $graph, $period, $from, $to);
}

$cond = "";

if ( $mode == 'report_list' ) {
	if ( $listid > 0 ) {
		//$cond .= "AND sl.listid IN ( SELECT u.userid FROM #user_group u WHERE u.groupid = '$listid' ) ";
		$liststr = $listid;
	}
	if ( count($so->conds) > 1 ) {
		$f = $so->conds[1];
		// apply list filter
		$cond  = "AND sl.listid IN ( SELECT l.id FROM #list l WHERE 1 $f ) ";
	}
}

$format = ( $range == 'day' ? '%H' : '%m/%d' );
$query = "
	SELECT
		DATE_FORMAT(sl.udate, '$format') AS cdate,
		DATEDIFF('$to', sl.udate) AS diff,
		COUNT(*) AS count
	FROM
		#subscriber_list sl
	WHERE
		sl.status = 2
	AND sl.listid IN ('$liststr')
	AND DATE(sl.udate) > '$from'
	AND sl.udate < ('$to' + INTERVAL 1 DAY)
	$cond
	GROUP BY
		DATE(sl.udate)
";
$rs   = ac_sql_query($query);

$count = 0;
$total = intval(ac_http_param("total")) > 0;

while ($row = ac_sql_fetch_assoc($rs)) {
	if ($total)
		$count += $row["count"];
	else
		$count = $row["count"];

	$series[$row["diff"]] = $row["cdate"];
	$graph[$row["diff"]] += $count;
}

if ($total) {
	$xcount = 0;
	foreach ($graph as $key => $count) {
		if ($count > $xcount)
			$xcount = $count;
		$graph[$key] = $xcount;
	}
}

if ( isset($_GET['json']) ) {
	krsort($series);
	krsort($graph);
	$series = array_values($series);
	$graph = array_values($graph);
}

$max = $cnt = $sum = $last = 0;
foreach ( $graph as $v ) {
	if ( $v > $max ) $max = $v;
	$sum += $v;
	$cnt++;
	$last = $v;
}
$avg = $sum / count($graph);

$extras = array(
	'avg' => round($avg, 2),
	'max' => $max,
	'cnt' => $cnt,
	'sum' => $sum,
	'last' => $last,
	'empty' => !(bool)$sum,
);


if ( !$sum && isset($_GET['json']) ) {
	$graph[0] = 1;
	$graph[1] = 1.1;
	$graph[2] = 1.1;
	$graph[3] = 1.4;
	$graph[4] = 1.3;
	$graph[5] = 1.2;
	$graph[6] = 1.1;
	$graph[7] = 1;
	$graph[8] = 1;
	$graph[9] = 1;
	$graph[10] = 1;
	$graph[11] = 1;
	$graph[12] = 1.1;
	$graph[13] = 1.15;
	$graph[14] = 1.2;
	$graph[15] = 1;
	$graph[16] = 1;
	$graph[17] = 1;
	$graph[18] = 1;
	$graph[19] = 1;
	$graph[20] = 1.1;
	$graph[21] = 1;
	$graph[22] = 1;
	$graph[23] = 1;
	$graph[24] = 1.1;
	$graph[25] = 1.2;
	$graph[26] = 1.3;
	$graph[27] = 1.1;
	$graph[28] = 1;
	$graph[29] = 1;
}


//dbg($series,1);dbg($graph);


$smarty->assign("series", $series);
$smarty->assign("graph", $graph);

//dbg(ac_prefix_replace($query), 1);dbg($series, 1);dbg(array_sum($graph), 1);dbg($graph);

?>
