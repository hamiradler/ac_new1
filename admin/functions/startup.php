<?php

function startup_recent_subscribers($limit) {
	$admin = ac_admin_get();
	$cond = "";

	if (!ac_admin_ismaingroup()) {
		$liststr = implode("','", $admin["lists"]);
		$cond = "AND sl.listid IN ('$liststr')";
	}

	$limit = (int)$limit;
	if ( $limit <= 0 ) $limit = 10;
	return ac_sql_select_array("
		SELECT
			s.id,
			s.email,
			CONCAT(sl.first_name, ' ', sl.last_name) AS a_fullname,
			l.name AS a_listname,
			sl.sdate
		FROM
			#subscriber_list sl FORCE INDEX (sdate),
			#subscriber s,
			#list l
		WHERE
			s.id = sl.subscriberid
		AND
			l.id = sl.listid
		$cond
		ORDER BY
			sl.sdate DESC
		LIMIT
			$limit
	", array('sdate'));
}

function startup_recent_campaigns($limit) {
	$admin   = ac_admin_get();
	$liststr = implode("','", $admin["lists"]);
	$status_array = campaign_statuses();
	$limit = (int)$limit;
	if ( $limit <= 0 ) $limit = 10;

	$ary = ac_sql_select_array($q = "
		SELECT
			c.id,
			c.name,
			c.status,
			c.sdate
		FROM
			#campaign c
		WHERE
		  (
				c.userid = $admin[id]
				OR
				c.id IN (SELECT cl.campaignid FROM #campaign_list cl WHERE cl.listid IN ('$liststr'))
			)
		AND
			c.status = 0
		ORDER BY
			c.sdate DESC
		LIMIT
			$limit
	", array('sdate'));

	foreach ($ary as $k => $v) {
		$ary[$k]["a_lists"] = implode(", ", ac_sql_select_list("
			SELECT
				l.name
			FROM
				#list l,
				#campaign_list cl
			WHERE
				l.id = cl.listid
			AND
				cl.campaignid = '$v[id]'
		"));

		$ary[$k]["a_lists_short"] = ac_str_shorten($ary[$k]["a_lists"], 30);
		if (isset($status_array[$v["status"]])) {
			$ary[$k]["a_statusname"] = $status_array[$v["status"]];
		} else {
			$ary[$k]["a_statusname"] = _p("[Unknown]");
		}
	}

	return $ary;
}

function startup_viable() {
	$plink = ac_site_plink();
	$port  = 80;

	if (strpos($plink, "http://") !== false) {
		$tmp = substr($plink, 7);
	} elseif (strpos($plink, "https://") !== false) {
		$tmp = substr($plink, 8);
		$port = 443;
	}

	$tmp  = explode("/", $tmp);
	$addr = $tmp[0];

	# Check if $addr has a port number in it...
	if (strpos($addr, ":") !== false) {
		$tmp  = explode(":", $addr);
		$addr = $tmp[0];
	}

	$url  = $plink . "/ac_global/scripts/readable.php";

	$rval = ac_http_viable($addr, $port, $url);
	return $rval;
}

function startup_rewrite() {
	$rval = array(
		"result" => true,
		"shortreason" => "",
		"explanation" => "",
	);

	$site = $GLOBALS["site"];

	if ($site['general_url_rewrite']) {
		$plink = ac_site_plink();
		$url   = $plink . "/rewritetest/104";
		$rval  = ac_http_testdata($url, "<!-- ac:em:rewrite:test -->");
	}

	return $rval;
}

function startup_invoices() {
	require_once(dirname(dirname(__FILE__)) . '/manage/_functions.php');

	$url = hosted_invoice_url();

	$str = ac_http_get($url);

	return $str;
}

function startup_walkthrough() {
	return false;
}

function startup_walkthrough_close($alertid) {
	global $admin;

	$alertid = (int)$alertid;
	if ( !$alertid ) return ac_ajax_api_result(false, _a("Walkthrough not provided."));

	$qry = "SELECT * FROM #walkthrough_rel WHERE userid = '$admin[id]' AND alertid = '$alertid'";
	$walkthrough = ac_sql_select_row($qry);
	if ( $walkthrough ) return ac_ajax_api_result(true, _a("Walkthrough already closed."));

	$in = array(
		'id' => 0,
		'userid' => $admin['id'],
		'alertid' => $alertid,
	);
	ac_sql_insert("#walkthrough_rel", $in);

	return ac_ajax_api_result(true, _a("Walkthrough has been marked as closed."));
}

?>
