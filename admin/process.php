#!/usr/local/bin/php
<?php

// require main include file
require_once(dirname(__FILE__) . '/prepend.inc.php');
require_once(ac_global_functions('process.php'));
require_once ac_global_functions("cron.php");
require_once ac_admin("functions/mailer/campaignmailer.php");

ac_flush('');

// turning off some php limits
@ignore_user_abort(1);
@ini_set('max_execution_time', 950 * 60);
@set_time_limit(950 * 60);
$ml = ini_get('memory_limit');
if ( $ml != -1 and (int)$ml < 128 and substr($ml, -1) == 'M') @ini_set('memory_limit', '128M');
set_include_path('.');

// admin permission reset (use admin=1!)
$admin = ac_admin_get_totally_unsafe(1);

// Preload the language file
ac_lang_get('admin');

$id = (int)ac_http_param('id');
$debug = (bool)ac_http_param('debug');



if ( $id == 0 ) {
	if ( isset($_SERVER['argv'][1]) ) {
		$id = (int)$_SERVER['argv'][1];
		if ( !$id or strlen("$id") != strlen($_SERVER['argv'][1]) ) {
			if ( isset($_SERVER['argv'][2]) ) {
				$id = (int)$_SERVER['argv'][2];
				if ( isset($_SERVER['argv'][3]) ) $debug = (bool)$_SERVER['argv'][3];
			}
		} else {
			if ( isset($_SERVER['argv'][2]) ) $debug = (bool)$_SERVER['argv'][2];
		}
	}
}

if ( $debug ) $_GET['debugspawn'] = 1;

if ( $id == 0 ) {
	ac_flush('');

	ac_cron_monitor_start(basename(__FILE__, '.php')); // log cron start
	ac_process_respawn(null, $debug); // this triggers to run all process

	// Double-check that there are no bad processes.
	$rs = ac_sql_query("SELECT id, data FROM #process WHERE percentage < 100 AND ldate < (NOW() - INTERVAL 1 HOUR)");
	while ($row = ac_sql_fetch_assoc($rs)) {
		$data = @unserialize($row["data"]);

		if ($data === false || !is_int($data))
			continue;

		$camp = ac_sql_select_row("SELECT id, status FROM #campaign WHERE sendid = '$data' AND status = 4");

		if ($camp)
			ac_process_remove($row["id"]);

		// How about if there is no campaign, period?
		$camp = ac_sql_select_row("SELECT id, status FROM #campaign WHERE sendid = '$data'");

		if (!$camp)
			ac_process_remove($row["id"]);
	}
	
	ac_cron_monitor_stop(); // log cron end
} else {
	ac_flush('.');
	ac_process_pickup($id); // this runs process
}

ac_process_cleanup(); // this removes old&finished processes

?>
