<?php

// Notes for the future:
// em_import_column -- table of fields to map.
// em_import_list -- lists we're importing into.

class Import extends Result {
	const ip4_localhost = 2130706433;

	private $mapfields;
	private $lists;

	private $insub;
	private $inlist;

	private $percent;

	public function __construct($ary = array()) {
		parent::__construct($ary);

		$this->mapfields = array();
		$this->lists = array();

		$this->percent = null;
	}
	
	// Static functions
	// --

	public static function current() {
		$admin = ac_admin_get();

		reset($admin["groups"]);
		$groupid = (int)current($admin["groups"]);

		$sel = new Select("Import");
		$sel->where(
			array("groupid = '%s'", $groupid)
		);
		$sel->orderby("id DESC");
		$sel->limit(10);

		return $sel->export();
	}

	// The subscriber results for a given import, based on processid.  Returns 0/0 if no process is left.
	public static function log($importid) {
		$import = Cache::lookup("Import", $importid);
		$rval = ac_sql_select_row("
			SELECT
				(SELECT COUNT(*) FROM #subscriber_import WHERE processid = '{$import->processid}' AND res = 1) AS success
		");

		// Specifically, make this a zero for the return... success may be an 
		// empty string if there was no process.
		if (!$rval["success"])
			$rval["success"] = 0;

		$rval["failrows"] = ac_sql_select_array("
			SELECT
				email,
				msg
			FROM
				#subscriber_import
			WHERE
				processid = '{$import->processid}'
			AND
				res = '0'
			LIMIT
				1000
		");
		$rval["failed"] = count($rval["failrows"]);

		return $rval;
	}

	// Non-static
	// --

	public function export() {
		$rval = parent::export();
		$rval["percent"] = $this->percent();

		return $rval;
	}

	public function percent() {
		if ($this->percent === null) {
			$rs = ac_sql_query("SELECT percentage FROM #process WHERE id = '{$this->processid}'");
			if (ac_sql_num_rows($rs) == 0) {
				// This process has been removed, so we'll assume it completed.
				$this->percent = 100;
			} else {
				$row = ac_sql_fetch_assoc($rs);
				$this->percent = min(100, (int)$row["percentage"]);
			}
		}

		return $this->percent;
	}
	
	// Given an input array ($data), map the data inside it into subscriber 
	// data fields according to the contents of the em_import_map table.
	public function map($data) {
		if (!$data || !is_array($data))
			return;

		if (!$this->mapfields)
			$this->mapfields = ac_sql_select_array("SELECT * FROM #import_map WHERE importid = '{$this->id}'");

		if (!$this->lists)
			$this->lists = ac_sql_select_list("SELECT listid FROM #import_list WHERE importid = '{$this->id}'");

		$this->inlist = array();
		$this->insub = array(
			"id" => 0,
			"=cdate" => "NOW()",
		);

		foreach ($this->lists as $listid) {
			$this->inlist[$listid] = array(
				"id" => 0,
				"=sdate" => "NOW()",
				"ip4_sub" => self::ip4_localhost,
				"ip4_last" => self::ip4_localhost,
			);
		}

		foreach ($this->mapfields as $field) {
			$src = $field["src"];

			if (!isset($data[$src]))
				continue;

			$val = $data[$src];
			$dst = explode(".", $field["dst"]);

			if (count($dst) < 2)
				continue;

			switch ($dst[0]) {
				case "sub":		// Subscriber info
					$this->map_sub($dst[1], $val);
					break;

				case "field":	// Custom fields
					$this->map_field($dst[1], $val);
					break;

				default:
					break;
			}
		}

		// There's no email field to map with.  Skip.
		if (!$this->insub["id"] && !isset($this->insub["email"]))
			return;

		ac_sql_insert_update("#subscriber", $this->insub);

		foreach ($this->inlist as $k => $v) {
			ac_sql_insert_update("#subscriber_list", $v);
		}
	}

	// Map the input data ($val) to a generic subscriber field ($dst).  
	// It's either going to update a field in em_subscriber or 
	// em_subscriber_list.
	function map_sub($dst, $val) {
		switch ($dst) {
			case "email":
				$esc = ac_sql_escape($val);
				$this->insub["id"] = (int)ac_sql_select_one("SELECT id FROM 
					#subscriber WHERE email = '$esc'");

				if ($this->insub["id"] > 0) {
					unset($this->insub["=cdate"]);
					foreach ($this->inlist as $k => $v) {
						$this->inlist[$k]["id"] = (int)ac_sql_select_one("SELECT id FROM #subscriber_list WHERE subscriberid = '$this->insub[id]' AND listid = '$k'");

						if ($this->inlist[$k]["id"] > 0) {
							unset($this->inlist[$k]["=sdate"]);
							unset($this->inlist[$k]["ip4_sub"]);
							unset($this->inlist[$k]["ip4_last"]);
						}
					}
				} else {
					$this->insub["email"] = $val;
				}

				break;

			case "first_name":
			case "last_name":
				foreach ($this->inlist as $k => $v)
					$this->inlist[$k][$dst] = $val;

				break;

			case "ip4_sub":
			case "ip4_last":
			case "ip4_unsub":
				$val = inet_pton($val);

				if ($val !== false) {
					foreach ($this->inlist as $k => $v)
						$this->inlist[$k][$dst] = $val;
				}

				break;

			case "sdate":
				$val = strtotime($val);

				if ($val !== false) {
					$val = date("Y-m-d H:i:s", $val);

					foreach ($this->inlist as $k => $v)
						$this->inlist[$k][$dst] = $val;
				}

				break;
		}
	}

	function map_field($dst, $val) {
	}
}

?>
