<?php

class Select {
	private $rs;

	private $cache;
	private $cpos;

	public $exprs;
	public $tables;
	public $conds;

	public $leftjoins;

	public $order;
	public $limit;
	public $offset;

	public $class;

	public function __construct($class) {
		$this->class = $class;

		$this->tables = array(class_table($class) . " t");
		$this->exprs = array();
		$this->conds = array("1");		// E.g., WHERE 1
		$this->leftjoins = array();

		$this->limit = 0;
		$this->offset = 0;
		$this->order = "";
		$this->group = "";
		$this->having = "";

		$this->rs = null;
		$this->cache = null;
		$this->cpos = 0;
	}

	public function from() {
		$args = func_get_args();

		foreach ($args as $arg)
			$this->tables[] = $arg;
	}

	public function leftjoin($table, $cond) {
		$this->leftjoins[] = "$table ON $cond";
	}

	public function select() {
		$args = func_get_args();

		foreach ($args as $arg)
			$this->exprs[] = $arg;
	}

	public function where() {
		$args = func_get_args();

		foreach ($args as $arg) {
			if (is_array($arg)) {
				$fmt = array_shift($arg);
				while (count($arg) > 0) {
					$repl = ac_sql_escape(array_shift($arg));
					$pos = strpos($fmt, "%s");

					if ($pos !== false)
						$fmt = substr_replace($fmt, $repl, $pos, strlen($pos));
				}

				$this->conds[] = $fmt;
			} else {
				$this->conds[] = $arg;
			}
		}
	}

	public function limit($limit, $offset = 0) {
		$this->limit = $limit;
		$this->offset = $offset;
	}

	public function orderby($col) {
		$this->order = $col;
	}

	public function groupby($col, $having = '') {
		$this->group = $col;
		$this->having = $having;
	}

	public function compile() {
		$tables = implode(", ", $this->tables);
		$exprs = implode(", ", $this->exprs);
		$conds = implode(" AND ", $this->conds);

		if ($exprs == "")
			$exprs = "*";

		$rval = "SELECT $exprs FROM $tables WHERE $conds";

		if ($this->group) {
			$rval .= " GROUP BY {$this->group}";

			if ($this->having)
				$rval .= " HAVING {$this->having}";
		}
		
		if ($this->order)
			$rval .= " ORDER BY {$this->order}";

		if ($this->limit > 0)
			$rval .= " LIMIT {$this->offset}, {$this->limit}";

		return $rval;
	}

	public function pprint() {
		$tables = implode(",\n    ", $this->tables);
		$exprs = implode(",\n    ", $this->exprs);
		$conds = implode("\nAND\n    ", $this->conds);

		if ($exprs == "")
			$exprs = "*";

		$rval = "SELECT\n    $exprs\nFROM\n    $tables\nWHERE\n    $conds";

		if ($this->group) {
			$rval .= "\nGROUP BY\n    {$this->group}";

			if ($this->having)
				$rval .= "\nHAVING\n    {$this->having}";
		}
		
		if ($this->order)
			$rval .= "\nORDER BY\n    {$this->order}";

		if ($this->limit > 0)
			$rval .= "\nLIMIT\n    {$this->offset}, {$this->limit}";

		return $rval;
	}

	public function next() {
		if ($this->rs === null) {
			if ($this->cache === null)
				$this->cache = Cache::query($this->class, $this->compile());

			if (!$this->cache)
				$this->rs = ac_sql_query($this->compile());
		}

		if ($this->cache) {
			if (!isset($this->cache[$this->cpos]))
				return null;

			$rval = $this->cache[$this->cpos++];
			return $rval;
		} else {
			if ($this->rs === false)
				return null;

			$row = ac_sql_fetch_assoc($this->rs);

			if ($row) {
				$rval = new $this->class($row);

				if (isset($row["id"]))
					Cache::set($this->class, $rval->id, $rval);

				Cache::setquery($this->class, $this->compile(), $rval);
				return $rval;
			} else {
				return null;
			}
		}
	}

	public function rows() {
		$rval = array();

		while ($row = $this->next())
			$rval[] = $row;

		return $rval;
	}

	public function export() {
		$rval = array();

		while ($row = $this->next())
			$rval[] = $row->export();

		return $rval;
	}
}

?>
