<?php

class User extends Result {
	const COOKIE = "em_acp_globalauth_cookie";
	
	// Static functions
	// --

	// Create a new api key hash for this user and assign that into the database.
	public static function resetapi($userid) {
		$user = Cache::lookup("User", $userid);

		$user->regenkey();
		$user->update();
		return $user->apikey;
	}

	public static function api_lookup($key) {
		// Don't bother.
		if ($key == "")
			return null;

		$sel = new Select("User");
		$sel->where(
			array("apikey = '%s'", $key)
		);
		$sel->limit(1);

		return $sel->next();
	}

	// Non-static
	// --

	public function regenkey($current_time = true) {
		for ($i = 0, $limit = 5; $i < $limit; $i++) {
			$input1 = sprintf("%s%s", rand(), $current_time ? date("YmdHis") : date("YmdHis", rand(1000000, 100000000)));
			$input2 = sprintf("%s%s", date("YmdHis", time() - rand(100000, 10000000)), rand());

			$this->apikey = strtolower(md5($input1) . sha1($input2));

			$sel = new Select("User");
			$sel->where(array("apikey = '%s'", $this->apikey));
			$sel->limit(1);

			if (!$sel->next())
				break;
		}
	}

	// The code necessary to make us think that we're logged in.
	public function login() {
    	$key = md5("acp_" . $this->username) . $this->absid;
		$_SESSION["globalauth_$key"] = true;

		if (@setcookie(self::COOKIE, $key, 0, "/")) {
			$_COOKIE[self::COOKIE] = $key;
		} else {
			@setcookie(self::COOKIE, "", time() - 1296000, "/");
			unset($_COOKIE[self::COOKIE]);
		}
	}
}

?>
