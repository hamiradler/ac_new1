<?php
if (!@ini_get("zlib.output_compression")) @ob_start("ob_gzhandler");

define('ACPUBLIC', true);
if ( !isset($_GET['useauth']) ) define('ACP_USER_NOAUTH', true);

// require main include file
require_once('admin/prepend.inc.php');
require_once ac_global_functions("http.php");
require_once ac_admin("functions/form.php");

$id = (int)ac_http_param("id");
$pageid = (string)ac_http_param("pageid");
$appid = (string)ac_http_param("appid");

if ($id < 1)
	exit;

$GLOBALS["form_compile_view"] = "working";
$parts = form_compile($id);

if (count($parts) == 0)
	die("!!form does not exist");

$form = form_select_row($id);

if (!$form)
	exit;

if (!ac_str_instr($pageid, $form["facebookurl"])) {
	// Looks like our page changed.  Let's update it.

	// See if we can find the page url based on the id.
	$graph = ac_http_get("https://graph.facebook.com/$pageid");
	$pageurl = "";

	if ($graph != "false") {
		$json = @json_decode($graph);

		if (isset($json->link))
			$pageurl = $json->link . "?sk=app_$appid";
	}

	$up = array(
		"facebookurl" => $pageurl,
	);

	ac_sql_update("#form", $up, "id = '$id'");
}

$extra = array(
	"s" => "",
	"c" => "0",
	"m" => "0",
	"act" => "sub",
	"lists" => ac_sql_select_list("SELECT listid FROM #form_list WHERE formid = '$form[id]'"),
	"cal" => false,
);

$rval = form_header($form, $extra);

if ($form["widthpx"] > 0) {
	//$rval = preg_replace('#width:\d+px;/*F*/#', 'width:' . $form["widthpx"] . 'px;', $rval);
	$rval = str_replace("<div class='_form'>", "<div class='_form' style='width:$form[widthpx]px'>", $rval);
}

echo $rval;

foreach ($parts as $part) {
	echo $part["html"];
}

echo form_footer($form);

?>
