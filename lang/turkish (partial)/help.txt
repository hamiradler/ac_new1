##### TRANSLATION STRINGS #####
##### Example: "original string" = "your-translation-here" #####

"(Optional) Specify how many emails you wish to send with this mail connection before looking for the next connection. If you only have one mail connection this setting does not matter." = ""
"(Optional) You can specify a limit of the number of emails to send in a certain time period. (Example = Only allow 1,000 emails per hour)" = ""
"Attachments will be disabled." = ""
"Brief Description for you to recognize. Does NOT affect your actual personalization tag." = ""
"Brief Description for you to recognize. Does NOT affect your actual template." = ""
"Enter a brief description for this bounce code here." = ""
"GD Library is needed to generate images from the application. Most common (and useful) use are CAPTCHA images." = ""
"Header needs to be in format 'HEADER-NAME: HEADER VALUE'" = ""
"If authentication is not required, do not enter a password." = ""
"If authentication is not required, do not enter a username." = ""
"If selected, the software will disable most functions such as deleting, sending and more." = ""
"If the URL is used, it has to start with http:// or https:// . Otherwise, a file path will be assumed. If a path starts with './', the application folder will be assumed." = ""
"If this option is checked, the person filling the form will not be able to proceed with form submission unless the field has some value entered." = ""
"If your mail transfer agents does not include the code in form 5.X.X., enter a string that should be matched so this Bounce Code can be assigned.\n(example: ' returned: Unknown User')" = ""
"License agreement does not allow you to set your own copyright, but you may opt to have the copyright invisibe to public/non-code view. Copyrights may never be removed from the actual code." = ""
"Logs will be kept for 30 days." = ""
"Most mail transfer agents include the code in form 5.X.X. If your server is not returning those, you can enter here some identifier for this Bounce Code." = ""
"Note: this overrides the 'day of the month' option" = ""
"Notice: Hold CTRL to select multiple destinations." = ""
"Notice: Hold CTRL to select multiple items." = ""
"Notice: Hold CTRL to select multiple lists." = ""
"Notice: Hold CTRL to select multiple statuses." = ""
"Notice: This action will be performed on each selected list! Hold CTRL to select multiple lists." = ""
"Notice: This custom header will be a member of each selected list! Hold CTRL to select multiple lists." = ""
"Notice: This email account will be a member of each selected list! Hold CTRL to select multiple lists." = ""
"Notice: This message will be a member of each selected group! Hold CTRL to select multiple group." = ""
"Notice: This personalization will be a member of each selected list! Hold CTRL to select multiple lists." = ""
"Please check if any vital function is listed here." = ""
"Sessions need to be enabled in order for this application to work." = ""
"Set * for 'every hour'." = ""
"Set * for 'every minute'." = ""
"Soft bounces usually are emails that were not delivered (yet), but the problem appears to be temporal. Hard bounces are the ones that most likely permanent (such as 'invalid email address', etc.)" = ""
"Some scripts might not finish the execution within the allowed timeframe, which is set to a value lower than a default PHP value (30 seconds)." = ""
"Subscriber will receive an opt-in email with a link to confirm list subscription(s)." = ""
"The contents of this field will show up as a tooltip when visitor hovers over the field." = ""
"This is just a label for your own internal use" = ""
"This is the character that separates the fields within the file. Usually this is a comma." = ""
"This is the internal name for you to use. Every cron job needs to have a unique identifier." = ""
"This value is too low, and this server imposed limit cannot be changed." = ""
"This will turn this Cron Job on or off" = ""
"Upon saving, the system will try to add this content to your .htaccess file if it doesn't have it already. If the file cannot be written to, you will have to put this into file %s" = ""
"With safe mode on, the script will not be able to adjust the server configuration for optimal performance." = ""
"You are responsible for escaping! Also, if constructing a LIST, then strings should be encapsulated with quotes." = ""
"Your server imposes the following limits: maximum file upload size is %s and maximum post size is %s." = ""
"Your server is set to allow less memory than this script requires." = ""



##### 2009-05-28 #####

"%REPORTLINK% is shorthand for the shared report link that you see above." = ""



##### 2009-07-10 #####




##### 2009-07-14 #####

"(Optional) This option will automatically pause your mailing after a certain number of messages.  This helps reduce your mail servers load." = ""
"The label of this field (a title) can be shown both above the field, or on its left side." = ""



##### 2009-08-27 #####

"This feature will use Apache's mod_rewrite plugin to reformat its public URLs to appear more descriptive, mentioning for example the title of a list or form in the URL." = ""



##### 2009-11-25 #####




##### 2009-12-21 #####

"Begin typing their name and live results will be displayed." = ""



##### 2010-03-28 #####




##### 2010-04-07 #####




##### 2010-07-02 #####

"In some cases, particularly Active Directory, the user name you normally log in with is not actually what the server expects through its normal LDAP interface.  This field is the name of the attribute that you want to use to log in with.  In most of those cases, the value which must go here would be 'samAccountName'." = ""
"In some cases, particularly with Active Directory, we must log in with an administrative account before we can truly log in with a user.  In that case, you would place here the full distinguished name of the LDAP server's administrative user (e.g. cn=Administrator,ou=Users,dc=domain,dc=com)." = ""
"The attribute that the LDAP server expects us to log in with.  In the vast majority of cases, this field should be 'cn' (short for 'common name')." = ""
"The base of the distinguished name that users would log in with; e.g., ou=Users,dc=domain,dc=com" = ""
"The full distinguished name of an administrator account, necessary for Active Directory's login process.  Example: cn=Administrator,ou=Users,dc=domain,dc=com" = ""
"The password for the administrator account, mentioned above." = ""
"The password of the administrative user mentioned above." = ""
"With this checked, we will log in with a fully-formed distinguished name (cn=user,ou=Users,dc=domain,dc=com); without it, we'll log in with 'user' and let the server sort things out." = ""



##### 2010-09-07 #####

"The location of this software on the web. To change this, run Updater from a new location." = ""



##### 2010-09-30 #####




##### 2010-12-01 #####




##### 2011-02-23 #####




##### 2011-07-13 #####

"This button will re-import all stock templates. PLEASE NOTE: This will only import global stock templates you are missing. If you already have some stock templates, but have modified them, it will NOT overwrite customizations made." = ""
"This value will be used as a placeholder for this personalization field. If you enter 'MYTAG', then your content should have a placeholder %MYTAG% that would be replaced with a field value. NOTE: spaces will be replaced with a dash, and % characters are not allowed." = ""



##### 2011-08-01 #####




##### 2011-11-03 #####




##### 2011-12-12 #####

"See instructions for the appropriate field name to include here." = ""
