<?php

require_once(ac_global_classes('page.php'));

class rewritetest_context extends ACP_Page {
	function rewritetest_context() {
		$this->pageTitle = _p("Rewrite Test");
		parent::ACP_Page();
		$this->getParams();
	}

	function getParams() {
	}

	function process(&$smarty) {
		$this->setTemplateData($smarty);
		$smarty->assign("content_template", "rewritetest.htm");
	}
}

?>
