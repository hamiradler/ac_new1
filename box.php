<?php

define('ACPUBLIC', true);
define('ACP_USER_NOAUTH', true);

// require main include file
require_once(dirname(__FILE__) . '/admin/prepend.inc.php');


if (ac_http_param_exists('p'))
	$_POST['f'] = ac_http_param('p');

if (ac_http_param_exists('imgverify'))
	$_POST['captcha'] = ac_http_param('imgverify');

if (ac_http_param_exists('funcml')) {
	$funcml = ac_http_param('funcml');
	switch ($funcml) {
		default:
			$_POST['act'] = $funcml;
			break;

		case "add":
			$_POST["act"] = "sub";
			break;

		case "unsub2":
			$_POST["act"] = "unsub";
			break;
	}
}

include(dirname(__FILE__) . "/proc.php");
exit;

?>
