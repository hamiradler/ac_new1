<?php
if ( !defined('ACPUBLIC') ) define('ACPUBLIC', true);
if ( !defined('ACP_USER_NOAUTH') ) define('ACP_USER_NOAUTH', true);

// require main include file
require_once(dirname(__FILE__) . '/admin/prepend.inc.php');

require_once(ac_global_functions('ajax.php'));
require_once ac_global_functions("custom_fields.php");
require_once(ac_global_functions('gravatar.php'));

require_once ac_admin("functions/list.php");
require_once ac_admin("functions/subscriber.php");
require_once ac_admin("functions/form.php");
require_once ac_admin("functions/forward.php");

// Preload the language file
ac_lang_get('public');

// Character conversions if subscription form is using a different charset
$in = ac_http_param('_charset');
$out = _i18n("utf-8");

if( $in != $out ) ac_charset_convert_gp($in, $out);


// collect input
$campaignid = (int)ac_http_param('c');
$messageid = (int)ac_http_param('m');
$listid = (int)ac_http_param('nl');
$formid = (int)ac_http_param('f');
$action = (string)ac_http_param("act");
$nlbox  = ac_http_param_forcearray("nlbox");

if ( !$nlbox ) {
	// coming from (un)subscribe link
	$nlbox = array_diff(array_map('intval', explode(',', $listid)), array(0));
} else {
	$x = ( is_array($nlbox) ? $nlbox : explode(',', $nlbox));
	// coming from subscription form
	$nlbox2 = array_diff(array_map('intval', explode(',', $listid)), array(0));
	$nlbox = array_diff(array_map('intval', $x), array(0));
	$nlbox = array_unique(array_merge($nlbox, $nlbox2));
	foreach($nlbox as $y)
	{
		if(!isset($admin['lists'][$y]))
			$admin['lists'][$y] = $y;
	}
}

// Still don't know?  Grab all the lists from the form.
if ((!$nlbox && $formid > 0) || $action == "csub") {
	// Wait.  If we're subscribing and we don't know the lists, then this should be an error.
	if ($action != "sub" && $action != "unsub")
		$nlbox = ac_sql_select_list("SELECT listid FROM #form_list WHERE formid = '$formid'");
}

$listid = implode(',', $nlbox);
$listidsql = implode("','", $nlbox);

$nl = (int)$listid;
if ( $campaignid ) {
	$_SESSION['nlp'] = ac_sql_select_list("SELECT listid FROM #campaign_list WHERE campaignid = '$campaignid'");
	if ( !$_SESSION['nlp'] ) $_SESSION['nlp'] = null;
} elseif ( $nl ) {
	$_SESSION['nlp'] = ( $nl == $listid ? $nl : $nlbox );
}
require(ac_admin('functions/inc.branding.public.php'));


// subscriber id
$id = trim((int)ac_http_param('id'));
// subscriber hash
$hash = trim((string)ac_http_param('s'));
// subscriber email
$email = trim((string)ac_http_param('email'));
$email = str_replace(chr(0), "", $email);	# This happens from time to time.  Don't ask why--I have no idea.
if ( $email ) { // fix email
	if ( !ac_str_is_email($email) ) {
		$email = ac_b64_decode($email);
		if ( !ac_str_is_email($email) ) {
			$email = '';
		}
	}
}
else {
	$email = '';
}

/* find subscriber(id) */
$subscriber = false;
$fromhash = false;
// by hash
if ( !$subscriber and $hash ) {
	$subscriber = subscriber_exists($hash, $nlbox, 'hash');
	$fromhash = true;
}
// by email
if ( !$subscriber and $email ) {
	$subscriber = subscriber_exists($email, $nlbox, 'exact');
}
// by id
if ( !$subscriber and $id ) {
	$subscriber = subscriber_select_row($id);
}

// hardcode hash and email
if ( isset($subscriber['email']) ) {
	$email = $subscriber['email'];
	//$hash = ($subscriber['subscriberid']) ? md5($subscriber['subscriberid'] . $subscriber['email']) : md5($subscriber['id'] . $subscriber['email']);
	$hash  = $subscriber['hash'];
}
$subscriberid = ( isset($subscriber['subscriberid']) ? $subscriber['subscriberid'] : 0 );

// captcha
$captcha = md5(strtoupper((string)ac_http_param('captcha')));
// check based on the lists selected
$usecaptcha = (int)ac_sql_select_one('=SUM(p_use_captcha)', '#list', "id IN ('$listidsql')") > 0;
if ( !$site['gd'] ) $usecaptcha = false;

// unsubscribe reason
$unsubreason = ac_http_param('reason');

if ($unsubreason == "other")
	$unsubreason = ac_http_param("explanation");

// subscription form id
$fid = $formid;
// If subscription form, override list captcha settings with that of form
if ( $formid ) {
	$usecaptcha = (int)ac_sql_select_one("SELECT COUNT(*) FROM #form_part WHERE formid = '$formid' AND builtin = 'captcha'");
}

$lists = $codes = '';
$extra = array();
/*$ask2confirm = */$ask4reason = false;

$form = null;

$ip = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : '127.0.0.1';


switch ( $action ) {

	default:
		if (count($nlbox) > 0) {
			// Just to be safe, redirect to the unsubscribe page...
			$listid = $nlbox[0];
			ac_http_redirect(rewrite_plink("unsubscribe", "listid=$listid"));
		} else {
			ac_http_redirect(ac_site_plink());
		}

		break;

	case 'sub':
		$continue = true;
		// use captcha
		if ($usecaptcha) {
			if ( isset($_SESSION["image_random_value"]) ) {
				if ( !isset($_SESSION["image_random_value"][$captcha]) ) {
					$continue = false;
					$lists = '0';$codes = '18';
					$extra = array(
						0 => array(
							"error_code" => 18,
							"message" => _a("We were not able to confirm the captcha")
						)
					);
					break;
				}
			}
			else {
				$continue = false;
				$lists = '0';$codes = '25';
				$extra = array(
					0 => array(
						"error_code" => 18,
						"message" => _a("Session timed out"),
					)
				);
			}
		}

		if ($continue) {
			$fullname  = trim((string)ac_http_param('fullname'));
			if ($fullname) {
				$fullname = explode(" ", $fullname);
				$firstname = array_shift($fullname);
				$lastname = implode(" ", $fullname);
			} else {
				$firstname = trim((string)ac_http_param('firstname'));
				$lastname  = trim((string)ac_http_param('lastname'));

				if ($firstname == "")
					$firstname = trim((string)ac_http_param('first_name'));

				if ($lastname == "")
					$lastname  = trim((string)ac_http_param('last_name'));
			}

			if ( !isset($_POST['field']) ) {
				$xf = array();
				foreach ( $_POST as $k => $v ) {
					if ( substr($k, 0, 6) == 'field_' ) {
						$tmparr = explode('_', substr($k, 6));
						if ( count($tmparr) == 2 ) {
							$xf[(int)$tmparr[0] . ',' . (int)$tmparr[1]] = $v;
						}
					}
				}
				if ( $xf ) $_POST['field'] = $xf;
			}
			$fields = ac_http_param_forcearray('field');

			$form = form_select_row($formid);
			if (!$form)
				ac_http_redirect(ac_site_plink());

			$subscribe = subscriber_subscribe(0, $email, $firstname, $lastname, $nlbox, $fid, $fields, true);
			foreach ( $subscribe as $k => $v ) {
				//if ( $v['confirm'] ) $ask2confirm = true;
				$subscriber = subscriber_exists($email, $nlbox, 'exact');
				if ( isset($subscriber['email']) ) {
					//$hash = md5($subscriber['id'] . $subscriber['email']);
					$hash = $subscriber['hash'];
				}
			}

			$succeeded = true;
			foreach ($subscribe as $listid => $v) {
				if (!$v["succeeded"]) {
					$succeeded = false;
					break;
				}
			}

			if ($succeeded) {
				require_once ac_admin("functions/bounce_management.php");
				bounce_management_reset($subscriber['id']);
			}

			if (!$form["sendoptin"] && $succeeded) {
				// These need to be confirmed immediately.
				$listary = list_select_array(null, $nlbox);
				$notifies = array();

				gravatar_cache($subscriber['id'], '25', null, '#subscriber', 'id');

				foreach ($listary as $list) {
					$up = array(
						"status" => 1,
					);
					ac_sql_update("#subscriber_list", $up, "subscriberid = '$subscriber[id]' AND listid = '$list[id]'");
					subscriber_action_dispatch("subscribe", $subscriber, $list["id"], null, null);

					if ( (int)$list["send_last_broadcast"] ) {
						// (re)send last broadcast message
						mail_campaign_send_last($subscriber, $list["id"]);
					}

					if ($list["subscription_notify"])
						$notifies[] = $list;
				}

				if (count($nlbox) > 0)
					mail_responder_send($subscriber, $nlbox, 'subscribe');

				if (count($notifies) > 0)
					mail_admin_send($subscriber, $notifies, 'subscribe');
			}

			$extra = $subscribe;
			list($lists, $codes) = subscriber_codes($subscribe);
		}

		// Override lists with whatever the form has for useconf.
		$useconf = (int)ac_sql_select_one($q = "SELECT listid FROM #form_list WHERE formid = '$formid' AND useconf = 1 LIMIT 1");

		if ($useconf)
			$lists = $useconf;

		break;

	case 'csub':
		$lists = ac_sql_select_list("SELECT listid FROM #form_list WHERE formid = '$formid'");
		$formlists = $lists;

		if (count($lists) == 0)
			ac_http_redirect(ac_site_plink());

		$liststr = implode("','", $lists);
		ac_sql_update("#subscriber_list", array("status" => 1), "status = 0 AND listid IN ('$liststr') AND subscriberid = '$subscriberid'");

		// Grab the lists again, as they may not have subscribed to everything.
		$lists = ac_sql_select_list("SELECT listid FROM #subscriber_list WHERE status = 1 AND listid IN ('$liststr') AND subscriberid = '$subscriberid'");

		$listary = list_select_array(null, $lists);
		$notifies = array();

		foreach ($listary as $list) {
			subscriber_action_dispatch("subscribe", $subscriber, $list["id"], null, null);

			if ( (int)$list["send_last_broadcast"] ) {
				// (re)send last broadcast message
				mail_campaign_send_last($subscriber, $list["id"]);
			}

			if ($list["subscription_notify"])
				$notifies[] = $list;
		}

		if (count($lists) > 0)
			mail_responder_send($subscriber, $lists, 'subscribe');

		if (count($notifies) > 0)
			mail_admin_send($subscriber, $notifies, 'subscribe');

		stats_activity_log('subscribe', $subscriberid, $listid, $did = (int)ac_sql_select_one("id", "#subscriber_list", "listid IN ('$liststr') AND subscriberid = '$subscriberid'"));

		gravatar_cache($subscriberid, '25', null, '#subscriber', 'id');

		// Now hang on.  We may need to push to a specific list for confirmation.
		if (count($lists))
			$lists = $lists[0];
		else
			$lists = $formlists[0];

		$codes = '';

		$useconf = (int)ac_sql_select_one("SELECT listid FROM #form_list WHERE useconf = 1 AND formid = '$formid'");
		if ($useconf)
			$lists = $useconf;

		require_once ac_admin("functions/bounce_management.php");
		bounce_management_reset($subscriber['id']);


		// Need to grab the subscribe confirmation form... as the input form at
		// this point will likely be the original subscribe form.
		$form = form_select_row_listid($lists, FORM_SUBSCRIBE_CONFIRM_RESULT);

		break;

	case 'unsub': // used $s (hash) to obtain $subscriberid
		if (!$subscriberid) {
			if ($fromhash) {
				// Probably an invalid s hash.  Just push to the list form.
				ac_http_redirect(rewrite_plink("unsubscribe", "listid=$listid"));
			}

			$_SESSION["unsubscribe_error_message"] = _a("No subscriber exists with that email address");
			ac_http_redirect(rewrite_plink("unsubscribe_error", "listid=$listid", "codes=0", "s="));
		}

		$form = form_select_row_listid($listid, FORM_UNSUBSCRIBE);

		if ( $campaignid ) {
			if(isset($_GET['ALL'])) //Unsubscribe from all of that user group's lists that you are on
			{
				$query = "
					SELECT
						s.listid
					FROM
						#subscriber_list s,
						#user_group ug,
						#list_group lg,
						#campaign c
					WHERE
						s.subscriberid = '$subscriberid'
					AND
						s.status='1'
					AND
						c.id = '$campaignid'
					AND
						c.userid = ug.userid
					AND
						ug.groupid = lg.groupid
					AND
						lg.listid = s.listid
					";
				$cnl = ac_sql_select_list($query);
				// if no lists are found, try joinging with deleted campaigns table instead
				if ( !$cnl ) {
					$query = "
						SELECT
							s.listid
						FROM
							#subscriber_list s,
							#user_group ug,
							#list_group lg,
							#campaign_deleted c
						WHERE
							s.subscriberid = '$subscriberid'
						AND
							s.status='1'
						AND
							c.id = '$campaignid'
						AND
							c.userid = ug.userid
						AND
							ug.groupid = lg.groupid
						AND
							lg.listid = s.listid
						";
				}
			}
			else //Unsubscribe just from that campaign's list(s)
			{
				// get campaign's lists instead
				$query = "
					SELECT
						c.listid
					FROM
						#campaign_list c,
						#subscriber_list s
					WHERE
						c.campaignid = '$campaignid'
					AND
						s.subscriberid = '$subscriberid'
					AND
						c.listid = s.listid
				";
			}
			$cnl = ac_sql_select_list($query);
			// if no lists are found, don't cancel out the initial list array
			if ( $cnl ) $nlbox = $cnl;
		}
		$unsubscribe = subscriber_unsubscribe($subscriberid, $email, $nlbox, null, $fid, $campaignid, $messageid);
		foreach ( $unsubscribe as $k => $v ) {
			//if ( $v['confirm'] ) $ask2confirm = true;
			if ( $v['reason' ] ) $ask4reason  = true;
		}
		list($lists, $codes) = subscriber_codes($unsubscribe);
		$extra = array("c" => $campaignid, "m" => $messageid);

		if ( $campaignid ) {
			// check if campaign has read tracking turned on
			if ( ac_sql_select_one("trackreads", "#campaign", "id = '$campaignid'") ) {
				// check if he read that campaign
				$readlinkid = (int)ac_sql_select_one("id", "#link", "campaignid = '$campaignid' AND messageid = 0 AND link = 'open'");
				if ( $readlinkid ) {
					// check if he read that campaign
					$found = (int)ac_sql_select_one("=COUNT(*)", "#link_data", "linkid = '$readlinkid' AND subscriberid = '$subscriberid'");
					if ( !$found ) {
						ac_http_spawn(ac_site_plink("lt.php?nl=$nl&c=$campaignid&m=0&s=$hash&l=open&_nogeo"));
					}
				}
			}
		}

		break;

	case 'unsub_result': // uses hash to obtain $subscriberid
		if (!$subscriberid)
			exit;

		$orig = ac_sql_select_list("SELECT unsubreason FROM #subscriber_list WHERE subscriberid = '$subscriberid' AND listid IN ('$listidsql')");
		ac_sql_update_one('#subscriber_list', 'unsubreason', $unsubreason, "subscriberid = '$subscriberid' AND listid IN ('$listidsql')");
		$lists = $listid;
		$codes = ac_http_param('codes');

		if (in_array('', $orig)) {
			// Looks like we never gave a reason before, so add one to the counter.
			ac_sql_query("UPDATE #campaign SET unsubreasons = unsubreasons + 1 WHERE id = '$campaignid'");
			ac_sql_query("UPDATE #campaign_deleted SET unsubreasons = unsubreasons + 1 WHERE id = '$campaignid'");
			ac_sql_query("UPDATE #campaign_message SET unsubreasons = unsubreasons + 1 WHERE campaignid = '$campaignid' AND messageid = '$messageid'");
		}
		break;

	case 'forward':
		$continue = true;
		// use captcha
		if ($usecaptcha) {
			if ( isset($_SESSION["image_random_value"]) ) {
				if ( !isset($_SESSION["image_random_value"][$captcha]) ) {
					$_SESSION["forward_error"] = _a("We were not able to confirm the captcha");
					ac_http_redirect(rewrite_plink("forward", "nl=$listid", "codes=0", "s=$subscriber[hash]", "c=$campaignid", "m=$messageid"));
				}
			}
			else {
				$_SESSION["forward_error"] = _a("We were not able to confirm the captcha");
				ac_http_redirect(rewrite_plink("forward", "nl=$listid", "codes=0", "s=$subscriber[hash]", "c=$campaignid", "m=$messageid"));
			}
		}
		if ( !$continue ) break;

		$name = ac_http_param("yrname");
		$email = $subscriber["email"];
		if ( !trim((string)$name) ) $name = $email;

		$cid = (int)ac_http_param("yrcampaign");
		$mid = (int)ac_http_param("yrmessage");

		$rcpt_names = ac_http_param_forcearray("rcpt_name");
		$rcpt_emails = ac_http_param_forcearray("rcpt_email");

		$lists = $listid;
		$codes = ac_http_param('codes');

		form_forward($subscriber["id"], $cid, $mid, $name, $email, $rcpt_names, $rcpt_emails);
		break;

	case 'update_request': // Just send an email to the guy
		if (!$subscriberid) {
			$_SESSION["update_request_error"] = _a("No such email address is subscribed to this list");
			ac_http_redirect(rewrite_plink("update_request", "listid=$listid"));
		}

		if ($usecaptcha) {
			if ( isset($_SESSION["image_random_value"]) ) {
				if ( !isset($_SESSION["image_random_value"][$captcha]) ) {
					$_SESSION["update_request_error"] = _a("We were not able to confirm the captcha");
					ac_http_redirect(rewrite_plink("update_request", "listid=$listid"));
				}
			}
			else {
				$_SESSION["update_request_error"] = _a("Session timed out");
				ac_http_redirect(rewrite_plink("update_request", "listid=$listid"));
			}
		}

		$form = form_select_row_listid($listid, FORM_DETAILS_REQUEST);

		$lists = $listid;
		$codes = ac_http_param('codes');
		$tags = array(
			"%LINK%" => "%PLINK%/index.php?action=update&lists=$lists&codes=$codes&s=$subscriber[hash]",
		);
		form_send_message($listid, FORM_DETAILS_REQUEST, $subscriber, $tags);
		break;

	case 'update':
		if (!$subscriberid)
			exit;

		$form = form_select_row_listid($listid, FORM_DETAILS);

		$lists = $listid;
		$codes = ac_http_param('codes');

		// Handle any changes to the subscriber table.
		$up = array();

		if (isset($_POST["email"]) && $subscriber["email"] != $_POST["email"])
			$up["email"] = $_POST["email"];

		ac_sql_update("#subscriber", $up, "id = '$subscriberid'");

		// Now handle any changes to the subscriber_list table.
		$up = array();

		if (isset($_POST["fullname"])) {
			$expl = explode(" ", $_POST["fullname"], 2);
			$_POST["first_name"] = $expl[0];

			if (isset($expl[1]))
				$_POST["last_name"] = $expl[1];
		}

		if (isset($_POST["first_name"]) && $subscriber["first_name"] != $_POST["first_name"])
			$up["first_name"] = $_POST["first_name"];

		if (isset($_POST["last_name"]) && $subscriber["last_name"] != $_POST["last_name"])
			$up["last_name"] = $_POST["last_name"];

		ac_sql_update("#subscriber_list", $up, "subscriberid = '$subscriberid' AND listid = '$listid'");

		// Custom fields.
		if (isset($_POST["field"]))
			ac_custom_fields_update_data($_POST["field"], "#field_value", "fieldid", array("relid" => $subscriberid));

		# Save the update to em_update.
		$ins = array(
			"subscriberid" => $subscriberid,
			"campaignid"   => $campaignid,
			"messageid"    => $messageid,
			"=tstamp"      => "NOW()",
		);

		if (isset($_SERVER["REMOTE_ADDR"]))
			$ins["=ip"] = "INET_ATON('$ip')";

		ac_sql_insert("#update", $ins);

		// Need to update the campaign row.
		$up = array(
			"=updates" => "updates + 1",
		);

		ac_sql_update("#campaign", $up, "id = '$campaignid'");
		ac_sql_update("#campaign_message", $up, "campaignid = '$campaignid' AND messageid = '$messageid'");

		stats_activity_log('update', $subscriberid, $listid, $did = 0);

		break;
}

if (!$form) {
	if ( !$formid and $subscriber ) $formid = $fid = (int)$subscriber['formid'];

	// get default redirection urls
	$form = form_select_row($formid);
	if ( !$form ) {
		exit;
	}
}

// subscription form redirect engine
$redirect = form_redirect($form, $subscriber, $action, $codes, $lists, $ask4reason, $extra);

if ( !ac_str_is_url($redirect) ) {
	echo $redirect;
	exit;
}

// add reason/confirm wanted switch
//if ( $ask2confirm ) $redirect .= "&confirm=1";
if ( $ask4reason  ) $redirect .= "&reason=1&s=$hash";
//if ( $ask4reason  ) $redirect .= "&reason=1";

//dbg($redirect);
header("Location: " . $redirect);

?>
