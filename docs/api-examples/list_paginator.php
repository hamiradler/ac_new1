<?php
/**
Title: View many existing mailing lists using pagination, much like it appears in the standard user interface.

Description: View many mailing lists based on sorting, offset, limits, filters, and public/private. This allows you to view mailing lists much like you would through the admin interface - Manage Lists screen.

Supported formats: xml, json, serialize

Supported request methods: GET

Requires authentication: true

Parameters (* denotes requirement):
{
	[*api_key] => Your API key
	[*api_action] => list_paginator
	[*api_output] => xml, json, or serialize
	[*sort] => Leave empty for default sorting. Example: 01, 01D, 02, 02D, etc.
	[*offset] => The amount of records you want to skip over. This works in tandem with "limit." Example: 20, 50
	[*limit] => The amount of mailing lists you wish to retrieve. Example: 10, 20, 50, 100
	[*filter] => The section filter that should be applied. This corresponds to a unique ID in the sectionfilter database table. Example: 11
	[*public] => 0: no, 1: yes
}

Example response:
{
	[paginator] => ID of the paginator.
	[offset] => Result pages to skip over. Example: 0
	[limit] => Results returned per page. Example: 20
	[total] => Total returned. Example: 1
	[cnt] => Total count returned. Example: 1
	[rows] => Array
     (
			 [id] => Unique ID of the list. Example: 393
			 [stringid] => URL identifier for this list. Example: list-1
			 [userid] => List user ID (user that created the list). Example: 1
			 [name] => Internal display name of the list. Example: List 1
			 [cdate] => Date the list was created. Example: 2012-03-13 13:17:12
			 [p_use_tracking] => Example: 1
			 [p_use_analytics_read] => Example: 0
			 [p_use_analytics_link] => Example: 0
			 [p_use_twitter] => Whether or not to auto-post campaigns sent to this list to Twitter (must also turn on during campaign process). Example: 0
			 [p_use_facebook] => Whether or not to auto-post campaigns sent to this list to Facebook (must also turn on during campaign process). Example: 0
			 [p_embed_image] => Example: 1
			 [p_use_captcha] => Whether or not to require captcha when being added to this list. Example: 1
			 [send_last_broadcast] => Example: 0
			 [private] => Public or private setting for the list. Example: 0
			 [analytics_source] => Google Analytics source name. Example: List 1
			 [analytics_ua] => Google Analytics account number. Example: UA-12345-6
			 [twitter_token] => Twitter access token.
			 [twitter_token_secret] => Twitter access token secret.
			 [facebook_session] => Facebook session data, which contains the access token.
			 [carboncopy] => Emails to CC when new campaigns are sent to this list.
			 [subscription_notify] => Send to these emails when new subscribers are added to this list.
			 [unsubscription_notify] => Send to these emails when someone unsubscribes from this list.
			 [require_name] => Example: 0
			 [get_unsubscribe_reason] => Example: 0
			 [to_name] => Example: Subscriber
			 [optinoptout] => Example: 1
			 [sender_name] => Physical mailing address name. Example: My Company
			 [sender_addr1] => Physical mailing address line 1. Example: 123 S. Street Blvd
			 [sender_addr2] => Physical mailing address line 2. Example: Suite 100
			 [sender_city] => Physical mailing address city. Example: Chicago
			 [sender_state] => Physical mailing address state. Example: IL
			 [sender_zip] => Physical mailing address zip code. Example: 60601
			 [sender_country] => Physical mailing address country. Example: USA
			 [sender_phone] => Physical mailing address phone number. Example: 123-456-7890
			 [optinmessageid] => Example: 0
			 [optoutconf] => Example: 0
			 [listid] => Example: 393
			 [p_admin] => Example: 1
			 [p_list_add] => Example: 1
			 [p_list_edit] => Example: 1
			 [p_list_delete] => Example: 1
			 [p_list_sync] => Example: 1
			 [p_list_filter] => Example: 1
			 [p_message_add] => Example: 1
			 [p_message_edit] => Example: 1
			 [p_message_delete] => Example: 1
			 [p_message_send] => Example: 1
			 [p_subscriber_add] => Example: 1
			 [p_subscriber_edit] => Example: 1
			 [p_subscriber_delete] => Example: 1
			 [p_subscriber_import] => Example: 1
			 [p_subscriber_approve] => Example: 1
			 [luserid] => List user ID (user that created the list). Example: 3
			 [url] => Public Archive URL for the list. Example: http://ACCOUNT.activehosted.com/archive/list-1
			 [segments] =>
			 [avatars] =>
			 [addressid] => ID of the physical mailing address being used. Example: 0
			 [subscribers] => Total number of subscribers for this list. Example: 3
			 [campaigns] => Total campaigns sent to this list. Example: 3
			 [emails] => Total emails that have been sent campaigns for this list. Example: 8
     )
	[result_code] => Whether or not the response was successful. Examples: 1 = yes, 0 = no
	[result_message] => A custom message that appears explaining what happened. Example: Something is returned
	[result_output] => The result output used. Example: serialize
}
**/


// By default, this sample code is designed to get the result from your
// server (where ActiveCampaign Email Marketing is installed) and to print out the result
$url    = 'http://account.activehosted.com';


$params = array(

	// the API Key can be found on the "Your Settings" page under the "API" tab.
	// replace this with your API Key
	'api_key'      => 'YOUR_API_KEY',

	// this is the action that fetches a list info based on the ID you provide
	'api_action'   => 'list_paginator',

	// define the type of output you wish to get back
	// possible values:
	// - 'xml'  :      you have to write your own XML parser
	// - 'json' :      data is returned in JSON format and can be decoded with
	//                 json_decode() function (included in PHP since 5.2.0)
	// - 'serialize' : data is returned in a serialized format and can be decoded with
	//                 a native unserialize() function
	'api_output'   => 'serialize',

	'somethingthatwillneverbeused' => '', // this variable is pushed right back in the response
	'sort' => '', // leave empty to use a default one; other values are 01, 01D, 02, 02D, etc (number is a column index, and D means 'order descending')
	'offset' => 0, // start with this item (first page would be loaded using offset=0,limit=20, second page using offset=20,limit=20)
	'limit' => 20, // items per page
	'filter' => 0, // which sectionfilter to use (0=no filter)
	'public' => 0, // is public (1=yes, 0=no)

);

// This section takes the input fields and converts them to the proper format
$query = "";
foreach( $params as $key => $value ) $query .= $key . '=' . urlencode($value) . '&';
$query = rtrim($query, '& ');

// clean up the url
$url = rtrim($url, '/ ');

// This sample code uses the CURL library for php to establish a connection,
// submit your request, and show (print out) the response.
if ( !function_exists('curl_init') ) die('CURL not supported. (introduced in PHP 4.0.2)');

// If JSON is used, check if json_decode is present (PHP 5.2.0+)
if ( $params['api_output'] == 'json' && !function_exists('json_decode') ) {
	die('JSON not supported. (introduced in PHP 5.2.0)');
}

// define a final API request - GET
$api = $url . '/admin/api.php?' . $query;

$request = curl_init($api); // initiate curl object
curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
//curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment if you get no gateway response and are using HTTPS

$response = (string)curl_exec($request); // execute curl fetch and store results in $response

// additional options may be required depending upon your server configuration
// you can find documentation on curl options at http://www.php.net/curl_setopt
curl_close($request); // close curl object

if ( !$response ) {
	die('Nothing was returned. Do you have a connection to Email Marketing server?');
}

// This line takes the response and breaks it into an array using:
// JSON decoder
//$result = json_decode($response);
// unserializer
$result = unserialize($response);
// XML parser...
// ...

// Result info that is always returned
echo 'Result: ' . ( $result['result_code'] ? 'SUCCESS' : 'FAILED' ) . '<br />';
echo 'Message: ' . $result['result_message'] . '<br />';

// The entire result printed out
echo 'The entire result printed out:<br />';
echo '<pre>';
print_r($result);
echo '</pre>';

// Raw response printed out
echo 'Raw response printed out:<br />';
echo '<pre>';
print_r($response);
echo '</pre>';

// API URL that returned the result
echo 'API URL that returned the result:<br />';
echo $api;

?>